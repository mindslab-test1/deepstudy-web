#ifndef __CINCLUDE_H__
#define __CINCLUDE_H__

////////////////////////////////////////////////////////////////////
// common include files 
////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define O_BINARY     0

#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <limits.h>
#include <poll.h>
#include <errno.h>
#include <pthread.h> 
#include <signal.h> 
#include <aio.h>

typedef int BOOL;
#define TRUE  1
#define FALSE 0

////////////////////////////////////////////////////////////////////
// c++ include files 
////////////////////////////////////////////////////////////////////
#include <string>
#include <vector>
#include <deque>
#include <map>
using namespace std;

////////////////////////////////////////////////////////////////////
// etri include files 
////////////////////////////////////////////////////////////////////
#include <CoreTtsApi.h>


#endif // __CINCLUDE_H__


