/////////////////////////////////////////////////////////////////////////////
// cm_cfg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////

#include "cm_cfg.h"
#pragma GCC diagnostic ignored "-Wwrite-strings"

CmCfg* CmCfg::__cmCfg = NULL;

_CRITICAL_SECTION  cfg_lock;

/////////////////////////////////////////////////////////////////////////////
// CmCfg class
/////////////////////////////////////////////////////////////////////////////
CmCfg::CmCfg()
{
    lineList   = NULL;
    batchWrite = 0;
    updateCfgEnable = 0;

    _initializeCriticalSection(&cfg_lock);
}

CmCfg::~CmCfg()
{
    Py_XDECREF(lineList);
}

/////////////////////////////////////////////////////////////////////////////
// CmCfg class member function implemetation
/////////////////////////////////////////////////////////////////////////////
int CmCfg::readCfg()
{
    FILE *fp;
    char *p;

    strcpy(myName, "cfg");
    strcpy(fileName, "../conf/tts.cfg");

    if((fp=fopen(fileName, "rt")) == NULL) {
        if((p=getenv("ENG_EV_HOME")) == NULL) {
            Error1("file open error : %s", fileName);
            return -1;
        }
        sprintf(fileName, "%s/cfg/tts.cfg", p);
        if((fp=fopen(fileName, "rt")) == NULL) {
            Error1("file open error : %s", fileName);
            return -1;
        }
    }
    Info1("configuration file : %s", fileName);

    if(readConfig() < 0) {
        Error0("readConfig() failed!");
        return -1;
    }

    return 0;
}

void CmCfg::start()
{
    if(makeConfigList() < 0) {
	Error0("makeConfigList() error");
    }

    Info("updateCmConfig() thread starting...");
    updateCfgEnable = 0;
    pthread_t tid;
    pthread_create(&tid, NULL, updateCmConfig, this);
}

void CmCfg::stop()
{
    updateCfgEnable = 0;
}

void *CmCfg::updateCmConfig(void *_this)
{
    ((CmCfg*)_this)->updateCfg();
}

void CmCfg::updateCfg()
{
    int fd;
    struct stat fstate;

    if((fd = open(fileName, O_RDONLY)) < 0) {
	Error2("error=file open(%s), result=%d", fileName, fd);
    }
    fstat(fd, &fstate);
    long lastModified = fstate.st_mtime;

    while(updateCfgEnable) {
	sleep(5);
	_enterCriticalSection(&cfg_lock);
	fstat(fd, &fstate);
	if(lastModified != fstate.st_mtime) {
	    Info2("File(%s) is modified at %s", fileName, ctime(&fstate.st_mtime));
	    lastModified = fstate.st_mtime;
	    updateConfig();
	}
	_leaveCriticalSection(&cfg_lock);
    }

    close(fd);
}

int CmCfg::updateConfig()
{
    FILE *fp = openConfigFile("rt");
    if(fp == NULL) {
	fclose(fp);
        return -1;	
    }

    PyObject *dict = PyDict_New();
    PyObject *result = PyRun_File(fp, "tts.cfg", Py_file_input, dict, dict);
    fclose(fp);
    if(result == NULL) {
	Error0("Error in script file!");
        Py_DECREF(dict);
        return -1;	
    }

    Debug1("dict size=%d", PyDict_Size(dict));
    //setCmConfigFromFile(dict);

    return 0;
}

FILE *CmCfg::openConfigFile(char *mode)
{
    FILE *fp;

    if((fp=fopen(fileName, mode)) == NULL) {
	Error1("file open error : %s", fileName);
	return NULL;
    }

    return fp;
}

/*************************************************************************/
void CmCfg::logError(int level, const char* fileName, int lineNum,
        const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, myName, errorFmt, args);
    va_end(args);
}

int CmCfg::readConfig()
{
    int  i, num;

    Py_Initialize();

    PyObject *m = PyImport_AddModule("__main__");  // a borrowed reference
    if(!m) {
	Error0("unable to initialize main");
	return -1;
    }

    FILE *fp = openConfigFile("rt");
    if(fp == NULL) {
	fclose(fp);
        Py_Finalize();
        return -1;	
    }

    PyObject *dict = PyModule_GetDict(m);
    if(dict == NULL) {
	Error0("Error in GetDict()!");
	fclose(fp);
        Py_Finalize();
        return -1;	
    }

    PyObject *result = PyRun_File(fp, "tts.cfg", Py_file_input, dict, dict);
    fclose(fp);
    if(result == NULL) {
	Error0("Error in script file!");
        Py_DECREF(dict);
        Py_Finalize();
        return -1;	
    }
    
    //// SYSTEM parameters
    getIntegerItem(dict, "SYSTEMid", &cfgSystem.id, 1, 1, __MAX_SYSTEM__);
    getStringItem(dict, "SYSTEMname", cfgSystem.name, "EV01", __MAX_ADDRESS_LENGTH__); 
    getStringItem(dict, "SYSTEMlogPath", cfgSystem.logPath, "logfile", __MAX_PATH_LENGTH__); 
    getIntegerItem(dict, "SYSTEMport", &cfgSystem.port, 9993, 9000, 50000); 

    //// LIMIT values

    getIntegerItem(dict, "LIMITmaxOam", &cfgLimit.maxOam, 3, 1, __MAX_OAM__);
    getIntegerItem(dict, "LIMITmaxChannel", &cfgLimit.maxChannel, 100, 1, __MAX_CHANNEL__);

    //// LOG values
    getIntegerItem(dict, "LOGlevel", &cfgLog.level, 0, 0, 1);
    getIntegerItem(dict, "LOGbuffered", &cfgLog.buffered, 1, 0, 1); 

    //// TTS Server values
    if(getStringTupleItem(dict, "TTSsrvEng", cfgTtsServer.engServer[0],
        __MAX_TTS_SERVER__-1, __MAX_ADDRESS_LENGTH__, &num) == -1) {
        Error("There is no English TTS Server Address!");
        cfgLimit.maxTtsEng = 0;
    }
    else {
        cfgLimit.maxTtsEng = num;
    }

    if(getStringTupleItem(dict, "TTSsrvKor", cfgTtsServer.korServer[0],
        __MAX_TTS_SERVER__-1, __MAX_ADDRESS_LENGTH__, &num) == -1) {
        Error("There is no Korean TTS Server Address!");
        cfgLimit.maxTtsKor = 0;
    }
    else {
        cfgLimit.maxTtsEng = num;
    }

    for(i = 0 ; i < 24 ; i++)
        cfgLog.updateTime[i] = -1;

    if(getIntegerTupleItem(dict, "LOGupdateTime", cfgLog.updateTime, 
        24, 0, 0, 23, &num) 
        == -1) {
        cfgLog.updateTime[0] = 3;
        Info("There is no LOG updateTime, setting to 3");
    }

    //Py_DECREF(result);
    //Py_DECREF(dict);
   
    // not really needed
    //Py_Finalize();

   
    return 0;
}

int CmCfg::makeConfigList()
{
    FILE *fp = openConfigFile("rt");
    if(fp == NULL) {
	fclose(fp);
	return -1;
    }

    PyObject *pyFile = PyFile_FromFile(fp, fileName, "r", NULL);
    if( (pyFile == NULL) || !PyFile_Check(pyFile) ) {
	Error1("File(%s) is not PyFileObject", fileName);
	Py_DECREF(pyFile);
	fclose(fp);
	return -1;
    }

    // make a new list of length 0
    lineList = PyList_New(0);
    PyObject *pyLine;

    while(1) {
	// reads exact one line from pyFile object
	pyLine = PyFile_GetLine(pyFile, 0);
	string strLine = string(PyString_AsString(pyLine));
	// if pyLine is a string object then append this to lineList
	// until the end of file is reached
	if(PyString_Check(pyLine)) {
	    PyList_Append(lineList, pyLine);
	    // an empty string returns if the end of file is reached
	    if(PyString_Size(pyLine) == 0) {
		Debug("End of File(tts.cfg)");
		break;
	    }
	}
    }

    Py_DECREF(pyFile);
    Py_DECREF(pyLine);
    fclose(fp);
    return 0;
}

int CmCfg::setIntegerItem(const char *name, int value,
	int min, int max)
{
    int size = PyList_Size(lineList);
    if(size < 0) {
	return -1;
    }

    // bounds the value with min, max
    if(value < min) value = min;
    else if(value > max) value = max;

    char buf[128];
    sprintf(buf, " %d", value);
    string newValue = string(buf);

    int foundItem = -1;
    PyObject *line;

    _enterCriticalSection(&cfg_lock);

    for(int i=0; i<size; i++) {
	line = PyList_GetItem(lineList, i);
	if(!PyString_Check(line)) 
	    continue;
	string strLine = string(PyString_AsString(line));

	// checks if the line is a comment
	int pos;
	if((pos = strLine.find(string("#"))) >= 0) {
	    Debug1("This line is a comment. # found at pos(%d)", pos);
	    continue;
	}

	foundItem = strLine.find(string(name));
	if(foundItem >= 0) {
	    Debug2("%s is found in line %d", name, i+1);
	    int len = strLine.length();
	    int start = strLine.find('=');
	    strLine.replace(start+1, len-start-2, newValue); 
	    Debug1("line=%s", strLine.c_str());

	    line = PyString_FromString(strLine.c_str());
	    if(PyList_SetItem(lineList, i, line) < 0) {
		Error0("PyList_SetItem() error");
		Py_DECREF(line);
		_leaveCriticalSection(&cfg_lock);
		return -1;
	    }
	    break;
	}
    }

    if(foundItem < 0) {
	_leaveCriticalSection(&cfg_lock);
	return -1;
    }

    if(!batchWrite) {
	writeConfigFile();
    }

    _leaveCriticalSection(&cfg_lock);
    return 0;
}

void CmCfg::tokenize(const string &str, list<string>& tokens,
	const string& delimiters) 
{
    // skips delimiters at beginning
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // finds first "non-delimiters"
    string::size_type pos = str.find_first_of(delimiters, lastPos);

    while(string::npos != pos || string::npos != lastPos) {
	// if found a token, add it to the list
	tokens.push_back(str.substr(lastPos, pos-lastPos));
	// skips delimiters. Note the "not_of"
	lastPos = str.find_first_not_of(delimiters, pos);
	// finds next "non-delimiters"
	pos = str.find_first_of(delimiters, lastPos);
    }
}

int CmCfg::setStringItem(const char *name, const char *value, 
	int index /* = 0 */)
{
    int size = PyList_Size(lineList);
    if(size < 0) {
	return -1;
    }

    int foundItem = -1;
    PyObject *line;

    _enterCriticalSection(&cfg_lock);

    for(int i=0; i<size; i++) {
	line = PyList_GetItem(lineList, i);
	if(!PyString_Check(line)) 
	    continue;
	string strLine = string(PyString_AsString(line));
	Debug2(">>>> %d line = %s", i+1, strLine.c_str());
	string newValue;

	foundItem = strLine.find(string(name));
	if(foundItem >= 0) {
	    Debug2("%s is found in line %d", name, i+1);
	    int len = strLine.length();
	    int start = strLine.find('=');

	    if(index > 0) {
		string s = strLine.substr(start+1, len-start-2);
		Debug1("sub string=%s", s.c_str());
		string delimiters = string(", ");
		list<string> tokens;
		tokenize(s, tokens, delimiters);
		list<string>::iterator it = tokens.begin();
		for(int j=0; it!=tokens.end(); it++, j++) {
		    if(j == index-1) {
			if(*it == "'"+string(value)+"'") {
			    Py_DECREF(line);
			    _leaveCriticalSection(&cfg_lock);
			    return 0;
			}
			*it = "'"+string(value)+"'";
		    }
		    newValue.append(*it + ((j < tokens.size()-1) ? delimiters : ""));
		}
	    }
	    else {
		newValue = " '" + string(value) + string("'");
	    }
	    strLine.replace(start+1, len-start-2, newValue); 
	    Debug1("new line=%s", strLine.c_str());

	    line = PyString_FromString(strLine.c_str());
	    if(PyList_SetItem(lineList, i, line) < 0) {
		Error0("PyList_SetItem() error");
		Py_DECREF(line);
		_leaveCriticalSection(&cfg_lock);
		return -1;
	    }
	    break;
	}
    }

    if(foundItem < 0) {
	_leaveCriticalSection(&cfg_lock);
	return -1;
    }

    if(!batchWrite) {
	writeConfigFile();
    }

    _leaveCriticalSection(&cfg_lock);
    return 0;
}

int CmCfg::writeConfigFile()
{
    FILE *fp = openConfigFile("wt");
    if(fp == NULL) {
	fclose(fp);
	return -1;
    }
    PyObject *pyFile = PyFile_FromFile(fp, fileName, "wt", NULL);
    if( (pyFile == NULL) || !PyFile_Check(pyFile) ) {
	Error1("File(%s) is not PyFileObject", fileName);
	Py_DECREF(pyFile);
	fclose(fp);
	return -1;
    }

    PyObject *line;
    Debug1("lineList size = %d", PyList_Size(lineList));
    for(int i=0; i<PyList_Size(lineList); i++) {
	line = PyList_GetItem(lineList, i);
	if(!PyString_Check(line)) continue;
	PyFile_WriteObject(line, pyFile, Py_PRINT_RAW);
    }

    fclose(fp);

    return 0;
}

int CmCfg::getIntegerItem(PyObject *a, char *name, int *value, 
                           int default_value, int min_value, int max_value)
{
    int  ret;

    PyObject *b = PyDict_GetItemString(a, name);
    if(b) {
	if(PyInt_Check(b)) {
	    *value = PyInt_AsLong(b);
	    if(*value > max_value) {
		*value = max_value;
	    }
	    else if(*value < min_value) {
		*value = min_value;
	    }
	    Info2("%s = %d", name, *value);
	    ret = 0;
	}
	else {
	    Info1("%s is not a integer!", name);
	    ret = -1;
	}
    }
    else {
	Info2("%s is not set, default=%d", name, default_value);
	ret = -1;
    }

    if(ret == -1) {
	*value = default_value;
    }
    return ret;
}

int CmCfg::getIntegerTupleItem(PyObject *a, char *name, int *value,
            int maxNum, int default_value, int min_value, int max_value, int *num)
{
    int  i, cnt;

    *num = 0;

    PyObject *t = PyDict_GetItemString(a, name);

    if(!t) {
	Debug1("%s is not set", name);
	return -1;
    }

    if(PyTuple_Check(t)) {
	*num = PyTuple_Size(t);
	for(i=0,cnt=0; i<*num; ++i) {
	    PyObject *b = PyTuple_GetItem(t, i); 
	    if(PyInt_Check(b)) {
		value[cnt] = PyInt_AsLong(b);
		if(value[cnt] > max_value) {
		    value[cnt] = max_value;
		}
		else if(value[cnt] < min_value) {
		    value[cnt] = min_value;
		}
		Info3("%s[%d] = %d", name, cnt+1, value[cnt]);
	    }
	    else {
		Info3("%s[%d] is not a integer! default=%d", name,
		       cnt+1, default_value);
	        value[cnt] = default_value;	
	    }
	    if(++cnt >= maxNum)
		break;
	}
	*num = cnt;
    }
    else {
	if(PyInt_Check(t)) {
	    value[0] = PyInt_AsLong(t);
	    if(value[0] > max_value) {
		value[0] = max_value;
	    }
	    else if(value[0] < min_value) {
		value[0] = min_value;
	    }
	    Info3("%s[%d] = %d", name, 1, value[0]);
	    *num = 1;
	}
	else {
	    Info1("%s is neither a integer nor a tuple!", name);
	    return -1;
	}
    }

    Info2("%s size = %d", name, *num);
    return 0;
}

int CmCfg::getLongItem(PyObject *a, char *name, long *value,
	                long default_value, long min_value, long max_value)
{
    int  ret;

    PyObject *b = PyDict_GetItemString(a, name);
    if(b) {
	if(PyLong_Check(b)) {
	    *value = PyLong_AsLong(b);
	    if(*value > max_value) {
		*value = max_value;
	    }
	    else if(*value < min_value) {
		    *value = min_value;
	    }
	    Info2("%s = %08lx", name, *value);
	    ret = 0;
	}
	else {
	    Info1("%s is not a long!", name);
	    ret = -1;
	}
    }
    else {
	Info2("%s is not set, default=%08lx", name, default_value);
	ret = -1;
    }

    if(ret == -1) {
	*value = default_value;
    }
    return ret;
}

int CmCfg::getLongTupleItem(PyObject *a, char *name, long *value, 
                             int maxNum, long default_value, long min_value,
			     long max_value, int *num)
{
    int  i, cnt;

    *num = 0;

    PyObject *t = PyDict_GetItemString(a, name);

    if(!t) {
	Info1("%s is not set", name);
	return -1;
    }

    if(PyTuple_Check(t)) {
	*num = PyTuple_Size(t);
	for(i=0,cnt=0; i<*num; ++i) {
	    PyObject *b = PyTuple_GetItem(t, i);
	    if(PyLong_Check(b)) {
		value[cnt] = PyLong_AsLong(b);
		if(value[cnt] > max_value) {
		    value[cnt] = max_value;
		}
		else if(value[cnt] < min_value) {
		    value[cnt] = min_value;
		}
		Info3("%s[%u] = %08lx", name, cnt+1, value[cnt]);
	    }
	    else {
		Info3("%s[%d] is not a long!  default=%08lx", name,
			cnt+1, default_value);
		value[cnt] = default_value;
	    }
	    if(++cnt >= maxNum)
		break;
	}
	*num = cnt;
    }
    else {
	if(PyLong_Check(t)) {
	    value[0] = PyLong_AsLong(t);
	    if(value[0] > max_value) {
		value[0] = max_value;
	    }
	    else if(value[0] < min_value) {
		    value[0] = min_value;
	    }
	    Info3("%s[%d] = %08lx", name, 1, value[0]);
	    *num = 1;
	}
	else {
	    Info1("%s is neither a long nor a tuple!", name);
	    return -1;
	}
    }

    Info2("%s size = %d", name, *num);
    return 0;
}

int CmCfg::getStringItem(PyObject *a, char *name, char *value, 
                          char *default_value, int bsize)
{
    int  ret;

    PyObject *b = PyDict_GetItemString(a, name);
    if(b) {
	if(PyString_Check(b) && (PyString_Size(b)<bsize)) {
	    strcpy(value, PyString_AsString(b));
	    Info2("%s = %s", name, value);
	    ret = 0;
	}
	else {
	    Info1("%s is not a string!", name);
	    ret = -1;
	}
    }
    else {
	Info2("%s is not set, default=%s", name, default_value);
	ret = -1;
    }

    if(ret == -1) {
	strcpy(value, default_value);
    }
    return ret;
}

int CmCfg::getStringTupleItem(PyObject *a, char *name, char *value,
                               int maxNum, int bsize, int *num)
{
    int  i, cnt;

    *num = 0;

    PyObject *t = PyDict_GetItemString(a, name);

    if(!t) {
	Info1("%s is not set", name);
	return -1;
    }

    if(PyTuple_Check(t)) {
	*num = PyTuple_Size(t);
	for(i=0,cnt=0; i<*num; ++i) {
	    PyObject *s = PyTuple_GetItem(t, i); 
	    if(PyString_Check(s) && (PyString_Size(s)<bsize)) {
		strcpy(value+cnt*bsize, PyString_AsString(s));
		Info3("%s[%d] = %s", name, cnt+1, value+cnt*bsize);
		++cnt;
	    }
	    if(cnt >= maxNum)
		break;
	}
	*num = cnt;
    }
    else {
        if(PyString_Check(t) && (PyString_Size(t)<bsize)) {
	    strcpy(value, PyString_AsString(t));
	    Info2("%s[1] = %s", name, value);
	    *num = 1;
	}
	else {
	    Info2("%s is neither a string(size<%d) nor a tuple!", name, bsize);
	    return -1;
	}
    }

    Info2("%s size = %d", name, *num);
    return 0;
}

void CmCfg::copyCfgSystem(CFG_SYSTEM *cfg)
{
    memmove(cfg, &cfgSystem, sizeof(CFG_SYSTEM));
}

void CmCfg::copyCfgLimit(CFG_LIMIT *cfg)
{
    memmove(cfg, &cfgLimit, sizeof(CFG_LIMIT));
}

void CmCfg::copyCfgLog(CFG_LOG *cfg)
{
    memmove(cfg, &cfgLog, sizeof(CFG_LOG));
}



