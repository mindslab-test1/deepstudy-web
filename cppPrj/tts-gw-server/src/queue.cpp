// queue.cpp: implementation of the Queue class.
//
//////////////////////////////////////////////////////////////////////
#include "queue.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
DataQueue::DataQueue()
{
    buf = 0; 
    bufSize = 0;

    _initializeCriticalSection(&queue_lock);
}

DataQueue::~DataQueue()
{
    if(buf != 0) delete[] buf;
}

void DataQueue::makeBuf(int size)
{
    if(size > 0) 
	buf = new char[size];
    bufSize = size;
    clear();
}

void DataQueue::clear()
{
    _enterCriticalSection(&queue_lock);
    readPtr = 0;
    writePtr = 0;
    dataSize = 0;
    _leaveCriticalSection(&queue_lock);
}

int DataQueue::pop(int size)
{
    if(size <= 0) 
	return 0;

    _enterCriticalSection(&queue_lock);

    if(dataSize < size) 
	size = dataSize;

    readPtr += size;
    if(readPtr >= bufSize)
	readPtr -= bufSize;
    dataSize -= size;

    _leaveCriticalSection(&queue_lock);
    return size;
}

int DataQueue::pop(char *buffer, int size, int flag)
{
    if(size <= 0) 
	return 0;

    _enterCriticalSection(&queue_lock);

    if(dataSize < size) 
	size = dataSize;

    int tail = bufSize - readPtr;

    if(tail >= size) {
	memmove(buffer, &buf[readPtr], size);
    }
    else {
	memmove(buffer, &buf[readPtr], tail);
	memmove(&buffer[tail], buf, size-tail);
    }

    if(flag != 0) {
	readPtr += size;
	if(readPtr >= bufSize)
	    readPtr -= bufSize;
	dataSize -= size;
    }

    _leaveCriticalSection(&queue_lock);
    return size;
}

int DataQueue::push(const char *buffer, int size, int flag)
{
    if(size <= 0) 
	return 0;

    _enterCriticalSection(&queue_lock);

    if(flag == 0) {
	if((bufSize-dataSize) < size) 
	    size = bufSize - dataSize;
    }

    int tail  = bufSize - writePtr;

    if(tail >= size) {
	memmove(&buf[writePtr], buffer, size);
    }
    else {
	const char *p;
	if(size > bufSize) {
	    p = &buffer[size-bufSize];
	    size = bufSize;
	}
	else {
	    p = buffer;
	}
	memmove(&buf[writePtr], p, tail);
	memmove(buf, &p[tail], size-tail);
    }

    writePtr += size;
    if(writePtr >= bufSize)
	writePtr -= bufSize;
    dataSize += size;

    if(dataSize > bufSize) {
	dataSize = bufSize;
	readPtr = writePtr;
    }
    
    _leaveCriticalSection(&queue_lock);
    return size;
}

int DataQueue::getBufSize()
{
    return(bufSize);
}

int DataQueue::getDataSize()
{
    _enterCriticalSection(&queue_lock);
    int size = dataSize;
    _leaveCriticalSection(&queue_lock);

    return(size);
}

int DataQueue::getEmptySize()
{
    _enterCriticalSection(&queue_lock);
    int size = bufSize - dataSize;
    _leaveCriticalSection(&queue_lock);

    return(size);
}




