#include "channel.h"
#include <iconv.h>

ChDev::ChDev() {

}

ChDev::~ChDev() {

}

int ChDev::openChDev(int chnum) {
    int result;
    chNum = chnum;
    sprintf(myName, "%s%03d", "CH", chnum);
    isWorking = FALSE;

    result = _openChDev();

    return result;
}

int ChDev::_openChDev() {
    Debug("initialized");

    return 0;
}

BOOL ChDev::isAvailable() {
    if(isWorking)
        return FALSE;
    else 
        return TRUE;
}

void ChDev::setWorking(BOOL flag) {
    isWorking = flag;
}

void ChDev::doSimpleTts(const TtsRequest* request, TtsResponse* response) {

    CoreTTSClient *client;
    int nRet;
    int SpeechFormat = WF_M16L_WAV;

    try {
        Info1("REQ:%s", request->DebugString().c_str());
        char a[2048];
        if(request->script().length() > 0) {
            strcpy(a, request->script().c_str());
            //Info1("a=%s", a);
        } 
        else {
            throw runtime_error("Invalid script");
        }

        char serverIp[32];
        char serverPort[16];
        memset(serverIp, 0, sizeof(32));
        memset(serverPort, 0, sizeof(16));

        char in_script[8192];
        char out_script[8192];
        memset(in_script, 0, sizeof(in_script));
        memset(out_script, 0, sizeof(out_script));

        strcpy(in_script, request->script().c_str());
        size_t in_size, out_size;
        in_size = strlen(in_script);
        out_size = sizeof(out_script);

        if((strcmp("ENG", request->lang().c_str())) == 0) {
            // ENGLISH TTS SERVER
            char* p = strstr(cmCfg.getTtsSrvEng(1), ":");
            strncpy(serverIp, cmCfg.getTtsSrvEng(1), p - cmCfg.getTtsSrvEng(1));
            strcpy(serverPort, p+1);
            strcpy(out_script, in_script);
            Debug2("ENG TTS[%s:%i]", serverIp, atoi(serverPort));

        }
        else {
            // KOREAN TTS SERVER
            char* p = strstr(cmCfg.getTtsSrvKor(1), ":");
            strncpy(serverIp, cmCfg.getTtsSrvKor(1), p - cmCfg.getTtsSrvKor(1));
            strcpy(serverPort, p+1);
            iconv_t it = iconv_open("EUC-KR", "UTF-8");

            char *in_script_ptr = in_script;
            char *out_script_ptr = out_script;

            int ret = iconv(it, &in_script_ptr, &in_size, &out_script_ptr, &out_size);
            if(ret < 0) {
                Error1("iconv Error[%d]", ret);
                //throw runtime_error("iconv Failed");
            }
            iconv_close(it);
            Debug2("KOR TTS[%s:%i]", serverIp, atoi(serverPort));
        }
        
        client = TTSCreate(serverIp, atoi(serverPort), SpeechFormat);
        if(client == NULL) {
            Error("TTSCreate Error: can't create client");
            throw runtime_error("TTS Create Failed");
        }

        nRet = TTSOpen(client);
        if(nRet != TTS_OK) {
            Error1("TTSOpen Error[%x]", nRet);
            TTSDelete(client);
            throw runtime_error("TTS Open Failed");
        }

        char wavPath[1024];
        strcpy(wavPath, request->filename().c_str());
        char* extPtr = strstr(wavPath, ".mp3");
        if(extPtr == NULL) {
            throw runtime_error("TTS target filename error");
        }

        strncpy(extPtr, ".wav", 4);

        nRet = TTSGetSpeech(client, out_script, 
                   wavPath);
        if(nRet != TTS_OK) {
            Error1("TTS Get Speech Error[%x]", nRet);
            TTSClose(client);
            TTSDelete(client);
            throw runtime_error("TTS Get Speech Failed");
        }

        nRet = TTSClose(client);
        if(nRet != TTS_OK) {
            Error1("TTS Close Error[%x]", nRet);
            TTSDelete(client);
            throw runtime_error("TTS Close Error");
        }

        nRet = TTSDelete(client);
        if(nRet != TTS_OK) {
            Error1("TTS Delete Error[%x]", nRet);
            throw runtime_error("TTS Delete Error");
        }
        char cmd[2048];
        sprintf(cmd, "lame --silent %s %s", wavPath, request->filename().c_str());
        nRet = system(cmd);
        Debug1("lame cmd: %s", cmd);
        Debug1("lame ret: %d", nRet);
        if(nRet != 0) {
            Error1("lame Error[%d]", nRet);
            throw runtime_error("lame convert Error");
        }

        response->set_result_code(200);
        response->set_result_msg("SUCCESS");
        response->set_script(request->script());
        response->set_filename(request->filename());
    }
    catch(runtime_error e) {

        response->set_result_code(-1);
        response->set_result_msg(e.what());
        response->set_script("");
        response->set_filename("");
    }
    catch(...) {
        response->set_result_code(-1);
        response->set_result_msg("ERROR");
        response->set_script("");
        response->set_filename("");
    }
    return;
}

void ChDev::doTts(const TtsRequestEx* requestEx, TtsResponse* response) {
    try {
        int idx = 0;
        char wavPath[1024];
        strcpy(wavPath, requestEx->filename().c_str());
        char* extPtr = strstr(wavPath, ".mp3");
        if(extPtr == NULL) {
            throw runtime_error("TTS target filename error");
        }
        strncpy(extPtr, ".wav", 4);

        char soxCmd[2048];
        sprintf(soxCmd, "sox");

        string script = "";

        for(int i=0 ; i < requestEx->sentences_size() ; i++) {
            char wp[1024];
            sprintf(wp, "%s.tmp.%d", wavPath, i);
            idx++;
            
            TtsSentence sentence = requestEx->sentences(i);
            bool nRet = internalTts(sentence.lang().c_str(), sentence.script().c_str(), wp);
            script += sentence.script() + " ";

            if(nRet != TRUE) {
                throw runtime_error("TTS Failed");
            }
            strcat(soxCmd, " ");
            strcat(soxCmd, wp);
        }

        // merge wav files to mp3
        strcat(soxCmd, " ");
        strcat(soxCmd, wavPath);
        int ret = system(soxCmd);
        Debug1("sox cmd: %s", soxCmd);
        Debug1("sox ret: %d", ret);
        if(ret != 0) {
            Error1("sox Error[%d]", ret);
            throw runtime_error("sox Error");
        }

        char cmd[2048];
        sprintf(cmd, "lame --silent %s %s", wavPath, requestEx->filename().c_str());
        ret = system(cmd);
        Debug1("lame cmd: %s", cmd);
        Debug1("lame ret: %d", ret);
        if(ret != 0) {
            Error1("lame Error[%d]", ret);
            throw runtime_error("lame convert Error");
        }
        
        char delCmd[1024];
        sprintf(delCmd, "rm -rf %s.tmp.*", wavPath);
        ret = system(delCmd);

        response->set_result_code(200);
        response->set_result_msg("SUCCESS");
        response->set_script(script);
        response->set_filename(requestEx->filename());
    } 
    catch(runtime_error e) {

        response->set_result_code(-1);
        response->set_result_msg(e.what());
        response->set_script("");
        response->set_filename("");
    }
    catch(...) {
        response->set_result_code(-1);
        response->set_result_msg("ERROR");
        response->set_script("");
        response->set_filename("");
    }
    return;
}

bool ChDev::internalTts(const char* lang, const char* script, char* fileName) {
    CoreTTSClient *client;
    int nRet;
    int SpeechFormat = WF_M16L_WAV;

    try {
        Info2("TTS REQ: LANG=%s, script=%s", lang, script);
        if(strlen(script) <= 0) {
            throw runtime_error("Invalid script");
        }

        char serverIp[32];
        char serverPort[16];
        memset(serverIp, 0, sizeof(32));
        memset(serverPort, 0, sizeof(16));

        char in_script[8192];
        char out_script[8192];
        memset(in_script, 0, sizeof(in_script));
        memset(out_script, 0, sizeof(out_script));

        strcpy(in_script, script);
        size_t in_size, out_size;
        in_size = strlen(in_script);
        out_size = sizeof(out_script);
   
        if((strcmp("ENG", lang)) == 0) {
            // ENGLISH TTS SERVER
            char* p = strstr(cmCfg.getTtsSrvEng(1), ":");
            strncpy(serverIp, cmCfg.getTtsSrvEng(1), p - cmCfg.getTtsSrvEng(1));
            strcpy(serverPort, p+1);
            strcpy(out_script, in_script);
            Debug2("ENG TTS[%s:%i]", serverIp, atoi(serverPort));

        }
        else {
            // KOREAN TTS SERVER
            char* p = strstr(cmCfg.getTtsSrvKor(1), ":");
            strncpy(serverIp, cmCfg.getTtsSrvKor(1), p - cmCfg.getTtsSrvKor(1));
            strcpy(serverPort, p+1);
            iconv_t it = iconv_open("EUC-KR", "UTF-8");

            char *in_script_ptr = in_script;
            char *out_script_ptr = out_script;

            int ret = iconv(it, &in_script_ptr, &in_size, &out_script_ptr, &out_size);
            if(ret < 0) {
                Error1("iconv Error[%d]", ret);
            //    throw runtime_error("iconv Failed");
            }
            iconv_close(it);
            Debug2("KOR TTS[%s:%i]", serverIp, atoi(serverPort));
        }
        
        client = TTSCreate(serverIp, atoi(serverPort), SpeechFormat);
        if(client == NULL) {
            Error("TTSCreate Error: can't create client");
            throw runtime_error("TTS Create Failed");
        }

        nRet = TTSOpen(client);
        if(nRet != TTS_OK) {
            Error1("TTSOpen Error[%x]", nRet);
            TTSDelete(client);
            throw runtime_error("TTS Open Failed");
        }

        char wavPath[1024];
        strcpy(wavPath, fileName);

        nRet = TTSGetSpeech(client, out_script, 
                   wavPath);
        if(nRet != TTS_OK) {
            Error1("TTS Get Speech Error[%x]", nRet);
            TTSClose(client);
            TTSDelete(client);
            throw runtime_error("TTS Get Speech Failed");
        }

        nRet = TTSClose(client);
        if(nRet != TTS_OK) {
            Error1("TTS Close Error[%x]", nRet);
            TTSDelete(client);
            throw runtime_error("TTS Close Error");
        }

        nRet = TTSDelete(client);
        if(nRet != TTS_OK) {
            Error1("TTS Delete Error[%x]", nRet);
            throw runtime_error("TTS Delete Error");
        }

        return TRUE;
    }
    catch(runtime_error e) {
        Error1("runtime error: %s", e.what());
        return FALSE;
    }
    catch(...) {
        Error0("Exception!!!");
        return FALSE;
    }
}

bool ChDev::exists_file(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

void ChDev::logError(int level, const char* fileName, int lineNum,
        const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, myName, errorFmt, args);
    va_end(args);
}

