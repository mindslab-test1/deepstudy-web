//
// cm_cfg.h : header file
//
#ifndef __CM_CFG_H__
#define __CM_CFG_H__

#include <python2.7/Python.h>
#include <list>
using namespace std;
#include "cm_log.h"

#define cmCfg  (CmCfg::instance())


#define __MAX_SYSTEM__          	100
#define __MAX_PROTOCOL_LENGTH__ 	16
#define __MAX_OAM__             	5
#define __MAX_CHANNEL__            	1024
#define __MAX_TIMER_VALUE__   		3600 
#define __MAX_ADDRESS_LENGTH__ 		32
#define __MAX_PATH_LENGTH__     	1024
#define __MAX_TRAPSINK__        	8
#define __MAX_PREFIX_COUNT__       	32
#define __MAX_STR_LENGTH__       	8192
#define __MAX_TTS_SERVER__       	10

typedef struct {
    int   id;
    char  name[__MAX_ADDRESS_LENGTH__];
    char  logPath[__MAX_PATH_LENGTH__];
    int   port;
} CFG_SYSTEM;

typedef struct {
    int   maxOam;
    int   maxChannel;
    int   maxTtsEng;
    int   maxTtsKor;
} CFG_LIMIT;

typedef struct {
    int   level;
    int   buffered;
    int   updateTime[24];
} CFG_LOG;

typedef struct {
    char  engServer[__MAX_TTS_SERVER__][__MAX_ADDRESS_LENGTH__];
    char  korServer[__MAX_TTS_SERVER__][__MAX_ADDRESS_LENGTH__];
} CFG_TTS_SERVER;

/////////////////////////////////////////////////////////////////////////////
// CmCfg class
/////////////////////////////////////////////////////////////////////////////
class CmCfg
{
    protected:
	CmCfg();	// standard constructor
	virtual ~CmCfg();

    public:
	static CmCfg& instance()
	{
	    if(__cmCfg == NULL)
		__cmCfg = new CmCfg;
	    return *__cmCfg;
	}

	static void free()
	{
	    if(__cmCfg != NULL) {
		delete __cmCfg;
		__cmCfg = NULL;
	    }
	}

	int  readCfg();
        void start();
	void stop();

	static void *updateCmConfig(void *);
	void updateCfg();
	int  updateConfig();
	FILE *openConfigFile(char *);
	//void setCmConfigFromFile(PyObject *);

	void logError(int, const char*, int, const char*, ...);


    int  readConfig();
    int  getIntegerItem(PyObject *, char *, int *, int, int, int);
    int  getIntegerTupleItem(PyObject*, char*, int*, int, int, int, int, int*);
    int  getLongItem(PyObject*, char*, long*, long, long, long);
    int  getLongTupleItem(PyObject*, char*, long*, int, long, long, long, int*);
    int  getStringItem(PyObject *, char *, char *, char *, int);
    int  getStringTupleItem(PyObject*, char*, char*, int, int, int*);
	
	int  makeConfigList();
	int  setIntegerItem(const char *, int, int, int);
	int  setStringItem(const char *, const char *, int index=0);
	int  writeConfigFile();
	
	void tokenize(const string &, list<string>&, const string& delimiters=" ");

	void copyCfgSystem(CFG_SYSTEM *);
	void copyCfgLimit(CFG_LIMIT *);
	void copyCfgLog(CFG_LOG *);

	int getSystemId() {return cfgSystem.id;}
	char *getSystemName() {return cfgSystem.name;}
	char *getSystemLogPath() {return cfgSystem.logPath;}
    int getSystemPort() {return cfgSystem.port;}

	int getLimitMaxOam() {return cfgLimit.maxOam;}
	int getLimitMaxChannel() {return cfgLimit.maxChannel;}

	int getLogLevel() {return cfgLog.level;}
	int getLogBuffered() {return cfgLog.buffered;}

    char* getTtsSrvEng(int id) {
        return cfgTtsServer.engServer[id-1];}
    char* getTtsSrvKor(int id) {
        return cfgTtsServer.korServer[id-1];}


	// set configurations
	int   setSystemId(int value) { 
	    return setIntegerItem("SYSTEMid", value, 1, __MAX_SYSTEM__); }
	int   setSystemName(const char *str) { 
	    return setStringItem("SYSTEMname", str); }

    private:
    static CmCfg *__cmCfg;

	PyObject *lineList;
	int	     batchWrite;
	int 	 updateCfgEnable;

	char myName[32];
	char fileName[256];

    CFG_SYSTEM     cfgSystem;
    CFG_LIMIT      cfgLimit;
    CFG_LOG        cfgLog;
    CFG_TTS_SERVER cfgTtsServer;
};

#endif // __CM_CFG_H__

