//////////////////////////////////////////////////////////////////////
// queue.h: interface for the Queue class.
/////////////////////////////////////////////////////////////////

#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "cinclude.h"
#include "cm_thread.h"


class DataQueue  
{
public:
	DataQueue();
	virtual ~DataQueue();

	void makeBuf(int);
	void clear();

	int pop(int);
	int pop(char *, int, int flag=1);
	int push(const char *, int, int flag=1);
	
	int getBufSize();
	int getDataSize();
	int getEmptySize();

private:
	char *buf;
	int  bufSize;    
	int  readPtr;           /* start of ring buffer */
	int  writePtr;
	int  dataSize;

	_CRITICAL_SECTION   queue_lock;
};

#endif // __QUEUE_H__






