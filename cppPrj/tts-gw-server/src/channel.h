#ifndef __CH_DEV_H__
#define __CH_DEV_H__

#include "cm_log.h"
#include "cm_cfg.h"
#include "tts_gw.grpc.pb.h"

using std::string;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::Status;
using grpc::ServerReaderWriter;
using grpc::ClientReaderWriter;
using engedu::tts_gw::TtsRequest;
using engedu::tts_gw::TtsResponse;

using namespace engedu::tts_gw;

#define MAXCHAR 1024

class ChDev {
    public:
    ChDev();
    virtual ~ChDev();

    int openChDev(int);
    BOOL isAvailable();
    void setWorking(BOOL);
    void doSimpleTts(const TtsRequest* request, TtsResponse* response);
    void doTts(const TtsRequestEx* requestEx, TtsResponse* response);
    
    private:
    int _openChDev();
    bool exists_file(const std::string& name);
    bool internalTts(const char* lang, const char* script, char* fileName);
	void logError(int, const char*, int, const char*, ...);
    int chNum;
    char myName[64];
    BOOL isWorking;
};
#endif
