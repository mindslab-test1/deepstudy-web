#ifndef _CORETTS_API_H_
#define _CORETTS_API_H_

#ifndef CORETTS_API
#ifdef _WIN32
#define CORETTS_API __declspec(dllimport)
#define TTS_CALLING __cdecl
#else
#define CORETTS_API extern 
#define TTS_CALLING
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* 합성음의 포맷을 정의한다 */
typedef enum {
	WF_M8=1,
	WF_M8_WAV,
	WF_L16,
	WF_L16_WAV,
	WF_M16,
	WF_M16_WAV,
	WF_A8,
	WF_A8_WAV,
	WF_L8,
	WF_L8_WAV,	// 10
	WF_M16L,
	WF_M16L_WAV,
	WF_M8L,
	WF_M8L_WAV,
	WF_A8L,
	WF_A8L_WAV,
	WF_M8L_VOX,
	WF_M8L_MP2,
	WF_M16L_MP2,
	WF_M22,
	WF_M22_WAV,
	WF_L22,
	WF_L22_WAV,
	WF_M44,
	WF_M44_WAV,
	WF_L44,
	WF_L44_WAV
} WF_TYPE;

/* API의 리턴 타입을 정의한다 */
#define TTS_OK						0

/* 오류 타입을 정의한다 */
typedef enum {
	TTSERR_WSA_START=0x1001,
	TTSERR_WSA_CLEAN,
	TTSERR_SOCKET_CONNECTION,
	TTSERR_SOCKET_MAX_RETRY,
	TTSERR_SOCKET_INVALID,
	TTSERR_SOCKET_ERROR,
	TTSERR_SOCKET_SHUTDOWN,
	TTSERR_HOST_INVALID,
	TTSERR_NO_CLIENT,
	TTSERR_PORT_RANGE,
	TTSERR_CHANNEL_RANGE,
	TTSERR_ALREADY_INIT,
	TTSERR_NOT_OPENED,
	TTSERR_REJECTED,
	TTSERR_CHANNEL_ASSIGN,
	TTSERR_NOT_SYN_PKT,
	TTSERR_SYN_OPEN,
	TTSERR_SYN_READ,
	TTSERR_SYN_WRITE,
	TTSERR_WAVE_OPEN,
	TTSERR_WAVE_FORMAT,
	TTSERR_WAVE_HEADER,
	TTSERR_LIP_OPEN,
	TTSERR_LIP_READ,
	TTSERR_LIP_WRITE,
	TTSERR_MAX_WAIT_TIME,
	TTSERR_STATUS_READ,
	TTSERR_NO_RESPONSE,
	TTSERR_INVALID_RANGE,
	TTSERR_MEM_ALLOC,
	TTSERR_NOT_SUPPORTED,
	TTSERR_INPUT_OPEN,
	TTSERR_INPUT_READ,
	TTSERR_INPUT_FORMAT,
	TTSERR_OUTPUT_OPEN,
	TTSERR_OUTPUT_WRITE,
	TTSERR_OUTPUT_FORMAT,
	TTSERR_OUTPUT_BUFFER,
	TTSERR_OUTPUT_MEMORY,
	TTSERR_CONFIG,
	TTSERR_SAMPLING_RATE,
	TTSERR_WAVE_SEEK,
	TTSERR_WAVE_READ,
	TTSERR_VOX_OPEN,
	TTSERR_VOX_READ,
	TTSERR_VOX_WRITE,	// 0x102E
	TTSERR_INIT_PLAY,	// 0x102f
	TTSERR_EXTERN_STOP,	// 0x1030
	TTSERR_JNI_CALL,	// 0x1031
	TTSERR_INVALID_PTR, // + 50
	TTSERR_TEXT_SIZE,
	TTSERR_OPEN_RM,
	TTSERR_MIXSOUND,
	TTSERR_FADESOUND,
	TTSERR_CALLBACK
} TTSERR_TYPE;

/* 음성변조 모드를 정의한다. */
typedef enum {
	VE_JOLAMAN=1,
	VE_ROBOT,
	VE_RADIO,
	VE_PITCH1,
	VE_PITCH2,
	VE_PITCH3,
	VE_PITCH4,
	VE_PITCH5,
	VE_PITCH6,
	VE_PITCH7,
	VE_SPEED1,
	VE_SPEED2,
	VE_SPEED3,
	VE_SPEED4,
	VE_SPEED5,
	VE_ECHO,
	VE_GUMBANGI
} VEFFECT_TYPE;

/* 자원관리기의 연결 정보를 정의한다 */
typedef struct _TTS_CON_INFO_RM {
	unsigned char ipInfo[4];
	int nTtsPort;
	int nChUsed;
	int nChTotal;
} TTS_CON_INFO_RM;

/* 오디오 출력을 위한 포트를 지정한다 (솔라리스 플랫폼) */
#define WAVEOUT_SPEAKER         0x01
#define WAVEOUT_HEADPHONE       0x02
#define WAVEOUT_LINE_OUT        0x04

typedef void CoreTTSClient;

/* ====================================================================
윈도우즈 소켓을 초기화하는 함수이다. 음성합성엔진 API를 사용하는 서비스
프로그램에서 윈도우즈 소켓을 초기화하지 않은 경우 한번만 이 함수를 호출
하면 된다. 
1. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING StartWSA();

/* ====================================================================
윈도우즈 소켓 사용을 해제하는 함수이다. 음성합성엔진 API를 사용하는 
서비스 프로그램에서 윈도우즈 소켓 사용을 해제하지 않은 경우, 서비스 
프로그램 종료시 한번만 이 함수를 호출하면 된다. 
1. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING CleanWSA();

/* ====================================================================
음성합성엔진을 프로세서로 구동하는 경우 음성합성엔진의 구동상태를 파악
하는 함수이다. 이 함수를 사용하기 위해서는 음성합성엔진과 클라이언트 API가
같은 시스템에서 동작하고 있어야 한다.
1. 입력파라미터
     - pStatus: 음성합성엔진의 구동상태를 나타내는 것으로서, 0 에서 100 
       사이의 값을 가진다. 음성합성엔진이 정상적으로 구동 완료 되면 이값은 
       100 을 가리킨다.
     - pszMsg: 음성합성엔진의 상태 메세지를 받는 것으로서, 메세지의 최대 
       크기는 255 bytes 이다.
     - nMaxWaitTime: 음성합성엔진의 상태를 확인하는 최대 시간으로서, 단위는 
       초 이다. 함수는 이 시간 동안 상태를 확인할 수 없으면 오류 값을 리턴한다. 
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING GetStatusMsgFromTTS(
	int *pStatus, char *pszMsg, unsigned int nMaxWaitTime);

/* ====================================================================
음성합성엔진을 프로세서로 구동하는 경우 음성합성엔진이 구동완료 될 때까지
기다리는 함수이다. 이 함수를 사용하기 위해서는 음성합성엔진과 클라이언트 
API가 같은 시스템에서 동작하고 있어야 한다.
1. 입력파라미터
     - nMaxWaitTime: 음성합성엔진의 구동완료를 기다리는 최대 시간으로서, 
       단위는 초 이다. 함수는 이 시간 동안 상태를 확인할 수 없으면 오류 
       값을 리턴한다. 
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING GetWaitMsgFromTTS(
	unsigned int nMaxWaitTime);

/* ====================================================================
음성합성엔진 서버와 접속하기 위한 클라이언트를 생성하는 함수이다.
1. 입력파라미터: 
     - pszRemoteIP: 음성합성엔진이 동작하는 서버 주소이며, 숫자로 구성된
       dotted format IP주소이다. 예) 192.168.0.1
     - nPort: 음성합성엔진이 동작하는 포트번호이며, -1 을 입력하면
       디폴트 포트번호로 동작한다. 입력가능범위는 1 에서 65535 이다.
     - nFileFormat: TTSGetSpeech() 함수를 사용하는 경우, 저장하고자 하는 
       음성파일의 포맷을 지정하는 값으로서, 현재 지원하는 포맷은 다음과 같다.
       [PC용 엔진인 경우]
		 - WF_L16     : 16kHz/16bit/mono 인 linear PCM 포맷
	     - WF_L16_WAV : 16kHz/16bit/mono 인 linear PCM WAV 포맷
		 - WF_M16     : 16kHz/8bit/mono 인 mu-Law PCM 포맷
		 - WF_M16_WAV : 16kHz/8bit/mono 인 mu-Law PCM WAV 포맷
		 - WF_M16L    : 16kHz/16bit/mono 인 linear PCM 포맷
						(8bit->16bit conversion)
	     - WF_M16L_WAV: 16kHz/16bit/mono 인 linear PCM WAV 포맷
						(8bit->16bit conversion)
	     - WF_M16L_MP2: 32kHz/mono 인 MPEG1 Audio Layer2 포맷
	   [전화망용 엔진인 경우]
		 - WF_M8      : 8kHz/8bit/mono 인 mu-Law PCM 포맷
		 - WF_M8_WAV  : 8kHz/8bit/mono 인 mu-Law PCM WAV 포맷
		 - WF_A8      : 8kHz/8bit/mono 인 a-Law PCM 포맷
		 - WF_A8_WAV  : 8kHz/8bit/mono 인 a-Law PCM WAV 포맷
		 - WF_L8      : 8kHz/16bit/mono 인 linear PCM 포맷
		 - WF_L8_WAV  : 8kHz/16bit/mono 인 linear PCM WAV 포맷
		 - WF_M8L     : 8kHz/16bit/mono 인 linear PCM 포맷
						(8bit->16bit conversion)
		 - WF_M8L_WAV : 8kHz/16bit/mono 인 linear PCM WAV 포맷
						(8bit->16bit conversion)
		 - WF_M8L_VOX : 8kHz/4bit/mono 인 Dialogic ADPCM 포맷
						(8bit->4bit conversion)
		 - WF_A8L     : 8kHz/16bit/mono 인 linear PCM 포맷
						(8bit->16bit conversion)
		 - WF_A8L_WAV : 8kHz/16bit/mono 인 linear PCM WAV 포맷
						(8bit->16bit conversion)
	     - WF_M8L_MP2: 32kHz/mono 인 MPEG1 Audio Layer2 포맷
2. 리턴값: 정상적이면 클라이언트 포인터를, 그렇지 않으면 NULL을 리턴한다.
==================================================================== */
CORETTS_API CoreTTSClient* TTS_CALLING TTSCreate(
	char *pszRemoteIP, int nPort, int nFileFormat);

/* ====================================================================
음성합성엔진 서버와 접속하기 위해서 생성했던 클라이언트를 삭제하는 
함수이다.
1. 입력파라미터
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSDelete(
	CoreTTSClient *client);

/* ====================================================================
TTSCreate()함수에서 넘겨받은 서버주소를 이용하여 음성합성엔진 서버와 
접속을 수행하는 함수이다. 
1. 입력파라미터
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSOpen(
	CoreTTSClient *client);

/* ====================================================================
TTSCreate()함수에서 넘겨받은 서버주소를 이용하여 사용자가 지정한 채널로
음성합성엔진 서버와 접속을 수행하는 함수이다.
1. 입력파라미터
     - client: 클라이언트 포인터.
     - nChannel: 사용자 지정 채널 번호로서, 1 부터 최대채널 사이의 값.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSOpenChannel(
	CoreTTSClient *client, int nChannel);

/* ====================================================================
음성합성엔진 서버와 연결을 해제하는 함수이다.
1. 입력파라미터
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSClose(
	CoreTTSClient *client);

/* ====================================================================
합성하고자 하는 문장을 음성합성엔진 서버로 전송하고, 그 결과로 합성음을 
전달받는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszBuffer: 합성하고자 하는 문장을 저장한 텍스트 버퍼로서 버퍼의 
       끝에는 NULL이 있어야 함.
     - pszFileName: 음성합성엔진 서버로 부터 전송받은 합성음을 저장하는
       파일이름을 지정하는 것으로서 최대 255 bytes 까지 가능함.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSGetSpeech(
	CoreTTSClient *client, const char *pszBuffer, const char *pszFileName);

/* ====================================================================
합성하고자 하는 문장을 음성합성엔진 서버로 전송하고, 그 결과로 합성음을 
전달받는 함수로서 TTSGetSpeech()와는 달리 합성음을 파일로 저장하지 않고, 
사용자의 callback함수에게 합성음을 전달한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszBuffer: 합성하고자 하는 문장을 저장한 텍스트 버퍼로서 버퍼의 
       끝에는 NULL이 있어야 함. 
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSGetSpeechStream(
	CoreTTSClient *client, const char *pszBuffer);


CORETTS_API int TTS_CALLING TTSSendEmail2Manager(
	CoreTTSClient *client, const char *pszBuffer);


CORETTS_API int TTSSetTraining(void *lpVoid, int nMode);
/* ====================================================================
클라이언트가 서버로 부터 합성음을 전송받은 경우, 사용자 프로그램에서 
이를 처리할 수 있도록 API 내부에 callback 함수를 등록하는 함수이다. 
이 함수는 사용자 프로그램내에서  TTSGetSpeechStream() 함수보다 먼저 
호출되어야 한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pfn: 사용자 callback 함수.
     - nId: 클라이언트 ID 이며, 1 부터 최대채널 사이의 값.
     - pSamples: 합성음 데이터를 저장한 버퍼.
     - nSamples: 합성음 버퍼에 저장된 데이터의 갯수이며 byte단위로 계산됨.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetCallback(
	CoreTTSClient *client, 
	unsigned int (TTS_CALLING *pFn)(int nId, unsigned char *pSamples, int nSamples, int nTextOffset, int nTextLength));

/* ====================================================================
클라이언트가 TTSOpen()을 이용하여 접속한 경우, 음성합성 엔진으로 부터
할당받은 채널번호를 얻는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 1 부터 최대채널 사이의 값을, 그렇지 않으면 
   오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSGetChannel(CoreTTSClient *client);
CORETTS_API int TTS_CALLING TTSGetUsedChannel(CoreTTSClient *client, int nTimeout);
CORETTS_API int TTS_CALLING TTSGetPeakUsedChannel(CoreTTSClient *client, int nTimeout);

/* ====================================================================
클라이언트가 정상적으로 서버에 접속한 경우, 합성기 엔진에서 생성하는
합성음의 피치값을 조절하는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nPitch: 변경하고자 하는 피치레벨로서, 1-7 사이의 값(기본값은 3)을
       가진다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetPitch(
	CoreTTSClient *client, int nPitch);

/* ====================================================================
클라이언트가 정상적으로 서버에 접속한 경우, 합성기 엔진에서 생성하는
합성음의 속도를 조절하는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nSpeed: 변경하고자 하는 속도레벨로서, 1-5 사이의 값(기본값은 3)을
       가진다.
       속도를 퍼센트로 조절하기 위해서는 50 부터 200 까지 입력할 수 있으며,
       정상속도는 100 이다. 예를들어, 50 퍼센트는 속도가 2배 빨라지고
       200 퍼센트는 2배 느려진다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetSpeed(CoreTTSClient *client, int nSpeed);

CORETTS_API int TTS_CALLING TTSSetSampleRate(CoreTTSClient *client, int nSamplRate);
/* ====================================================================
클라이언트가 서버에 접속하는 경우 최대 접속 시도 횟수를 지정하는 함수
로서, TTSOpen() 또는 TTSOpenChannel() 함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nRetry: 지정하고자 하는 접속 시도 횟수로서, 1보다 큰 값을 가지며 
       기본값은 1이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetMaxConRetry(
	CoreTTSClient *client, int nRetry);

/* ====================================================================
합성기 엔진에서 립싱크 정보를 생성하도록 지정하는 함수로서, TTSGetSpeech()
함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMode: TRUE 이면 립싱크 정보를 생성하고, FALSE 이면 립싱크 정보를
       생성하지 않으며 기본값은 TRUE 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetLipSync(
	CoreTTSClient *client, int nMode);

/* ====================================================================
합성기 엔진으로 부터 립싱크 정보를 가져오는 함수로서, TTSGetSpeech() 함수가
성공한 다음에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszFileName: 음성합성엔진 서버로 부터 전송받은 립싱크 정보를 저장하는
       파일이름을 지정하는 것으로서 최대 255 bytes 까지 가능함.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSGetLipSync(
	CoreTTSClient *client, const char *pszFileName);

/* ====================================================================
TTSCreate()함수에서 넘겨받은 자원관리기 서버주소를 이용하여 음성합성엔진 
서버와 접속을 수행하는 함수이다. 이 함수는 자원관리기가 부하분산을 수행
하면서 항상 최적의 음성합성엔진 서버로 연결시켜준다.
1. 입력파라미터
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSOpenRM(
	CoreTTSClient *client);

/* ====================================================================
자원관리기와 연결을 해제하는 함수이다.
1. 입력파라미터
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSCloseRM(
	CoreTTSClient *client);

/* ====================================================================
합성기 엔진에서 음색변환된 음성을 생성하도록 지정하는 함수로서, 
TTSGetSpeech() 또는 TTSGetSpeechStream() 함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMode: TRUE 이면 음색변환된 음성을 생성하고, FALSE 이면 정상적인
       합성음을 생성한다. 기본값은 FALSE 이다.
     - fVtl: 성도길이를 지정하는 것으로서, 0.8 에서 1.2 사이의 값을 지정할
       수 있다. 기본 값은 1.0 이다.
     - fPitch: 억양을 지정하는 것으로서, 0.8 에서 1.2 사이의 값을 지정할
       수 있다. 기본 값은 1.0 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetVoiceConversion(
	CoreTTSClient *client,  int nVtl, int nPitch);

/* ====================================================================
합성기 엔진에서 사용하는 탐색공간을 지정하는 함수로서, TTSGetSpeech() 
또는 TTSGetSpeechStream() 함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nmaxCand: 탐색공간을 설정하는 값으로서, 50 에서 1000 사이의 값을 
       지정할 수 있다. 기본값은 150 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetMaxCand(
	CoreTTSClient *client, int nMaxCand);

/* ====================================================================
생성하고자 하는 합성음의 앞 뒤에 묵음을 삽입하는 함수로서, TTSGetSpeech() 
함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nStart: 합성음의 앞부분에 묵음을 삽입하고자 하는 경우에 1/1000 초  
       단위로 값을 지정한다. 기본 값은 0 이다.
     - nEnd: 합성음의 뒷부분에 묵음을 삽입하고자 하는 경우에 1/1000초  
       단위로 값을 지정한다. 기본 값은 0 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetSpeechMargin(
	CoreTTSClient *client, int nStart, int nEnd);

/* ====================================================================
합성음 생성시 문장과 문장 사이에 묵음을 삽입하는 함수로서, TTSGetSpeech() 
또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMargin: 삽입하고자 하는 묵음의 길이를 1/1000 초 단위로 
       값을 지정한다. 기본 값은 200 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetSentenceMargin(
	CoreTTSClient *client, int nMargin);

/* ====================================================================
합성음 생성시 문장내의 쉼표 부분에 묵음을 삽입하는 함수로서, TTSGetSpeech() 
또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMargin: 삽입하고자 하는 묵음의 길이를 1/1000 초 단위로 
       값을 지정한다. 기본 값은 200 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetPauseMargin(
	CoreTTSClient *client, int nMargin);

/* ====================================================================
합성하고자 하는 문장에 대해서 띄어쓰기 유무를 지정하는 함수로서, 
TTSGetSpeech() 또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nEnable: 자동으로 띄어쓰기를 하고자 하면 1, 그렇지 않으면 0 을
       지정한다. 기본 값은 1 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetWordSpacing(
	CoreTTSClient *client, int nEnable);

/* ====================================================================
합성하고자 하는 문장에 대해서 괄호읽기 유무를 지정하는 함수로서, 
TTSGetSpeech() 또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nEnable: 괄호안의 내용을 읽어주고자 하면 1, 그렇지 않으면 0 을
       지정한다. 기본 값은 0 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetParenReading(
	CoreTTSClient *client, int nEnable);

/* ====================================================================
합성하고자 하는 문장에 대해서 줄바꿈기호 무시 유무를 지정하는 함수로서, 
TTSGetSpeech() 또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nEnable: 줄바꿈기호를 무시하고자 하면 0, 그렇지 않으면 1 을
       지정한다. 기본 값은 1 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetReturnBreaking(
	CoreTTSClient *client, int nEnable);

/* ====================================================================
합성하고자 하는 문장의 일정부분에 대해서 음절단위 읽기 유무를 지정하는 
함수로서, TTSGetSpeech() 또는 TTSGetSpeechStream()함수를 호출하기 전에 
사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszStart: 음절단위 읽기 시작을 나타내는 태그 문자열을 지정한다. 
       합성기엔진은 이 태그 분자열을 제외한 다음 문자부터 또박또박 읽는다. 
       입력가능한 문자열의 최대 길이는 8 바이트 이며, 기본 문자열은 << 이다.
     - pszEnd: 음절단위 읽기 중지를 나타내는 태그 문자열을 지정한다. 
       합성기엔진은 이 태그 분자열의 앞까지 또박또박 읽는다. 
       입력가능한 문자열의 최대 길이는 8 바이트 이며, 기본 문자열은 >> 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetSyllableReading(
	CoreTTSClient *client, const char *pszStart, const char *pszEnd);

/* ====================================================================
클라이언트가 정상적으로 서버에 접속한 경우, 합성기 엔진에서 생성하는
합성음의 크기를 조절하는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - fVolume: 변경하고자 하는 볼륨 레벨로서, 0.10 에서 2.00 사이의 값
       (기본값은 1.00)을 가진다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetVolume(
	CoreTTSClient *client, double fVolume);

/* ====================================================================
생성하는 합성음에 대해서 음성변조 효과를 적용하는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMode: 적용하고자 하는 음성변조의 종류를 지정하는 것으로서, 
       0 에서 15 사이의 값(기본값은 0)을 가진다. 각각의 값에 대한
       정의는 다음과 같다.
       0 : 음성변조를 적용하지 않음
       1 : 졸라맨 음성
       2 : 로보트 음성
       3 : 라디오 음성
       4 - 10 : 피치 조절 음성
       11 - 15 : 속도 조절 음성
       16 : 메아리 음성
       17 : 굼뱅이 음성
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetVoiceEffect(
	CoreTTSClient *client, int nMode);

/* ====================================================================
클라이언트가 정상적으로 서버에 접속한 경우, TTSGetSpeech()함수 또는 
TTSGetSpeechStream()함수의 ReceiveTimeOut 시간을 지정하는 함수로서,
TTSOpen() 또는 TTSOpenChannel() 함수를 호출하기 전에 사용한다.
지정하는 시간내에 합성음이 수신되지 않는 상황이 발생하면 위의 함수들은 
TTSERR_SOCKET_ERROR로 리턴한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nSecond: 초단위로 TimeOut 값을 지정하며, 1 - 3600사이의 값
       (기본값은 0)을 가진다. 0 은 TimeOut 시간을 지정하지 않는 것과
       동일하다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetTimeOut(
	CoreTTSClient *client, int nSecond);

/* ====================================================================
합성하고자 하는 문장에 대해서 합성음을 실시간으로 재생하는 함수로서
TTSGetSpeech() 또는 TTSGetSpeechStream()함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nMode: 합성음을 재생하고자 하면 1, 그렇지 않으면 0 을
       지정한다. 기본 값은 0 이다. 솔라리스 플랫폼인 경우에는 출력용
       포트를 지정할 수 있다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetPlaySound(
	CoreTTSClient *client, int nMode);

/* ====================================================================
파일로 저장된 합성음을 재생하는 함수로서, 주로 TTSGetSpeech()함수를 호출 
하여 생성된 파일을 재상하고자 하는 경우에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszFileName: 재생하고자 하는 합성음 파일을 지정하는 것으로서 
       최대 255 bytes 까지 가능함.
     - nFileFormat: 음성파일의 포맷을 지정하는 값으로서 TTSCreate()함수의
       설명을 참조하면 된다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSPlayFile(
	CoreTTSClient *client, const char *pszFileName, int nFileFormat);

/* ====================================================================
재생하고 있는 합성음을 멈추게 하는 함수로서, TTSGetSpeechStream()함수를
사용하는 경우에만 동작한다.
1. 입력파라미터:
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSStopPlay(
	CoreTTSClient *client);

/* ====================================================================
재생하고 있는 합성음을 일시적으로 멈추게 하는 함수로서, TTSGetSpeechStream()함수를
사용하는 경우에만 동작한다.
1. 입력파라미터:
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSPausePlay(
	CoreTTSClient *client);

/* ====================================================================
재생하고 있는 합성음을 일시적으로 멈추게 한 것을 재개하는 함수로서, TTSGetSpeechStream()함수를
사용하는 경우에만 동작한다.
1. 입력파라미터:
     - client: 클라이언트 포인터.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSRestartPlay(
	CoreTTSClient *client);

/* ====================================================================
버퍼로 부터 넘겨받은 합성음의 속도를 조절하는 함수이다.
1. 입력파라미터:
     - client: 클라이언트 포인터.
     - pInBuf: 속도를 조절하고자 하는 입력 합성음 버퍼.
     - nInSize: 입력 합성음 버퍼의 크기(바이트단위).
     - pOutBuf: 속도를 조절한 출력 합성음 버퍼.
     - *nOutSize: 출력 합성음 버퍼의 크기(바이트단위)이며, 최대 nInSize의 
       두배이다.
     - nSpeed: 변경하고자 하는 속도레벨로서, 1-5 사이의 값(기본값은 3)을
       가진다.
     - nSampingRate: 입력 합성음의 샘플링 주파수를 지정한다.
     - nFormat: 입력 합성음의 포맷을 지정한다. 지정 가능한 값으로는
       WAVE_FORMAT_PCM, WAVE_FORMAT_MULAW, WAVE_FORMAT_ALAW 이 있다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSEffectSpeed(
	CoreTTSClient *client, unsigned char *pInBuf, int nInSize, unsigned char *pOutBuf, int *nOutSize,
		int nSpeed, int nSamplingRate, int nWaveFormat);

/* ====================================================================
클라이언트의 동작 상태를 로그 파일에 기록하는 함수이다.
     - client: 클라이언트 포인터.
     - nMode: 로그에 기록하고자 하면 1, 그렇지 않으면 0 을
       지정한다. 기본 값은 0 이다.
     - pszFileName: 로그 파일을 지정하는 것으로서 최대 255 bytes 까지 가능함.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetLog(
	CoreTTSClient *client, int nMode, const char *pszFileName);

/* ====================================================================
합성기엔진과 접속하여 합성음을 전송받는 도중에 합성음 수신을 멈추는 함수
로서, TTSGetSpeech() 함수를 사용하는 경우에만 멈춘다. 
     - client: 클라이언트 포인터.
     - nMode: 합성음 수신을 멈추고자 하면 1, 그렇지 않으면 0 을
       지정한다. 기본 값은 0 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSStop(
	CoreTTSClient *client, int nMode);

/* ====================================================================
자원관리기와 접속한 경우 연결정보를 얻는 함수로서 TTSOpenRM() 을 수행한 
후 사용한다. 
     - client: 클라이언트 포인터.
     - conInfo: 연결정보 구조체로서 상세정보는 아래와 같다.
       unsigned char ipInfo[4]: 접속하는 합성기엔진의 IP 정보 
       int nServerPort: 접속하는 합성기엔진의 포트 번호
       int nChUsed: 현재 사용중인 자원관리기의 채널 수
       int nChTotal: 현재 자원관리기의 최대 지원 채널 수
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSGetConInfoRM(
	CoreTTSClient *client, TTS_CON_INFO_RM *conInfo);

/* ====================================================================
클라이언트가 TTSOpen() 또는 TTSOpenRM()함수 호출 시 OpenTimeOut 시간을 
지정하는 함수로서, TTSOpen() 또는 TTSOpenRM()을 호출하기 전에 사용한다.
지정하는 시간내에 서버와 접속이 되지 않으면, TTSERR_SOCKET_ERROR로 
리턴한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nSecond: 초단위로 TimeOut 값을 지정하며, 1 - 3600사이의 값
       을 가진다. 기본값은 20 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetOpenTimeOut(
	CoreTTSClient *client, int nSecond);

/* ====================================================================
자원관리기 서버와 연결 실패하는 경우, 우회로 접속할 음성합성엔진 정보를
지정하는 함수이다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszRemoteIP: 음성합성엔진이 동작하는 서버 주소이며, 숫자로 구성된
       dotted format IP주소이다. 예) 192.168.0.1
     - nPort: 음성합성엔진이 동작하는 포트번호이며, -1 을 입력하면
       디폴트 포트번호로 동작한다. 입력가능범위는 1 에서 65535 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetRouteInfo(
	CoreTTSClient *client, char *pszRemoteIP, int nPort);

/* ====================================================================
배경음과 합성음을 믹싱하는 함수로서, TTSGetSpeech()함수를 호출 
하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - pszFileName: 믹싱하고자 하는 배경음 파일을 지정하는 것으로서,  
       최대 255 bytes 까지 가능함.
     - nMixRate: 믹싱하는 배경음의 비율을 퍼센트 단위로 지정하며, 
       1 - 100 사이의 값을 가진다. 기본값은 10 이다.
     - nRelative: 배경음을 믹싱하는 방법을 지정하며, 1 인 경우에는 
       합성음과 배경음의 볼륨을 맞춘 후 믹싱한다. 반대로 0 인 경우에는 
       원 음성 크기 그대로 믹싱한다. 기본값은 0 이다.
     - nOffset: 믹싱하고자 하는 배경음의 시작 위치를 1/1000 초 단위로
       지정하며, 기본값은 0 이다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetBgSound(
	CoreTTSClient *client, char *pszFileName, int nMixRate, int nRelative, int nOffset);

/* ====================================================================
배경음과 합성음이 믹싱된 음성을 페이드 처리하는 함수로서, TTSGetSpeech()
함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nFadeIn: 음성의 처음부터 얼마까지 페이드 인 처리할지 비율을 퍼센트 
       단위로 지정하며, 0 - 100 사이의 값을 가진다. 값이 0 인 경우에는 
       처리 하지 않는다.
     - nFadeOut: 음성의 마지막 얼마 전부터 페이드 아웃 처리할지 비율을 
       퍼센트 단위로 지정하며, 0 - 100 사이의 값을 가진다. 값이 0 인 
       경우에는 처리 하지 않는다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetFadeSound(
	CoreTTSClient *client, int nFadeIn, int nFadeOut);

/* ====================================================================
합성하고자 하는 텍스트의 길이를 제한하는 함수로서, TTSGetSpeech() 또는
TTSGetSpeechStream() 함수를 호출하기 전에 사용한다.
1. 입력파라미터: 
     - client: 클라이언트 포인터.
     - nTextLimit: 바이트 단위로 최대 입력 가능한 텍스트 길이를 지정하며, 
       0 - 임의숫자 사이의 값을 가진다. 값이 0 인 경우에는 기본값인 3072 로 
       지정되고, -1 인 경우에는 무한대로 지정된다.
2. 리턴값: 정상적이면 TTS_OK 를, 그렇지 않으면 오류 값을 리턴한다.
==================================================================== */
CORETTS_API int TTS_CALLING TTSSetTextLimit(
	CoreTTSClient *client, int nTextLimit);

#ifdef __cplusplus
}
#endif

#endif /* _CORETTS_API_H_ */
