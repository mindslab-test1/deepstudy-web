#ifndef _PRONEVAL_H_
#define _PRONEVAL_H_

#ifdef _USE_PTHREAD_MUTEX_
#include <pthread.h>
#endif

#include <stdio.h>

#ifdef __cplusplus
extern "C"{
#endif

#define APITYPE 
APITYPE void* doPronScore( void *a_pronstruct, void *aa_result , char *aa_ssinfo,  char *aa_data_p2s);
APITYPE void* createPronScoreDnnChild(  void * a_master);
APITYPE void* createPronScoreDnnMaster( char *a_serverCfg) ;
APITYPE void freePronScoreDnnMaster( void * );
APITYPE void freePronScore(void *a_struct);
	
#ifdef __cplusplus
}
#endif
#endif
