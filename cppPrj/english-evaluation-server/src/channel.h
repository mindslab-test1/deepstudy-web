#ifndef __CH_DEV_H__
#define __CH_DEV_H__

#include "cm_log.h"
#include "cm_cfg.h"
#include "english_evaluation.grpc.pb.h"

using std::string;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::Status;
using grpc::ServerReaderWriter;
using grpc::ClientReaderWriter;
using engedu::eng_evaluation::EvaluationRequest;
using engedu::eng_evaluation::EvaluationResponse;

using namespace engedu::eng_evaluation;

#define MAXCHAR 1024

class ChDev {
    public:
    ChDev();
    virtual ~ChDev();

    int openChDev(int, void*);
    BOOL isAvailable();
    void setWorking(BOOL);
    void doJob(const EvaluationRequest* request, EvaluationResponse* response);
    
    private:
    int _openChDev();
    bool exists_file(const std::string& name);
	void logError(int, const char*, int, const char*, ...);
    int chNum;
    char myName[64];
    BOOL isWorking;
   
    void *pron_structM;
    void *pron_structC;
};
#endif
