#include <iostream>
#include <stdio.h>

#include <grpc++/grpc++.h>
#include "english_evaluation.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using engedu::eng_evaluation::EvaluationService;
using engedu::eng_evaluation::EvaluationRequest;
using engedu::eng_evaluation::EvaluationResponse;

using namespace std;

int main(int argc, char** argv) {
    auto channel = grpc::CreateChannel("127.0.0.1:9993", grpc::InsecureChannelCredentials());
    auto stub = EvaluationService::NewStub(channel);
    if(argc < 2) {
        cout << "Usage: ee_client utter filenpath" << endl;
        return 0;
    }

    ClientContext ctx;

    EvaluationResponse resp;

    EvaluationRequest req;
    //req.set_utter("I am happy");
    req.set_utter(argv[1]);
    //req.set_score("10 10 10");
    //req.set_filename("/home/kirk/git/engedu/cppPrj/english-evaluation-server/src/Iamhappy.");
    req.set_filename(argv[2]);

    Status status = stub->SimpleAnalyze(&ctx, req, &resp);

    if(status.ok()) {
//        std::cout << "result: " << resp.result_code << " , " << resp.score << " , " << resp.sentence
//        << std::endl;
        std::cout << resp.DebugString() << std::endl;
    } 
    else {
        std::cout << "failed: " << status.error_message() << std::endl;
    }
    return 0;
}
