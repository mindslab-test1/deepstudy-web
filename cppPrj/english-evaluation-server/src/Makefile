#
# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

HOST_SYSTEM = $(shell uname | cut -f 1 -d_)
SYSTEM ?= $(HOST_SYSTEM)

MACHINE = gpu

ETRILIB=-L../lib -Xlinker --start-group -lfrontend -lpthread -lcm.opt -lpron.opt -lG2pEngMerge -lmsearch -lasearch -lbase -fopenmp -llaserdlnet -lesdlapi -lPron_st.opt\
		-lspeak.opt -lETRSpeechPlot.opt -lesdl.$(MACHINE) -lesdl4userdef -Xlinker --end-group
ETRIINC=-I../include

CUDA_PATH       ?= /usr/local/cuda
CUDA_INC_PATH   ?= $(CUDA_PATH)/include
CUDA_BIN_PATH   ?= $(CUDA_PATH)/bin
CUDA_LIB_PATH   ?= $(CUDA_PATH)/lib
ATLASLIBS = -L$(CUDA_LIB_PATH)64 -L/usr/lib64/atlas -llapack -lsatlas -lstdc++

ifeq ($(MACHINE), gpu)
EXEFLAGS := $(EXEFLAGS) -lm -lcudart -lcublas -lcuda
else
EXEFLAGS := $(EXEFLAGS) -lm
endif

CXX = g++
CPPFLAGS += `pkg-config --cflags protobuf grpc`
#CXXFLAGS += -std=c++11 -g -O2 -DMULTITHREAD -DUNIX $(ETRIINC) 
CXXFLAGS += -std=c++11 -g  -DMULTITHREAD -DUNIX $(ETRIINC) 
ifeq ($(SYSTEM),Darwin)
LDFLAGS += -L/usr/local/lib `pkg-config --libs protobuf grpc++`\
           -lgrpc++_reflection\
           -ldl -lutil\
		   $(ETRILIB) $(ATLASLIBS) $(EXEFLAGS)
else
LDFLAGS += -L/usr/local/lib `pkg-config --libs protobuf grpc++`\
           -Wl,--no-as-needed -lgrpc++_reflection -Wl,--as-needed\
           -ldl -lutil\
		   $(ETRILIB) $(ATLASLIBS) $(EXEFLAGS)

endif
PROTOC = protoc
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`

PROTOS_PATH = ../proto

vpath %.proto $(PROTOS_PATH)

all: system-check english_evaluation_server ee_client

PYTHONLIB=-L/usr/local/lib64/ -lpython2.7
PYTHONINC=-I/usr/local/python2.7

OBJS = english_evaluation.pb.o english_evaluation.grpc.pb.o english_evaluation_server.o \
       getopt.o cm_log.o queue.o cm_cfg.o queue.o evaluation_service_impl.o\
	   channel.o


ee_client: english_evaluation.pb.o english_evaluation.grpc.pb.o english_evaluation_client.o
	$(CXX) $^ $(LDFLAGS) -o $@

english_evaluation_server: $(OBJS)
	$(CXX) $^ $(LDFLAGS) $(PYTHONLIB) -o $@

%.grpc.pb.cc: %.proto
	$(PROTOC) -I $(PROTOS_PATH) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

%.pb.cc: %.proto
	$(PROTOC) -I $(PROTOS_PATH) --cpp_out=. $<

clean:
	rm -f *.o *.pb.cc *.pb.h ee_client english_evaluation_server

install:
	cp ee_client english_evaluation_server pronLR.GENIE_ADULT_KSEC.cfg pronLR.GENIE_ALL10k_KSEC.cfg ../bin/
# The following is to test your system and ensure a smoother experience.
# They are by no means necessary to actually compile a grpc-enabled software.

PROTOC_CMD = which $(PROTOC)
PROTOC_CHECK_CMD = $(PROTOC) --version | grep -q libprotoc.3
PLUGIN_CHECK_CMD = which $(GRPC_CPP_PLUGIN)
HAS_PROTOC = $(shell $(PROTOC_CMD) > /dev/null && echo true || echo false)
ifeq ($(HAS_PROTOC),true)
HAS_VALID_PROTOC = $(shell $(PROTOC_CHECK_CMD) 2> /dev/null && echo true || echo false)
endif
HAS_PLUGIN = $(shell $(PLUGIN_CHECK_CMD) > /dev/null && echo true || echo false)

SYSTEM_OK = false
ifeq ($(HAS_VALID_PROTOC),true)
ifeq ($(HAS_PLUGIN),true)
SYSTEM_OK = true
endif
endif

system-check:
ifneq ($(HAS_VALID_PROTOC),true)
	@echo " DEPENDENCY ERROR"
	@echo
	@echo "You don't have protoc 3.0.0 installed in your path."
	@echo "Please install Google protocol buffers 3.0.0 and its compiler."
	@echo "You can find it here:"
	@echo
	@echo "   https://github.com/google/protobuf/releases/tag/v3.0.0"
	@echo
	@echo "Here is what I get when trying to evaluate your version of protoc:"
	@echo
	-$(PROTOC) --version
	@echo
	@echo
endif
ifneq ($(HAS_PLUGIN),true)
	@echo " DEPENDENCY ERROR"
	@echo
	@echo "You don't have the grpc c++ protobuf plugin installed in your path."
	@echo "Please install grpc. You can find it here:"
	@echo
	@echo "   https://github.com/grpc/grpc"
	@echo
	@echo "Here is what I get when trying to detect if you have the plugin:"
	@echo
	-which $(GRPC_CPP_PLUGIN)
	@echo
	@echo
endif
ifneq ($(SYSTEM_OK),true)
	@false
endif
