#include "evaluation_service_impl.h"
#include "cm_log.h"
#include "cm_cfg.h"
#include "cm_thread.h"
#include <thread>

using std::string;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::Status;
using grpc::ServerReaderWriter;
using grpc::ClientReaderWriter;
using engedu::eng_evaluation::EvaluationRequest;
using engedu::eng_evaluation::EvaluationResponse;

using namespace engedu::eng_evaluation;

_CRITICAL_SECTION   ch_lock;

EvaluationServiceImpl::EvaluationServiceImpl() {
    _initializeCriticalSection(&ch_lock);
    strcpy(myName, "eval");
    Info("EvaluationServiceImpl START");
    initialize();

}

EvaluationServiceImpl::~EvaluationServiceImpl() {
    Info("EvaluationServiceImpl END");
}

void EvaluationServiceImpl::initialize() {
    reqCounter = 0;
    chDevice = NULL;
    maxChannel = cmCfg.getLimitMaxChannel();

    pronStruct = (void*)createPronScoreDnnMaster("../conf/pron.cfg");

    chDevice = (ChDev**)new ChDev[maxChannel];
    for(int i=0; i < maxChannel; i++) {
        chDevice[i] = new ChDev;
        chDevice[i]->openChDev(i+1, (void*)pronStruct);
    }
}

ChDev* EvaluationServiceImpl::getAvailableChDev() {

    _enterCriticalSection(&ch_lock);

    for(int i=0; i<maxChannel; i++) {
        if(chDevice[i]->isAvailable() == TRUE) {
            chDevice[i]->setWorking(TRUE);
            _leaveCriticalSection(&ch_lock);
            return chDevice[i];
        }
    }

    _leaveCriticalSection(&ch_lock);

    return NULL;
}

Status EvaluationServiceImpl::SimpleAnalyze(ServerContext* context, const EvaluationRequest* request, EvaluationResponse* response) {
    reqCounter ++;
    if(reqCounter % 10 == 0) {
        int workingChannel = 0;
        for(int i=0 ; i<maxChannel ; i++) {
            if(chDevice[i]->isAvailable() == FALSE) {
                workingChannel ++;
            }
        }
        Info2("Total Channel:%d, Working Channel:%d", maxChannel, workingChannel);
        reqCounter = 0;
    }

    Info("SimpleAnalyze START");

    ChDev* chdev = getAvailableChDev();
    if(chdev != NULL) {
        chdev->doJob(request, response);
        chdev->setWorking(FALSE);
    }
    else {
        response->set_result_code(-1);
        response->set_result_msg("FAIL:NO AVAILABLE CHANNEL");
        response->set_eval_score(0);
        response->set_regression_holistic(0.0);
        response->set_regression_speed(0.0);
        response->set_regression_rhythm(0.0);
        response->set_regression_intonation(0.0);
        response->set_regression_segmental(0.0);
        response->set_regression_segmental_feat19(0.0);
        response->set_sentence(request->utter());
    }

    Info1("SimpleAnalyze END:\n%s", response->DebugString().c_str());

    return Status::OK;
}

Status EvaluationServiceImpl::GrammerAnalyze(ServerContext* context, const EvaluationRequest* request, EvaluationResponse* response) {
    return Status::OK;
}

Status EvaluationServiceImpl::PronAnalyze(ServerContext* context, const EvaluationRequest* request, EvaluationResponse* response) {
    return Status::OK;
}

void EvaluationServiceImpl::logError(int level, const char* fileName, int lineNum,
        const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, myName, errorFmt, args);
    va_end(args);
}

