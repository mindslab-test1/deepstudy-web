#include "channel.h"

ChDev::ChDev() {

}

ChDev::~ChDev() {

}

int ChDev::openChDev(int chnum, void *data) {
    int result;
    chNum = chnum;
    sprintf(myName, "%s%03d", "CH", chnum);
    isWorking = FALSE;
    pron_structM = (void*)data;

    result = _openChDev();

    return result;
}

int ChDev::_openChDev() {
    Debug("initialized");
    pron_structC = createPronScoreDnnChild(pron_structM); 
    return 0;
}

BOOL ChDev::isAvailable() {
    if(isWorking)
        return FALSE;
    else 
        return TRUE;
}

void ChDev::setWorking(BOOL flag) {
    isWorking = flag;
}

void ChDev::doJob(const EvaluationRequest* request, EvaluationResponse* response) {
    DECODER_RESULT *result = NULL;
    char *pronClinicResult = NULL;

    try {
        Info1("REQ:%s", request->DebugString().c_str());


        char a[1024];
        if(request->utter().length() > 0) {
            strcpy(a, request->utter().c_str());
        //    Info1("a=%s", a);
        } 
        else {
            throw runtime_error("Invalid Utter");
        }
        if(!exists_file(request->filename())) {
            throw runtime_error("Invalid wav file");
        }

        result = (DECODER_RESULT*)malloc(sizeof(DECODER_RESULT));
        memset(result, 0, sizeof(DECODER_RESULT));
        strcpy(result->m_result, request->utter().c_str());

        result->m_confidence = 1;
        Info1("m_result=%s", result->m_result);

        pronClinicResult = (char*)malloc(sizeof(char)*MAXCHAR*12);
        strcpy(pronClinicResult,"");

        //p_intonationStruct = (ETRI_SpeechPlot_IntonationStruct *) doPronScoreV2_1( pron_struct, result, 
        //        (char*)request->filename().c_str(), pronClinicResult, NULL);
        doPronScore(pron_structC, result, (char*)request->filename().c_str(), pronClinicResult);
        Debug("=================");
        Debug1("%s", pronClinicResult);
        Debug("=================");

        char eval_score[15];
        char regression_holistic[15];
        char regression_speed[15];
        char regression_rhythm[15];
        char regression_intonation[15];
        char regression_segmental[15];
        char regression_segmental_feat19[15];

        char* startPtr = strstr(pronClinicResult, "<eval_score>");
        char* endPtr   = strstr(&startPtr[1], "</eval_score>");
        memcpy(eval_score, &startPtr[12], endPtr - startPtr - 12);
        //Debug1("SCORE=%s", eval_score);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_holistic, &startPtr[11], endPtr - startPtr - 11);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_speed, &startPtr[11], endPtr - startPtr - 11);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_rhythm, &startPtr[11], endPtr - startPtr - 11);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_intonation, &startPtr[11], endPtr - startPtr - 11);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_segmental, &startPtr[11], endPtr - startPtr - 11);

        startPtr = strstr(&endPtr[1], "<evalScore>");
        endPtr   = strstr(&startPtr[1], "</evalScore>");
        memcpy(regression_segmental_feat19, &startPtr[11], endPtr - startPtr - 11);


        //Debug1("RESULT=%s", pronClinicResult);
        response->set_result_code(200);
        response->set_result_msg("SUCCESS");
        response->set_eval_score(atoi(eval_score));
        response->set_regression_holistic(atof(regression_holistic)*20);
        response->set_regression_speed(atof(regression_speed)*20);
        response->set_regression_rhythm(atof(regression_rhythm)*20);
        response->set_regression_intonation(atof(regression_intonation)*20);
        response->set_regression_segmental(atof(regression_segmental)*20);
        response->set_regression_segmental_feat19(atof(regression_segmental_feat19)*20);
        response->set_sentence(request->utter());

        //Info("SimpleAnalyze END");
        free(pronClinicResult);
        free(result);
    }
    catch(runtime_error e) {
        if(pronClinicResult != NULL) {
            free(pronClinicResult);
        }
        if(result != NULL) {
            free(result);
        }

        response->set_result_code(-1);
        response->set_result_msg(e.what());
        response->set_eval_score(0);
        response->set_regression_holistic(0.0);
        response->set_regression_speed(0.0);
        response->set_regression_rhythm(0.0);
        response->set_regression_intonation(0.0);
        response->set_regression_segmental(0.0);
        response->set_regression_segmental_feat19(0.0);
        response->set_sentence(request->utter());
    }
    catch(...) {
        if(pronClinicResult != NULL) {
            free(pronClinicResult);
        }
        if(result != NULL) {
            free(result);
        }

        response->set_result_code(-1);
        response->set_result_msg("FAIL:INTERNAL ERROR");
        response->set_eval_score(0);
        response->set_regression_holistic(0.0);
        response->set_regression_speed(0.0);
        response->set_regression_rhythm(0.0);
        response->set_regression_intonation(0.0);
        response->set_regression_segmental(0.0);
        response->set_regression_segmental_feat19(0.0);
        response->set_sentence(request->utter());
    }

    return;
}

bool ChDev::exists_file(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

void ChDev::logError(int level, const char* fileName, int lineNum,
        const char* errorFmt, ...)
{
    va_list args;
    va_start(args, errorFmt);
    logger.vlogError(level, fileName, lineNum, 0, myName, errorFmt, args);
    va_end(args);
}

