package ai.mindslab.engedu.admin.qa.list.dao;

import ai.mindslab.engedu.admin.qa.list.dao.data.QADomainVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QaAnswerAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface QAListAdminMapper {

     List<QADomainVO> getDomainParent(Map<String, Object> param) throws EngEduException;
     List<QADomainVO> getDomainSub(Map<String, Object> param) throws EngEduException;
     int getQAQuestionAdminListCount(Map<String, Object> param) throws EngEduException;
     List<QAListAdminVO> getQAQuestionAdminList (Map<String, Object> param) throws EngEduException;
     List<QAListAdminVO> getQAQuestionAdminExcelList(Map<String, Object> param) throws EngEduException;
     int deleteQAQuestionAdmin(Map<String, Object> param) throws EngEduException;
     int deleteQAanswerAdmin(Map<String, Object> param) throws EngEduException;
     int deleteQAIndexAdmin(Map<String, Object> paramMap);
     List<QADomainVO> getSelectDomainList() throws EngEduException;
     QAListAdminVO getQuestionDetailAdmin(Map<String, Object> param) throws EngEduException;
     int updateQuestionAdmin(QAListAdminVO vo)throws EngEduException;
     int updateQaAnswerAdmin(QaAnswerAdminVO vo)throws EngEduException;
      int insertQuestionAdmin(QAListAdminVO vo)throws EngEduException;
     int insertQaAnswerAdmin(QaAnswerAdminVO vo)throws EngEduException;
     List<QAListAdminVO> getQAQuestionList(Map<String, Object> param);
}
