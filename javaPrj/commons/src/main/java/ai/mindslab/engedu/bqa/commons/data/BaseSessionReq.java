package ai.mindslab.engedu.bqa.commons.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
public class BaseSessionReq implements Serializable{

	protected String sessionId;
}
