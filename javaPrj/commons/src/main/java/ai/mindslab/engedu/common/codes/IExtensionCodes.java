package ai.mindslab.engedu.common.codes;

public interface IExtensionCodes {

    public static final String EXT_MP3 = ".mp3";
    public static final String EXT_WAV = ".wav";
    public static final String EXT_TXT = ".txt";
    public static final String EXT_PCM = ".pcm";

}
