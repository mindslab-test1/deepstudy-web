package ai.mindslab.engedu.common.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface ApiLogMapper {

	int insertApiLog(Map<String, Object> hashMap);

	int insertErrorLog(Map<String, Object> hashMap);
}
