package ai.mindslab.engedu.dic.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.dic.dao.data.MathDicVO;

@Mapper
public interface MathDicMapper {
	int getCount();

	List<MathDicVO> getSearch(Map<String, String> hashMap);
}
