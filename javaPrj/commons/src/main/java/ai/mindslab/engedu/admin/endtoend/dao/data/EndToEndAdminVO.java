package ai.mindslab.engedu.admin.endtoend.dao.data;

import ai.mindslab.engedu.game.dao.data.EndToEndVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class EndToEndAdminVO extends EndToEndVO {
    String userId;
}
