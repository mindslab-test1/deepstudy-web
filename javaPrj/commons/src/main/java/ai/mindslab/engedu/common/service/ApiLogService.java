package ai.mindslab.engedu.common.service;

import ai.mindslab.engedu.common.dao.ApiLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class ApiLogService {
	
	@Autowired
	private ApiLogMapper apiLogMapper;

	public String insertApiLog(String resultType, int resCode, String resMsg, String resResult, String userId){

		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("resultType", resultType);
		hashMap.put("resCode", resCode);
		hashMap.put("resMsg", resMsg);
		hashMap.put("resResult", resResult);
		hashMap.put("userId", userId);

		apiLogMapper.insertApiLog(hashMap);

		return hashMap.get("resultId").toString();
	}

	public void insertErrorLog(String resultId, String errText, String userId) {
		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("resultId", resultId);
		hashMap.put("errText", errText);
		hashMap.put("userId", userId);

		apiLogMapper.insertErrorLog(hashMap);
	}

}
