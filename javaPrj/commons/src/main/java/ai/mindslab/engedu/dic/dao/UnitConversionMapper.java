package ai.mindslab.engedu.dic.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;

@Mapper
public interface UnitConversionMapper {
	int getCount();

	QaIndexVo searchKeyword(Map<String, String> hashMap);
	
	QaAnswerVo getAnswer(Map<String, String> paramMap);
}
