package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IMindsChatBotCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IntentResolve {
	
	@Autowired
	@Qualifier("korToEngDicIntentExecute")
	private IntentExecute korToEngDicIntentExecute;
	
	@Autowired
	@Qualifier("generalTalkIntentExecute")
	private IntentExecute generalTalkIntentExecute;
	
	@Autowired
	@Qualifier("korDicIntentExecute")
	private IntentExecute korDicIntentExecute;
	
	@Autowired
	@Qualifier("korDicAntonymIntentExecute")
	private IntentExecute korDicAntonymIntentExecute;
	
	@Autowired
	@Qualifier("korDicSynonymIntentExecute")
	private IntentExecute korDicSynonymIntentExecute;
	
	@Autowired
	@Qualifier("mathDicIntentExecute")
	private IntentExecute mathDicIntentExecute;

	@Autowired
	@Qualifier("mindsLabChatBotExecute")
	private IntentExecute mindsLabChatBotExecute;

	@Autowired
	@Qualifier("endToEndIntentExecute")
	private IntentExecute endToEndIntentExecute;

	@Autowired
	@Qualifier("speedGameIntentExecute")
	private IntentExecute speedGameIntentExecute;

	@Autowired
	@Qualifier("timesTableIntentExecute")
	private IntentExecute timesTableIntentExecute;

	@Autowired
	@Qualifier("engFreeTalkIntentExecute")
	private IntentExecute engFreeTalkIntentExecute;

	@Autowired
	@Qualifier("calculateIntentExecute")
	private IntentExecute calculateIntentExecute;

	@Autowired
	@Qualifier("dayIntentExecute")
	private IntentExecute dayIntentExecute;

	@Autowired
	@Qualifier("timeIntentExecute")
	private IntentExecute timeIntentExecute;

	@Autowired
	@Qualifier("dateIntentExecute")
	private IntentExecute dateIntentExecute;
	
	@Autowired
	@Qualifier("unitConversionIntentExecute")
	private IntentExecute unitConversionIntentExecute;

	@Autowired
	@Qualifier("intentMsgService")
	private IntentMsgService intentMsgService;
	
	@Autowired
	private IntentFinderService intentFinderService;
	

	public IntentExecuteMessageVO getIntent(IntentExecuteVO intentExecuteVO) {
		
		
		log.info(new Object(){}.getClass().getEnclosingMethod().getName()+" intentExecuteVO.toString() >>>>"+intentExecuteVO.toString());
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO() ;
		int code = IRestCodes.ERR_CODE_SUCCESS;
		String msg = IRestCodes.ERR_MSG_SUCCESS;
		String errText = null;
		
		try {

			String serviceType = intentExecuteVO.getServiceType();

			//한영사전
			if (IntentServiceType.KORTOENGDIC.equals(serviceType) || IntentServiceType.KORTOENGDIC_FLASH.equals(serviceType) ) {
				ExtInfo extInfo = new ExtInfo();
				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {
					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.KORTOENGDIC, IntentServiceMsg.KORTOENGDIC_START));
					log.info(intentMsgService.getServiceMsg(IntentServiceType.KORTOENGDIC, IntentServiceMsg.KORTOENGDIC_START));
					

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.KORTOENGDIC, IntentServiceMsg.KORTOENGDIC_START_PRINTMENT) );

					
				// 인텐트 실행 	
				} else {
					intentExecuteMessageVO = korToEngDicIntentExecute.execute(intentExecuteVO);
				}

				if(IntentServiceType.KORTOENGDIC.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0015.getDomainName());
				}else if(IntentServiceType.KORTOENGDIC_FLASH.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0029.getDomainName());
				}
				intentExecuteMessageVO.setExtInfo(extInfo);
				
			
			// 국어사전 
			} else if (IntentServiceType.KORDIC.equals(serviceType) || IntentServiceType.KORDIC_FLASH.equals(serviceType) ) {
				ExtInfo extInfo = new ExtInfo();
				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {
					
					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.KORDIC, IntentServiceMsg.KORDIC_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.KORDIC, IntentServiceMsg.KORDIC_START_PRINTMENT) );
					
				// 인텐트 실행 	
				} else {
					intentExecuteMessageVO = korDicIntentExecute.execute(intentExecuteVO);
				}

				if(IntentServiceType.KORDIC.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0016.getDomainName());
				}else if(IntentServiceType.KORDIC_FLASH.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0030.getDomainName());
				}

				intentExecuteMessageVO.setExtInfo(extInfo);


			// 국어사전  반의어
			} else if ( IntentServiceType.KORDICANTONYM.equals(serviceType) || IntentServiceType.KORDICANTONYM_FLASH.equals(serviceType) ) {

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {
					
					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.KORDICANTONYM, IntentServiceMsg.KORDICANTONYM_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.KORDIC, IntentServiceMsg.KORDIC_START_PRINTMENT) );
					
				// 인텐트 실행 	
				} else {
					intentExecuteMessageVO = korDicAntonymIntentExecute.execute(intentExecuteVO);
				}

				if(IntentServiceType.KORDICANTONYM.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0024.getDomainName());
				}else if(IntentServiceType.KORDICANTONYM_FLASH.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0034.getDomainName());
				}
				intentExecuteMessageVO.setExtInfo(extInfo);

				
				
					
			// 국어사전  유의어
			} else if ( IntentServiceType.KORDICSYNONYM.equals(serviceType) || IntentServiceType.KORDICSYNONYM_FLASH.equals(serviceType) ) {

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.KORDICSYNONYM, IntentServiceMsg.KORDICSYNONYM_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.KORDIC, IntentServiceMsg.KORDIC_START_PRINTMENT) );
					
				// 인텐트 실행 	
				} else {
					intentExecuteMessageVO = korDicSynonymIntentExecute.execute(intentExecuteVO);
				}

				if(IntentServiceType.KORDICSYNONYM.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0023.getDomainName());
				}else if(IntentServiceType.KORDICSYNONYM_FLASH.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0033.getDomainName());
				}
				intentExecuteMessageVO.setExtInfo(extInfo);
				
				
			// 수학사전 
			} else if (IntentServiceType.MATHDIC.equals(serviceType) || IntentServiceType.MATHDIC_FLASH.equals(serviceType) ) {

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {
					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.MATHDIC, IntentServiceMsg.MATHDIC_START));
					log.info(intentMsgService.getServiceMsg(IntentServiceType.MATHDIC, IntentServiceMsg.MATHDIC_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.MATHDIC, IntentServiceMsg.MATHDIC_START_PRINTMENT) );
				// 인텐트 실행 	
				} else {
					intentExecuteMessageVO = mathDicIntentExecute.execute(intentExecuteVO);
					extInfo.setPrintMeans(intentExecuteMessageVO.getExtInfo().getPrintMeans());
					extInfo.setUrl(intentExecuteMessageVO.getExtInfo().getUrl());
				}
				if(IntentServiceType.MATHDIC.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0017.getDomainName());
				}else if(IntentServiceType.MATHDIC_FLASH.equals(serviceType)){
					extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0031.getDomainName());
				}
				intentExecuteMessageVO.setExtInfo(extInfo);
				
			// 끝말잇기
			} else if (IntentServiceType.ENDTOEND.equals(serviceType)) {
				intentExecuteMessageVO = endToEndIntentExecute.execute(intentExecuteVO);

			// 탕수육 게임
			} else if (IntentServiceType.SPEEDGAME.equals(serviceType)) {
				intentExecuteMessageVO = speedGameIntentExecute.execute(intentExecuteVO);

			// 구구단 게임
			} else if (IntentServiceType.TIMESTABLE.equals(serviceType)) {
				intentExecuteMessageVO = timesTableIntentExecute.execute(intentExecuteVO);

			// 영어 대화
			} else if (IntentServiceType.ENGFREETALK.equals(serviceType)) {
				intentExecuteMessageVO = engFreeTalkIntentExecute.execute(intentExecuteVO);
			// 계산기
			} else if (IntentServiceType.CALCULATE.equals(serviceType)) {

				ExtInfo extInfo = new ExtInfo();
				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.CALCULATE, IntentServiceMsg.CALCULATE_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.CALCULATE, IntentServiceMsg.CALCULATE_START_PRINTMENT) );
					

					// 인텐트 실행
				} else {
					intentExecuteMessageVO = calculateIntentExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0025.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);

			// 요일
			} else if (IntentServiceType.DAY.equals(serviceType)) {
				intentExecuteMessageVO = dayIntentExecute.execute(intentExecuteVO);

			// 시각
			} else if (IntentServiceType.TIME.equals(serviceType)) {
				intentExecuteMessageVO = timeIntentExecute.execute(intentExecuteVO);

			// 날짜
			} else if (IntentServiceType.DATE.equals(serviceType)) {
				intentExecuteMessageVO = dateIntentExecute.execute(intentExecuteVO);

			// 일반대화
			} else if (IntentServiceType.BASICTALK.equals(serviceType)) {
				intentExecuteMessageVO = generalTalkIntentExecute.execute(intentExecuteVO);
				
			// 백과사전 
			} else if(IntentServiceType.ENCYCLOPDIC.equals(serviceType)) {


				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.ENCYCLOPDIC, IntentServiceMsg.ENCYCLOPDIC_MAKE_MENT));

					extInfo.setPrintMeans(intentMsgService.getServiceMsg(IntentServiceType.ENCYCLOPDIC, IntentServiceMsg.ENCYCLOPDIC_START_PRINTMENT));


					// 인텐트 실행
				} else {
					intentExecuteVO.setChatBotType(IMindsChatBotCodes.NOAH);
					intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0020.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);

				// 날씨
			}else if(IntentServiceType.WEATHER.equals(serviceType)) {

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.WEATHER, IntentServiceMsg.WEATHER_MAKE_MENT));

					extInfo.setPrintMeans(intentMsgService.getServiceMsg(IntentServiceType.WEATHER, IntentServiceMsg.WEATHER_START_PRINTMENT));


					// 인텐트 실행
				} else {
					intentExecuteVO.setChatBotType(IMindsChatBotCodes.WEATHER);
					intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0041.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);

				// 미세 먼지
			}else if(IntentServiceType.DUST.equals(serviceType)){

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.DUST, IntentServiceMsg.DUST_MAKE_MENT));

					extInfo.setPrintMeans(intentMsgService.getServiceMsg(IntentServiceType.DUST, IntentServiceMsg.DUST_START_PRINTMENT));


					// 인텐트 실행
				} else {
					intentExecuteVO.setChatBotType(IMindsChatBotCodes.DUST);
					intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0042.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);

				// 환율
			}else if(IntentServiceType.EXCHANGE_RATE.equals(serviceType)){

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.EXCHANGE_RATE, IntentServiceMsg.EXCHANGE_RATE_MAKE_MENT));

					extInfo.setPrintMeans(intentMsgService.getServiceMsg(IntentServiceType.EXCHANGE_RATE, IntentServiceMsg.EXCHANGE_RATE_START_PRINTMENT));


					// 인텐트 실행
				} else {
					intentExecuteVO.setChatBotType(IMindsChatBotCodes.EXCHANGE_RATE);
					intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0043.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);

				// 제철음식
			}else if(IntentServiceType.SEASONAL_FOOD.equals(serviceType)){

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.SEASONAL_FOOD, IntentServiceMsg.SEASONAL_FOOD_MAKE_MENT));

					extInfo.setPrintMeans(intentMsgService.getServiceMsg(IntentServiceType.SEASONAL_FOOD, IntentServiceMsg.SEASONAL_FOOD_START_PRINTMENT));

					// 인텐트 실행
				} else {
					intentExecuteVO.setChatBotType(IMindsChatBotCodes.SEASONAL_FOOD);
					intentExecuteMessageVO = mindsLabChatBotExecute.execute(intentExecuteVO);
				}
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0044.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);


				// 단위변환
			} else if(IntentServiceType.UNIT_CONVERSION.equals(serviceType)){

				ExtInfo extInfo = new ExtInfo();

				// if 첫서비스 진입이면 환영 답변
				if (intentExecuteVO.isFirstServiceEntry()) {

					intentExecuteMessageVO = new IntentExecuteMessageVO();
					intentExecuteMessageVO.setResultMsg(intentMsgService.getServiceMsg(IntentServiceType.UNIT_CONVERSION, IntentServiceMsg.UNIT_CONVERSION_START));

					extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.UNIT_CONVERSION, IntentServiceMsg.UNIT_CONVERSION_START_PRINTMENT) );
					

					// 인텐트 실행
				} else {
					intentExecuteMessageVO = unitConversionIntentExecute.execute(intentExecuteVO);
				}

				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0040.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);
				
			}
			
		} catch (EngEduException e) {
//			e.printStackTrace();
			code = e.getErrCode();
			msg = e.getErrMsg();
			errText = ExceptionUtils.getStackTrace(e);
		} finally {
			purgeServiceType(intentExecuteVO, code);

			intentExecuteMessageVO.setCode(code);
			intentExecuteMessageVO.setMsg(msg);

			if(code == IRestCodes.ERR_CODE_FAILURE) {
				intentExecuteMessageVO.setErrText(errText);
			}

			if("QUESTION NOT FOUND".equals(msg)){
				ExtInfo extInfo = new ExtInfo();
				extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0022.getDomainName());
				intentExecuteMessageVO.setExtInfo(extInfo);
			}

		}
        return intentExecuteMessageVO;
	}
	
	private void purgeServiceType(IntentExecuteVO intentExecuteVO, int code) {

		String serviceType = intentExecuteVO.getServiceType();
		if(code == IRestCodes.ERR_CODE_SUCCESS){
			switch(serviceType) {
				case IntentServiceType.KORTOENGDIC_FLASH:
				case IntentServiceType.KORDIC_FLASH:
				case IntentServiceType.MATHDIC_FLASH:
				case IntentServiceType.ENCYCLOPDIC_FLASH:
				case IntentServiceType.KORDICSYNONYM_FLASH:
				case IntentServiceType.KORDICANTONYM_FLASH:
				case IntentServiceType.ENGFREETALK:
//				case IntentServiceType.CALCULATE:
				case IntentServiceType.DAY:
				case IntentServiceType.TIME:
				case IntentServiceType.DATE:
				case IntentServiceType.BASICTALK:
					intentFinderService.purgeServiceType(intentExecuteVO.getUserId());
					break;
			}
		} else {
			String exitType = intentExecuteVO.getExitType();

			switch(serviceType) {
				case IntentServiceType.TIMESTABLE:
					if(!StringUtils.isEmpty(exitType) && StringUtils.equals(exitType, "T")) {
						return;	// 구구단 타임 아웃일시에는 purge 안함
					}
			}
			intentFinderService.purgeServiceType(intentExecuteVO.getUserId());
		}
	}

	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO() ;
		int code = IRestCodes.ERR_CODE_SUCCESS;
		String msg = IRestCodes.ERR_MSG_SUCCESS;
		String errText = null;

		String serviceType = intentExecuteVO.getServiceType();

		try {
			switch(serviceType) {
				// 끝말 잇기 로직
				case IntentServiceType.ENDTOEND:
					intentExecuteMessageVO = endToEndIntentExecute.forceQuit(intentExecuteVO);
					break;
				// 탕수육 게임 로직
				case IntentServiceType.SPEEDGAME:
					intentExecuteMessageVO = speedGameIntentExecute.forceQuit(intentExecuteVO);
					break;
				// 구구단 게임 로직
				case IntentServiceType.TIMESTABLE:
					intentExecuteMessageVO = timesTableIntentExecute.forceQuit(intentExecuteVO);
					break;
				// 게임외의 로직
				default:
					intentExecuteMessageVO = new IntentExecuteMessageVO();
					break;
			}

			// (공통 로직): 서비스상태 null로 변경
//			purgeServiceType(intentExecuteVO, IRestCodes.ERR_CODE_FAILURE);

		} catch (EngEduException e) {
//			e.printStackTrace();
			code = e.getErrCode();
			msg = e.getErrMsg();
			errText = ExceptionUtils.getStackTrace(e);
		} finally {
			// (공통 로직): 서비스상태 null로 변경
			purgeServiceType(intentExecuteVO, IRestCodes.ERR_CODE_FAILURE);

			intentExecuteMessageVO.setCode(code);
			intentExecuteMessageVO.setMsg(msg);

			if(code == IRestCodes.ERR_CODE_FAILURE) {
				intentExecuteMessageVO.setErrText(errText);
			}
		}
        return intentExecuteMessageVO;
	}
}
