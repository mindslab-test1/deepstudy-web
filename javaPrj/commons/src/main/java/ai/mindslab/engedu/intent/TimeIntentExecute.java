package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

@Slf4j
@Component
public class TimeIntentExecute implements IntentExecute {
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
//			String inputStr = intentExecuteVO.getInputStr();

			// 현재는 지금만
			Calendar cal = Calendar.getInstance();
			int hour = cal.get(Calendar.HOUR);
			int minute = cal.get(Calendar.MINUTE);

			if (hour == 0) {
				hour = 12;
			}
			String resultMsg = "지금은 " + hour + "시 " + minute + "분이야";
			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0027.getDomainName());
			log.info("TimeIntentExecute ResultMsg:" + resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);
			intentExecuteMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return null;
	}
}