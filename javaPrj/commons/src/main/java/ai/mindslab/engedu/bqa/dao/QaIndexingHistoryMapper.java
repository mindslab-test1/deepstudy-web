package ai.mindslab.engedu.bqa.dao;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaIndexingHistoryVo;

@Mapper
public interface QaIndexingHistoryMapper {

	int insert(QaIndexingHistoryVo qaIndexingHistoryVo);
	
	int update(QaIndexingHistoryVo qaIndexingHistoryVo);
}
