package ai.mindslab.engedu.bqa.commons.utils;

import java.util.HashMap;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class PagingHelper {
	
	private PageInfo pageInfo(int pg, int pgSize) {
		return new PageInfo(pg, pgSize);
	}
	
	public HashMap<String,Object> setPageInfo(HashMap<String,Object> params, int pg, int pgSize) {
		HashMap<String,Object> p;
		if(params == null) {
			p = new HashMap<>();
		}else {
			p = params;
		}
		PageInfo pi = this.pageInfo(pg, pgSize);
		p.put("startIndex", pi.getStartIndex());
		p.put("pgSize", pgSize);
		return p;
	}

	@Data
	@NoArgsConstructor
	public static class PageInfo{
		private int startIndex;
		private int pgSize;
		
		PageInfo(int pg, int pgSize) {
			this.pgSize = pgSize;
			this.startIndex = (pg - 1) * pgSize;
		}
	}

}
