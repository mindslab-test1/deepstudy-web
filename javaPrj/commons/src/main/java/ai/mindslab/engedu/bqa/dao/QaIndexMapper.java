package ai.mindslab.engedu.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;

@Mapper
public interface QaIndexMapper {

	int insertList(Map<String, Object> paramMap);

	int updateUseYn(Map<String, Object> paramMap);

	int[] getRemoveList();
	
	int count();
	
	int getAddIdxCount();
	
	QaIndexVo search(Map<String, Object> paramMap);

	QaIndexVo searchKeyword(Map<String, Object> paramMap);

	QaIndexVo searchFullText(Map<String, Object> paramMap);

	List<QaIndexVo> getsByDomainId(int domainId);
	
	int deleteAll();

	List<QaIndexVo> deleteOneQuestionIndex(int questionId);

	int updateAutoIncrement(Map<String, Object> paramMap);
}
