package ai.mindslab.engedu.admin.qa.index.dao;

import ai.mindslab.engedu.admin.qa.index.dao.data.QAIndexAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface QAIndexAdminMapper {

    int getIndexHisgoryAdminListCount(Map<String, Object> param) throws EngEduException;
    List<QAIndexAdminVO> getIndexHisgoryAdminList(Map<String, Object> param) throws EngEduException;
}
