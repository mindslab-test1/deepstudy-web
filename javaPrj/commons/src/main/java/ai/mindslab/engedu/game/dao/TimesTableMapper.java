package ai.mindslab.engedu.game.dao;

import ai.mindslab.engedu.game.dao.data.TimesTableVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface TimesTableMapper {

    TimesTableVO getQuestion(Map<String, Object> hashMap);

    int insertUsedQuestion(Map<String, Object> hashMap);

    TimesTableVO getAnswer(Map<String, Object> hashMap);

    int updateUsedQuestionStatus(Map<String, Object> hashMap);

    Integer getSeqCorrectCount(Map<String, Object> hashMap);

    int deleteUsedQuestions(Map<String, Object> hashMap);

}
