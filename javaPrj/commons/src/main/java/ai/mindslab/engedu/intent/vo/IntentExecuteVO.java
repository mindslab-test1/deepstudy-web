package ai.mindslab.engedu.intent.vo;

import java.io.Serializable;
import java.util.List;

import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import lombok.Data;

@Data
public class IntentExecuteVO implements Serializable {
	private String userId;
	private String inputStr;
	private String serviceType;
	private boolean isFirstServiceEntry;
	private String exitType;
	private String chatBotType;
	
	private EngFreeTalkQuestionVO engFreeTalkQuestionVO;
	

}
