package ai.mindslab.engedu.dic.service;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;

@Slf4j
public class MindsLabChatBotService {

    public enum ChatBotURLEnum{

        AUTH_TOKEN("https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn"),
        OPEN_DIALOG("https://aicc-prd1.maum.ai:9980/api/v3/dialog/open"),
        TEXT_TO_TEXT_TALK("https://aicc-prd1.maum.ai:9980/api/v3/dialog/textToTextTalk"),
        CLOSE_DIALOG("https://aicc-prd1.maum.ai:9980/api/v3/dialog/close");

        private String url;

        ChatBotURLEnum(String name){ this.url = name; }

        public String getURL(){ return url; }

    }

    public String getChatBotAnswer(String paramUserId,String paramInputStr,String chatBotType){

        // kbqa api 연동은 m2u_v2_api 문서 참조

        HttpHeaders headers = getHeaders();

        log.debug("MindsLabChatBotService start");

        String authToken = getAuthToken(headers,chatBotType);

        log.debug("MindsLabChatBotService authToken  : " + authToken);

        String deviceId = getDeviceId(paramUserId);

        openDialog(headers,authToken,deviceId,chatBotType);

        log.debug("MindsLabChatBotService openDialog");

        JSONObject jsonObj = getTextToTextTalk(headers,authToken,paramInputStr,deviceId,chatBotType);

        log.debug("MindsLabChatBotService getTextToTextTalk {} : " + jsonObj.toString());

        String resultMsg = getResultMsg(jsonObj);

        closeDialog(headers,authToken,deviceId,chatBotType);

        log.debug("MindsLabChatBotService closeDialog");

        return resultMsg;

    }

    private HttpHeaders getHeaders() {

        HttpHeaders headers = new HttpHeaders();

        headers.set("Content-Type","application/json");
        headers.set("m2u-auth-internal","m2u-auth-internal");
        headers.set("Accept-Charset", "UTF-8");

        return headers;

    }

    @SuppressWarnings("unchecked")
    private String getResultMsg(JSONObject paramJsonObj) {

        String result;

        HashMap<String,Object> directive = (HashMap<String,Object>) paramJsonObj.get("directive");

        HashMap<String,Object> payload = directive != null ? (HashMap<String, Object>)  directive.get("payload") :null;
        HashMap<String,Object> response = payload != null ? (HashMap<String, Object>)  payload.get("response") :null;
        HashMap<String,Object> speech = response != null ? (HashMap<String, Object>)  response.get("speech") :null;
        result = speech!= null ? String.valueOf(speech.get("utter")) : "";

        return result;

    }

    @SuppressWarnings("unchecked")
    private String getAuthToken(HttpHeaders paramHeaders,String paramChatBotType){

        String authToken = "";

        try{

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            URI uri = URI.create(MindsLabChatBotService.ChatBotURLEnum.AUTH_TOKEN.getURL());

            JSONObject param = getParamJson(MindsLabChatBotService.ChatBotURLEnum.AUTH_TOKEN.toString(),"","","",paramChatBotType);

            HttpEntity<JSONObject> httpEntityRequest = new HttpEntity<>(param,paramHeaders);

            HashMap<String,Object> result = restTemplate.postForObject(uri,httpEntityRequest,HashMap.class);

            HashMap<String,Object> directive = (HashMap<String, Object>) result.get("directive");
            HashMap<String,Object> payload = directive != null ? (HashMap<String, Object>)  directive.get("payload") :new HashMap<>();
            HashMap<String,Object> authSuccess = payload != null ? (HashMap<String, Object>)  payload.get("authSuccess") :new HashMap<>();
            authToken = authSuccess != null ? String.valueOf(authSuccess.get("authToken")) : authToken;

        }catch(Exception e){

            e.printStackTrace();
            log.error("getAuthToken Error : {} " + e.getMessage());

        }

        return authToken;

    }

    private void openDialog(HttpHeaders paramHeaders,String paramAuthToken,String paramDeviceId,String paramChatBotType) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        URI uri = URI.create(MindsLabChatBotService.ChatBotURLEnum.OPEN_DIALOG.getURL());

        JSONObject param = getParamJson(
                MindsLabChatBotService.ChatBotURLEnum.OPEN_DIALOG.toString(),
                paramAuthToken,
                "",
                paramDeviceId,
                paramChatBotType);

        HttpEntity<JSONObject> httpEntityRequest = new HttpEntity<>(param,paramHeaders);

        restTemplate.postForObject(uri,httpEntityRequest,JSONObject.class);

    }

    private JSONObject getTextToTextTalk(
            HttpHeaders paramHeaders,
            String paramAuthToken,
            String paramInputStr,
            String paramDeviceId,
            String paramChatBotType)
    {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        URI uri = URI.create(MindsLabChatBotService.ChatBotURLEnum.TEXT_TO_TEXT_TALK.getURL());

        JSONObject param = getParamJson(
                MindsLabChatBotService.ChatBotURLEnum.TEXT_TO_TEXT_TALK.toString(),
                paramAuthToken,
                paramInputStr,
                paramDeviceId,
                paramChatBotType
        );

        HttpEntity<JSONObject> httpEntityRequest = new HttpEntity<>(param,paramHeaders);

        JSONObject jsonObj = restTemplate.postForObject(uri,httpEntityRequest,JSONObject.class);

        return jsonObj;

    }

    private void closeDialog(HttpHeaders paramHeaders,String paramAuthToken,String paramDeviceId,String paramChatBotType) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        URI uri = URI.create(MindsLabChatBotService.ChatBotURLEnum.CLOSE_DIALOG.getURL());

        JSONObject param = getParamJson(
                MindsLabChatBotService.ChatBotURLEnum.CLOSE_DIALOG.toString(),
                paramAuthToken,
                "",
                paramDeviceId,
                paramChatBotType
        );

        HttpEntity<JSONObject> httpEntityRequest = new HttpEntity<>(param,paramHeaders);

        restTemplate.postForObject(uri,httpEntityRequest,JSONObject.class);

    }

    /**
     * device Id 값 return userId + _ + 현재시간 ex) risingsuntae_20180905092708
     * @param paramUserId 사용자 id
     * @return device id
     */
    private String getDeviceId(String paramUserId){

        String result = paramUserId + IMarkCodes.UNDERSCORE +new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
        return result;

    }

    /**
     * api 호출 시 보낼 parameter
     * @param paramType auth or open dialog or text to text talk or close
     * @param paramAuthToken auth token 값
     * @param paramInputStr 학습자 문장
     * @param paramDeviceId device id
     * @return api 호출 parameter
     */
    private JSONObject getParamJson(String paramType,String paramAuthToken,String paramInputStr,String paramDeviceId,String paramChatBotType) {

        JSONObject result = new JSONObject();
        result.put("authToken",paramAuthToken);

        if(paramType.equals(MindsLabChatBotService.ChatBotURLEnum.AUTH_TOKEN.toString())){

            result.put("userKey","admin");
            result.put("passphrase","1234");

        }else if(paramType.equals(MindsLabChatBotService.ChatBotURLEnum.OPEN_DIALOG.toString())){

            HashMap<String,Object> payloadMap = new HashMap<>();
            payloadMap.put("utter","UTTER1");
            payloadMap.put("chatbot",paramChatBotType);
            payloadMap.put("skill","SKILL1");

            result.put("payload",payloadMap);


        }else if(paramType.equals(MindsLabChatBotService.ChatBotURLEnum.TEXT_TO_TEXT_TALK.toString())){

            HashMap<String,Object> payloadMap = new HashMap<>();
            payloadMap.put("utter",paramInputStr);
            payloadMap.put("lang","ko_KR");

            result.put("payload",payloadMap);

        }

        if(paramType.equals(MindsLabChatBotService.ChatBotURLEnum.OPEN_DIALOG.toString())
                || paramType.equals(MindsLabChatBotService.ChatBotURLEnum.TEXT_TO_TEXT_TALK.toString())
                || paramType.equals(MindsLabChatBotService.ChatBotURLEnum.CLOSE_DIALOG.toString())){

            HashMap<String,Object> locationMap = new HashMap<>();
            locationMap.put("latitude","10.3");
            locationMap.put("longitude","20.5");
            locationMap.put("location","mindslab");
            result.put("location",locationMap);

            HashMap<String,Object> deviceMap = new HashMap<>();
            deviceMap.put("id",paramDeviceId);
            deviceMap.put("type","WEB");
            deviceMap.put("version","0.1");
            deviceMap.put("channel","ADMINWEB");
            result.put("device",deviceMap);

        }

        log.debug("getParamJson paramType : " + paramType);
        log.debug("getParamJson param : {}" + result);

        return result;

    }

}
