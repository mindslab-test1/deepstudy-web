package ai.mindslab.engedu.bqa.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.QaQnaParaphraseVo;

@Mapper
public interface QaQnaParaphraseMapper {
	
	List<QaQnaParaphraseVo> getsParaphrase();
}
