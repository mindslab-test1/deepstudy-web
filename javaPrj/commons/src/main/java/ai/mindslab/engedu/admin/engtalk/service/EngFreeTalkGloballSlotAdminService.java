package ai.mindslab.engedu.admin.engtalk.service;

import ai.mindslab.engedu.admin.engtalk.dao.EngFreeTalkGlobalSlotAdminMapper;
import ai.mindslab.engedu.admin.engtalk.dao.EngFreeTalkLocalSlotAdminMapper;
import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EngFreeTalkGloballSlotAdminService {

    @Autowired
    EngFreeTalkGlobalSlotAdminMapper mapper;

    public int engTalkGlobalSlotAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engTalkGlobalSlotAdminCount(paramMap);
    }


    public List<EngTalkGlobalSlotAdminVO> engTalkGlobalSlotAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engTalkGlobalSlotAdminList(paramMap);
    }


    public List<EngTalkGlobalSlotAdminVO> engTalkGlobalSlotExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engTalkGlobalSlotExcelList(paramMap);
    }

    public EngTalkGlobalSlotAdminVO engTalkGlobalSlotAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.engTalkGlobalSlotAdmin(paramMap);
    }


    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public BaseResponse<Object> updateEngTalkSlotAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("data").toString();
            String userId = paramMap.get("user_id").toString();

            EngTalkGlobalSlotAdminVO vo =  new Gson().fromJson(unique, EngTalkGlobalSlotAdminVO.class);


            int delResult = 0;
            int result = 0;

            delResult = mapper.deleteGlobalSlotAdmin(vo);

            if(delResult > -1) {

                String[] slotValues = vo.getSlotValue().split(",");

                for(String slotValue : slotValues){
                    EngTalkGlobalSlotAdminSVO svo = new EngTalkGlobalSlotAdminSVO();
                    svo.setData(vo);
                    svo.setSlotValue(slotValue);
                    svo.setCreatorId(userId);
                    svo.setUpdatorId(userId);

                    result = mapper.insertGlobalSlotAdmin(svo);

                }
            }


        } catch (Exception e){
            e.printStackTrace();
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    public BaseResponse<Object> deleteEngTalkSlotAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("unique").toString();

            EngTalkGlobalSlotAdminVO[] paramList =  new Gson().fromJson(unique, EngTalkGlobalSlotAdminVO[].class);
            int  result =0;

            for(EngTalkGlobalSlotAdminVO vo : paramList){
                vo.setOldSlotKey(vo.getSlotKey());
                result = mapper.deleteGlobalSlotAdmin(vo);
            }

        } catch (Exception e){
            e.printStackTrace();
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int cellCount = 0;
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 2) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        EngTalkGlobalSlotAdminSVO svo = new EngTalkGlobalSlotAdminSVO();


                        svo.setSlotKey(ExcelUtils.getCellValue(row.getCell(0)));
                        svo.setSlotValue(ExcelUtils.getCellValue(row.getCell(1)));

                        svo.setCreatorId(vo.getUserId());
                        svo.setUpdatorId(vo.getUserId());

                        mapper.insertGlobalSlotAdmin(svo);

                    }
                }
            }
        }catch (Exception e){
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }


}
