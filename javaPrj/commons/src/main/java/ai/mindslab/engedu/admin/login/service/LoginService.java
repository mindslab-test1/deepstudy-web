package ai.mindslab.engedu.admin.login.service;

import ai.mindslab.engedu.admin.login.dao.LoginMapper;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LoginService {

    @Autowired
    LoginMapper mapper;

    public UserVO loginUser(String id){
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("id", id);
        return mapper.loginUser(paramMap);
    }
}
