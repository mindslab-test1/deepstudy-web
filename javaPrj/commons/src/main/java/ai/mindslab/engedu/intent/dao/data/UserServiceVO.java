package ai.mindslab.engedu.intent.dao.data;

import java.util.Date;

import lombok.Data;

@Data
public class UserServiceVO {
	
	private String userId;
	private String serviceType;
	private Date serviceStartTime;
	private Date serviceLimitTime;
	private Date currentTime;
	
	
	private String currentTimeStr;
	private String limitTimeStr;
		
	

}
