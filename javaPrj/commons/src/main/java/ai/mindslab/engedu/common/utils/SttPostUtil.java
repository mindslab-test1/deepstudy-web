package ai.mindslab.engedu.common.utils;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;

public class SttPostUtil{

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private class SttPostTargetToChangeWordVO implements Serializable {

        private String targetStr;
        private String changeStr;

    }

    /**
     * stt 후처리 ex) 백과 사전 -> 백과사전으로 replace
     * @param paramInputStr
     * @return
     */
    public String getSttPostStr(String paramInputStr){

        ArrayList<SttPostTargetToChangeWordVO> targetList = getTargetList();

        String result = paramInputStr;

        for ( SttPostTargetToChangeWordVO sttPostTargetToChangeWordVO: targetList) {

            if(paramInputStr.contains(sttPostTargetToChangeWordVO.getTargetStr())){
                result = result.replaceAll(sttPostTargetToChangeWordVO.getTargetStr(),sttPostTargetToChangeWordVO.getChangeStr());
            }
        }

        return result;

    }

    /**
     * STT replace 변환 대상 List
     * @return
     */
    private ArrayList<SttPostTargetToChangeWordVO> getTargetList() {

        ArrayList<SttPostTargetToChangeWordVO> result = new ArrayList<>();

        result.add(new SttPostTargetToChangeWordVO("\n"," "));
        result.add(new SttPostTargetToChangeWordVO("백과 사전","백과사전"));

        return result;

    }
}