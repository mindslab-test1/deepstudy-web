package ai.mindslab.engedu.admin.engtalk.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngTalkKeyCodeVO  implements Serializable {

    private String brandId;
    private String bookId;
    private String chapterId;
    private String questionId;
}
