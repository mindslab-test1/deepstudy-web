package ai.mindslab.engedu.admin.qa.index.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class QAIndexAdminVO implements Serializable {

    private int historyId;
    private String htype;
    private int total;
    private String indexStartTime;
    private String committedTime;
    private String takenTime;


}
