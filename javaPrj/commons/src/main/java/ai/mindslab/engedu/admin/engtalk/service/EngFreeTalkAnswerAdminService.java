package ai.mindslab.engedu.admin.engtalk.service;

import ai.mindslab.engedu.admin.engtalk.dao.EngFreeTalkAnswerAdminMapper;
import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAnswerKeyCodeVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EngFreeTalkAnswerAdminService {

    @Autowired
    EngFreeTalkAnswerAdminMapper mapper;


    public int EngTalkAnswerAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.EngTalkAnswerAdminCount(paramMap);
    }

    public List<EngTalkAdminAnswerVO> EngTalkAnswerAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.EngTalkAnswerAdminList(paramMap);
    }


    public List<EngTalkAdminAnswerVO> EngTalkAnswerExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.EngTalkAnswerExcelList(paramMap);
    }


    public EngTalkAdminAnswerVO EngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException{
        String unique = paramMap.get("unique").toString();
        List<String> keys =  Arrays.stream(unique.split(",")).collect(Collectors.toList());

        paramMap.put("keys", keys);

        return mapper.EngTalkAnswerAdmin(paramMap);
    }


    public BaseResponse<Object> updateEngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("unique").toString();
            List<String> keys =  Arrays.stream(unique.split(",")).collect(Collectors.toList());

            paramMap.put("keys", keys);

            int result = mapper.updateEngTalkAnswerAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    public BaseResponse<Object> deleteEngTalkAnswerAdmin(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("unique").toString();

            EngTalkAnswerKeyCodeVO[] keys =  new Gson().fromJson(unique, EngTalkAnswerKeyCodeVO[].class);

            paramMap.put("keys", keys);

            int result = mapper.deleteEngTalkAnswerAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        } catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int cellCount = 0;
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 5) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {
                        //sequence 를 가져온다.
                        int answerId = mapper.getEngTalkAnswerId();

                        EngTalkAdminAnswerSVO svo = new EngTalkAdminAnswerSVO();

                        svo.setBrandId(ExcelUtils.getCellValue(row.getCell(0)));
                        svo.setBookId(ExcelUtils.getCellValue(row.getCell(1)));
                        svo.setChapterId(ExcelUtils.getCellValue(row.getCell(2)));
                        svo.setQuestionId(ExcelUtils.getCellValue(row.getCell(3)));
                        svo.setAnswerText(ExcelUtils.getCellValue(row.getCell(4)));

                        svo.setAnswerId(answerId);
                        svo.setCreatorId(vo.getUserId());
                        svo.setUpdatorId(vo.getUserId());

                        mapper.insertEngTalkAnswerAdmin(svo);

                    }
                }
            }
        }catch (Exception e){
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }
}
