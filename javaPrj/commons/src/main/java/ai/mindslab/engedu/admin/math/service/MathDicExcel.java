package ai.mindslab.engedu.admin.math.service;

import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import ai.mindslab.engedu.admin.korengdic.service.KorToEngDicAdminService;
import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MathDicExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    MathDicAdminService mathDicAdminService;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 2000, 3800, 7000, 5000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "단어", "뜻", "URL"};

       List<MathDicAdminVO> list = mathDicAdminService.getExcelMathDicAdminList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;

        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(MathDicAdminVO vo : list){
            String[] data = {
                    vo.getMathId()+"",
                    vo.getWord(),
                    vo.getMeans(),
                    vo.getUrl()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}
