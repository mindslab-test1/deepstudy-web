package ai.mindslab.engedu.evaluation.dao.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GrammarEvaluationAbbreviationVO {

    /** BP CODE */
    private String brandId;
    /** 축약형 */
    private String abbreviationText;
    /** 원형 */
    private String originalText;
    /** 사용여부 */
    private String useYn;

    public GrammarEvaluationAbbreviationVO(String originalText,String abbreviationText){
        this.abbreviationText = abbreviationText;
        this.originalText = originalText;
    }

}
