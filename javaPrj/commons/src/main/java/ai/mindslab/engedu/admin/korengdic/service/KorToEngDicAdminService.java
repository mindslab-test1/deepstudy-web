package ai.mindslab.engedu.admin.korengdic.service;


import ai.mindslab.engedu.admin.korengdic.dao.KorToEngDicAdminMapper;
import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class KorToEngDicAdminService {

	@Autowired
	private KorToEngDicAdminMapper korToEngDicAdminMapper;

	public int getCount() throws EngEduException {
		//log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+kortoEngDicMapper.getCount());
		return korToEngDicAdminMapper.getCount();
	}

	public int getDicAdminCount(Map<String, Object> paramMap) throws EngEduException {
		//log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+kortoEngDicMapper.getCount());
		return korToEngDicAdminMapper.getDicAdminCount(paramMap);
	}
	
	
	public List<KorToEngDicAdminVO> getSearch(String searchKey) throws EngEduException {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return korToEngDicAdminMapper.getSearch(hashMap);
	}

	public List<KorToEngDicAdminVO> getDicAdminList(Map<String, Object> paramMap) throws EngEduException {
		return korToEngDicAdminMapper.getDicAdminList(paramMap);
	}

	public KorToEngDicAdminVO selectDicById(Map<String, Object> paramMap) throws  EngEduException{
		return korToEngDicAdminMapper.selectDetail(paramMap);
	}

	public BaseResponse<Object> updateKorengDetail(Map<String, Object> paramMap) {
		BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
		JsonObject obj = new JsonObject();
		try {
			int result = korToEngDicAdminMapper.updateKorengDetail(paramMap);

			if(result > -1){
				resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
				resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
			}

		} catch (Exception e){
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}

		return resp;
	}


	public BaseResponse<Object> deleteKorEngDic(Map<String, Object> paramMap) {
		BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
		JsonObject obj = new JsonObject();
		try {
			int result = korToEngDicAdminMapper.deleteKorEngDic(paramMap);

			if(result > -1){
				resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
				resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
			}

		} catch (Exception e){
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}

		return resp;
	}


	public List<KorToEngDicAdminVO> getExcelDicAdminList(Map<String, Object> paramMap) throws  EngEduException {
		return korToEngDicAdminMapper.getExcelDicAdminList(paramMap);
	}

	@Transactional(rollbackFor = { Exception.class })
	public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
		BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

		Workbook workbook = ExcelUtils.getExcelWorkBook(file);
		Sheet sheet = (Sheet) workbook.getSheetAt(0);

		int rowCount = sheet.getPhysicalNumberOfRows();
		int cellCount = 0;
		int lastCellNum = 0;

		try {

			for (int i = 0; i < rowCount; i++) {
				Row row = sheet.getRow(i);

				if (row != null) {
					cellCount = row.getPhysicalNumberOfCells();

					if (i == 0) {
						lastCellNum = row.getLastCellNum();

						if (lastCellNum != 3) {
							resp.setCode(IRestCodes.ERR_CODE_FAILURE);
							resp.setMsg("Excel 양식이 잘못되었습니다.");
							break;
						}
					} else {

						KorToEngDicAdminVO kVo = new KorToEngDicAdminVO();

						kVo.setKor(ExcelUtils.getCellValue(row.getCell(0)));
						kVo.setEng(ExcelUtils.getCellValue(row.getCell(1)));
						kVo.setPriority(Integer.parseInt(ExcelUtils.getCellValue(row.getCell(2))));

						korToEngDicAdminMapper.insertKorEngDic(kVo);

					}
				}
			}

		}catch (Exception e){
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}

		return resp;
	}

}
