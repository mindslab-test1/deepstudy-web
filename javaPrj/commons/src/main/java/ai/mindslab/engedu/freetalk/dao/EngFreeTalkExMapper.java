package ai.mindslab.engedu.freetalk.dao;

import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkAnswerVO;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkGlobalSlotVO;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkLocalSlotVO;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkExMapper {

    EngFreeTalkQuestionVO getQuestion(Map<String, Object> hashMap);

    List<EngFreeTalkAnswerVO> getAnswerList(Map<String, Object> hashMap);

    List<EngFreeTalkLocalSlotVO> getLocalSlotList(Map<String, Object> hashMap);

    List<String> getLocalSlotValueList(Map<String, Object> hashMap);

    List<EngFreeTalkGlobalSlotVO> getGlobalSlotList(Map<String, Object> hashMap);

    List<String> getGlobalSlotValueList(Map<String, Object> hashMap);
}
