package ai.mindslab.engedu.bqa.commons.codes;

public interface IBasicQAConst {

	/** DB SEARCH */
	public static final int SEARCH_TYPE_DB = 2;
	/** SOLR AND SEARCH */
	public static final int SEARCH_TYPE_SOLRAND = 3;
	 /** DB 검색 후 데이터 없으면 SOLR  AND 검색*/
	public static final int SEARCH_TYPE_DB_SOLRAND = 1;
	/** DB 검색 후 데이터 없으면 SOLR AND 검색 후 데이터 없으면 SOLR OR 검색  */
	public static final int SEARCH_TYPE_DB_SOLRAND_SOLROR = 4;
	public final static int SEARCH_RESULT_DB_FAIL = -1;
	public final static int SEARCH_RESULT_DB_SUCCESS = 1;
	public final static int SEARCH_RESULT_SOLRAND_FAIL = -2;
	public final static int SEARCH_RESULT_SOLRAND_SUCCESS = 2;
	public final static int SEARCH_RESULT_SOLROR_FAIL = -3;
	public final static int SEARCH_RESULT_SOLROR_SUCCESS = 3;

}
