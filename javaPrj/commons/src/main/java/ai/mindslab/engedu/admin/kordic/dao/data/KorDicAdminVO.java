package ai.mindslab.engedu.admin.kordic.dao.data;

import lombok.Data;

@Data
public class KorDicAdminVO {

    private String korId;
    private String word;
    private String means;
    private String printMeans;
    private String url;
    private int priority;

}
