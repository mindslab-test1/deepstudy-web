package ai.mindslab.engedu.dic.dao.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class KorToEngDicVO implements Serializable {
	private String korId;
	private String kor;
	private String eng;
	private int priority;
	

}
