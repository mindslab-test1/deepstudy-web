/*
 * Diff Match and Patch
 * Copyright 2018 The diff-match-patch Authors.
 * https://github.com/google/diff-match-patch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ai.mindslab.engedu.common.utils;

import java.util.*;

/*
 * Functions for diff, match and patch.
 * Computes the difference between two texts to create a patch.
 * Applies the patch onto another text, allowing for errors.
 *
 * @author fraser@google.com (Neil Fraser)
 */

/**
 * Class containing the diff, match and patch methods.
 * Also contains the behaviour settings.
 */
public class DiffMatchPatchUtil {

  /**
   * Number of seconds to map a diff before giving up (0 for infinity).
   */
  private float Diff_Timeout = 1.0f;

  /**
   * The data structure representing a diff is a Linked list of Diff objects:
   * {Diff(Operation.DELETE, "Hello"), Diff(Operation.INSERT, "Goodbye"),
   *  Diff(Operation.EQUAL, " world.")}
   * which means: delete "Hello", add "Goodbye" and keep " world."
   */
  public enum Operation {
    DELETE, INSERT, EQUAL
  }

  /**
   * Class representing one diff operation.
   */
  public static class Diff {
    /**
     * One of: INSERT, DELETE or EQUAL.
     */
    public Operation operation;
    /**
     * The text associated with this diff operation.
     */
    public String text;

    /**
     * Constructor.  Initializes the diff with the provided values.
     * @param operation One of INSERT, DELETE or EQUAL.
     * @param text The text being applied.
     */
    Diff(Operation operation, String text) {
      // Construct a diff with the specified operation and text.
      this.operation = operation;
      this.text = text;
    }

    /**
     * Display a human-readable version of this Diff.
     * @return text version.
     */
    public String toString() {
      String prettyText = this.text.replace('\n', '\u00b6');
      return "Diff(" + this.operation + ",\"" + prettyText + "\")";
    }

    /**
     * Create a numeric hash value for a Diff.
     * This function is not used by DMP.
     * @return Hash value.
     */
    @Override
    public int hashCode() {
      final int prime = 31;
      int result = (operation == null) ? 0 : operation.hashCode();
      result += prime * ((text == null) ? 0 : text.hashCode());
      return result;
    }

    /**
     * Is this Diff equivalent to another Diff?
     * @param obj Another Diff to compare against.
     * @return true or false.
     */
    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      Diff other = (Diff) obj;
      if (operation != other.operation) {
        return false;
      }
      if (text == null) {
        return other.text == null;
      } else return text.equals(other.text);
    }
  }

  /**
   * scoreDetail에 리스트로 담기위한 객체 클래스
   */
  public class WordScore {
    private String word;
    private String score;

    WordScore(String word, String score) {
      this.word = word;
      this.score = score;
    }

    public String getWord() {
      return word;
    }

    public String getScore() {
      return score;
    }

    public String toString() {
      return "\"" + this.word+ "\":" + score;
    }
  }

  /**
   * Character index를 문자열값으로 단어리스트 반환
   */
  private static class WordsToCharsResult {
    private String chars1;
    private String chars2;
    private List<String> wordArray;

    private WordsToCharsResult(String chars1, String chars2, List<String> wordArray) {
      this.chars1 = chars1;
      this.chars2 = chars2;
      this.wordArray = wordArray;
    }
  }

  /**
   * Find the differences between two texts.
   * @param text1 Old string to be diffed.
   * @param text2 New string to be diffed.
   * @return Linked List of Diff objects.
   */
  private LinkedList<Diff> diffMain(String text1, String text2) {
    // Set a deadline by which time the diff must be complete.
    long deadline;
    if (Diff_Timeout <= 0) {
      deadline = Long.MAX_VALUE;
    } else {
      deadline = System.currentTimeMillis() + (long) (Diff_Timeout * 1000);
    }
    return diffMain(text1, text2, deadline);
  }

  /**
   * Find the differences between two texts.  Simplifies the problem by
   * stripping any common prefix or suffix off the texts before diffing.
   * @param text1 Old string to be diffed.
   * @param text2 New string to be diffed.
   * @param deadline Time when the diff should be complete by.  Used
   *     internally for recursive calls.  Users should set DiffTimeout instead.
   * @return Linked List of Diff objects.
   */
  private LinkedList<Diff> diffMain(String text1, String text2, long deadline) {
    // Check for null inputs.
    if (text1 == null || text2 == null) {
      throw new IllegalArgumentException("Null inputs. (diff_main)");
    }

    // Check for equality (speedup).
    LinkedList<Diff> diffs;
    if (text1.equals(text2)) {
      diffs = new LinkedList<>();
      if (text1.length() != 0) {
        diffs.add(new Diff(Operation.EQUAL, text1));
      }
      return diffs;
    }

    // Trim off common prefix (speedup).
    int commonLength = diffCommonPrefix(text1, text2);
    String commonPrefix = text1.substring(0, commonLength);
    text1 = text1.substring(commonLength);
    text2 = text2.substring(commonLength);

    // Trim off common suffix (speedup).
    commonLength = diffCommonSuffix(text1, text2);
    String commonSuffix = text1.substring(text1.length() - commonLength);
    text1 = text1.substring(0, text1.length() - commonLength);
    text2 = text2.substring(0, text2.length() - commonLength);

    // Compute the diff on the middle block.
    diffs = diffCompute(text1, text2, deadline);

    // Restore the prefix and suffix.
    if (commonPrefix.length() != 0) {
      diffs.addFirst(new Diff(Operation.EQUAL, commonPrefix));
    }
    if (commonSuffix.length() != 0) {
      diffs.addLast(new Diff(Operation.EQUAL, commonSuffix));
    }

    diffCleanupMerge(diffs);
    return diffs;
  }

  /**
   * Find the differences between two texts.  Assumes that the texts do not
   * have any common prefix or suffix.
   * @param text1 Old string to be diffed.
   * @param text2 New string to be diffed.
   * @param deadline Time when the diff should be complete by.
   * @return Linked List of Diff objects.
   */
  private LinkedList<Diff> diffCompute(String text1, String text2, long deadline) {
    LinkedList<Diff> diffs = new LinkedList<>();

    if (text1.length() == 0) {
      // Just add some text (speedup).
      diffs.add(new Diff(Operation.INSERT, text2));
      return diffs;
    }

    if (text2.length() == 0) {
      // Just delete some text (speedup).
      diffs.add(new Diff(Operation.DELETE, text1));
      return diffs;
    }

    String longText = text1.length() > text2.length() ? text1 : text2;
    String shortText = text1.length() > text2.length() ? text2 : text1;
    int i = longText.indexOf(shortText);
    if (i != -1) {
      // Shorter text is inside the longer text (speedup).
      Operation op = (text1.length() > text2.length()) ?
                     Operation.DELETE : Operation.INSERT;
      diffs.add(new Diff(op, longText.substring(0, i)));
      diffs.add(new Diff(Operation.EQUAL, shortText));
      diffs.add(new Diff(op, longText.substring(i + shortText.length())));
      return diffs;
    }

    if (shortText.length() == 1) {
      // Single character string.
      // After the previous speedup, the character can't be an equality.
      diffs.add(new Diff(Operation.DELETE, text1));
      diffs.add(new Diff(Operation.INSERT, text2));
      return diffs;
    }

    return diffBisect(text1, text2, deadline);
  }

  /**
   * Find the 'middle snake' of a diff, split the problem in two
   * and return the recursively constructed diff.
   * See Myers 1986 paper: An O(ND) Difference Algorithm and Its Variations.
   * @param text1 Old string to be diffed.
   * @param text2 New string to be diffed.
   * @param deadline Time at which to bail if not yet complete.
   * @return LinkedList of Diff objects.
   */
  private LinkedList<Diff> diffBisect(String text1, String text2, long deadline) {
    // Cache the text lengths to prevent multiple calls.
    int text1_length = text1.length();
    int text2_length = text2.length();
    int max_d = (text1_length + text2_length + 1) / 2;
    int v_length = 2 * max_d;
    int[] v1 = new int[v_length];
    int[] v2 = new int[v_length];
    for (int x = 0; x < v_length; x++) {
      v1[x] = -1;
      v2[x] = -1;
    }
    v1[max_d + 1] = 0;
    v2[max_d + 1] = 0;
    int delta = text1_length - text2_length;
    // If the total number of characters is odd, then the front path will
    // collide with the reverse path.
    boolean front = (delta % 2 != 0);
    // Offsets for start and end of k loop.
    // Prevents mapping of space beyond the grid.
    int k1start = 0;
    int k1end = 0;
    int k2start = 0;
    int k2end = 0;
    for (int d = 0; d < max_d; d++) {
      // Bail out if deadline is reached.
      if (System.currentTimeMillis() > deadline) {
        break;
      }

      // Walk the front path one step.
      for (int k1 = -d + k1start; k1 <= d - k1end; k1 += 2) {
        int k1_offset = max_d + k1;
        int x1;
        if (k1 == -d || (k1 != d && v1[k1_offset - 1] < v1[k1_offset + 1])) {
          x1 = v1[k1_offset + 1];
        } else {
          x1 = v1[k1_offset - 1] + 1;
        }
        int y1 = x1 - k1;
        while (x1 < text1_length && y1 < text2_length
               && text1.charAt(x1) == text2.charAt(y1)) {
          x1++;
          y1++;
        }
        v1[k1_offset] = x1;
        if (x1 > text1_length) {
          // Ran off the right of the graph.
          k1end += 2;
        } else if (y1 > text2_length) {
          // Ran off the bottom of the graph.
          k1start += 2;
        } else if (front) {
          int k2_offset = max_d + delta - k1;
          if (k2_offset >= 0 && k2_offset < v_length && v2[k2_offset] != -1) {
            // Mirror x2 onto top-left coordinate system.
            int x2 = text1_length - v2[k2_offset];
            if (x1 >= x2) {
              // Overlap detected.
              return diffBisectSplit(text1, text2, x1, y1, deadline);
            }
          }
        }
      }

      // Walk the reverse path one step.
      for (int k2 = -d + k2start; k2 <= d - k2end; k2 += 2) {
        int k2_offset = max_d + k2;
        int x2;
        if (k2 == -d || (k2 != d && v2[k2_offset - 1] < v2[k2_offset + 1])) {
          x2 = v2[k2_offset + 1];
        } else {
          x2 = v2[k2_offset - 1] + 1;
        }
        int y2 = x2 - k2;
        while (x2 < text1_length && y2 < text2_length
               && text1.charAt(text1_length - x2 - 1)
               == text2.charAt(text2_length - y2 - 1)) {
          x2++;
          y2++;
        }
        v2[k2_offset] = x2;
        if (x2 > text1_length) {
          // Ran off the left of the graph.
          k2end += 2;
        } else if (y2 > text2_length) {
          // Ran off the top of the graph.
          k2start += 2;
        } else if (!front) {
          int k1_offset = max_d + delta - k2;
          if (k1_offset >= 0 && k1_offset < v_length && v1[k1_offset] != -1) {
            int x1 = v1[k1_offset];
            int y1 = max_d + x1 - k1_offset;
            // Mirror x2 onto top-left coordinate system.
            x2 = text1_length - x2;
            if (x1 >= x2) {
              // Overlap detected.
              return diffBisectSplit(text1, text2, x1, y1, deadline);
            }
          }
        }
      }
    }
    // Diff took too long and hit the deadline or
    // number of diffs equals number of characters, no commonality at all.
    LinkedList<Diff> diffs = new LinkedList<>();
    diffs.add(new Diff(Operation.DELETE, text1));
    diffs.add(new Diff(Operation.INSERT, text2));
    return diffs;
  }

  /**
   * Given the location of the 'middle snake', split the diff in two parts
   * and recurse.
   * @param text1 Old string to be diffed.
   * @param text2 New string to be diffed.
   * @param x Index of split point in text1.
   * @param y Index of split point in text2.
   * @param deadline Time at which to bail if not yet complete.
   * @return LinkedList of Diff objects.
   */
  private LinkedList<Diff> diffBisectSplit(String text1, String text2, int x, int y, long deadline) {
    String text1a = text1.substring(0, x);
    String text2a = text2.substring(0, y);
    String text1b = text1.substring(x);
    String text2b = text2.substring(y);

    // Compute both diffs serially.
    LinkedList<Diff> diffsA = diffMain(text1a, text2a, deadline);
    LinkedList<Diff> diffsB = diffMain(text1b, text2b, deadline);

    diffsA.addAll(diffsB);
    return diffsA;
  }

  /**
   * Determine the common prefix of two strings
   * @param text1 First string.
   * @param text2 Second string.
   * @return The number of characters common to the start of each string.
   */
  private int diffCommonPrefix(String text1, String text2) {
    // Performance analysis: https://neil.fraser.name/news/2007/10/09/
    int n = Math.min(text1.length(), text2.length());
    for (int i = 0; i < n; i++) {
      if (text1.charAt(i) != text2.charAt(i)) {
        return i;
      }
    }
    return n;
  }

  /**
   * Determine the common suffix of two strings
   * @param text1 First string.
   * @param text2 Second string.
   * @return The number of characters common to the end of each string.
   */
  private int diffCommonSuffix(String text1, String text2) {
    // Performance analysis: https://neil.fraser.name/news/2007/10/09/
    int text1_length = text1.length();
    int text2_length = text2.length();
    int n = Math.min(text1_length, text2_length);
    for (int i = 1; i <= n; i++) {
      if (text1.charAt(text1_length - i) != text2.charAt(text2_length - i)) {
        return i - 1;
      }
    }
    return n;
  }

  /**
   * Reorder and merge like edit sections.  Merge equalities.
   * Any edit section can move as long as it doesn't cross an equality.
   * @param diffs LinkedList of Diff objects.
   */
  private void diffCleanupMerge(LinkedList<Diff> diffs) {
    diffs.add(new Diff(Operation.EQUAL, ""));  // Add a dummy entry at the end.
    ListIterator<Diff> pointer = diffs.listIterator();
    int count_delete = 0;
    int count_insert = 0;
    String text_delete = "";
    String text_insert = "";
    Diff thisDiff = pointer.next();
    Diff prevEqual = null;
    int commonLength;
    while (thisDiff != null) {
      switch (thisDiff.operation) {
      case INSERT:
        count_insert++;
        text_insert += thisDiff.text;
        prevEqual = null;
        break;
      case DELETE:
        count_delete++;
        text_delete += thisDiff.text;
        prevEqual = null;
        break;
      case EQUAL:
        if (count_delete + count_insert > 1) {
          boolean both_types = count_delete != 0 && count_insert != 0;
          // Delete the offending records.
          pointer.previous();  // Reverse direction.
          while (count_delete-- > 0) {
            pointer.previous();
            pointer.remove();
          }
          while (count_insert-- > 0) {
            pointer.previous();
            pointer.remove();
          }
          if (both_types) {
            // Factor out any common prefixies.
            commonLength = diffCommonPrefix(text_insert, text_delete);
            if (commonLength != 0) {
              if (pointer.hasPrevious()) {
                thisDiff = pointer.previous();
                assert thisDiff.operation == Operation.EQUAL
                       : "Previous diff should have been an equality.";
                thisDiff.text += text_insert.substring(0, commonLength);
                pointer.next();
              } else {
                pointer.add(new Diff(Operation.EQUAL,
                    text_insert.substring(0, commonLength)));
              }
              text_insert = text_insert.substring(commonLength);
              text_delete = text_delete.substring(commonLength);
            }
            // Factor out any common suffixies.
            commonLength = diffCommonSuffix(text_insert, text_delete);
            if (commonLength != 0) {
              thisDiff = pointer.next();
              thisDiff.text = text_insert.substring(text_insert.length()
                  - commonLength) + thisDiff.text;
              text_insert = text_insert.substring(0, text_insert.length()
                  - commonLength);
              text_delete = text_delete.substring(0, text_delete.length()
                  - commonLength);
              pointer.previous();
            }
          }
          // Insert the merged records.
          if (text_delete.length() != 0) {
            pointer.add(new Diff(Operation.DELETE, text_delete));
          }
          if (text_insert.length() != 0) {
            pointer.add(new Diff(Operation.INSERT, text_insert));
          }
          // Step forward to the equality.
          thisDiff = pointer.hasNext() ? pointer.next() : null;
        } else if (prevEqual != null) {
          // Merge this equality with the previous one.
          prevEqual.text += thisDiff.text;
          pointer.remove();
          thisDiff = pointer.previous();
          pointer.next();  // Forward direction
        }
        count_insert = 0;
        count_delete = 0;
        text_delete = "";
        text_insert = "";
        prevEqual = thisDiff;
        break;
      }
      thisDiff = pointer.hasNext() ? pointer.next() : null;
    }
    if (diffs.getLast().text.length() == 0) {
      diffs.removeLast();  // Remove the dummy entry at the end.
    }

    /*
     * Second pass: look for single edits surrounded on both sides by equalities
     * which can be shifted sideways to eliminate an equality.
     * e.g: A<ins>BA</ins>C -> <ins>AB</ins>AC
     */
    boolean changes = false;
    // Create a new iterator at the start.
    // (As opposed to walking the current one back.)
    pointer = diffs.listIterator();
    Diff prevDiff = pointer.hasNext() ? pointer.next() : null;
    thisDiff = pointer.hasNext() ? pointer.next() : null;
    Diff nextDiff = pointer.hasNext() ? pointer.next() : null;
    // Intentionally ignore the first and last element (don't need checking).
    while (nextDiff != null) {
      assert prevDiff != null;
      assert thisDiff != null;
      if (prevDiff.operation == Operation.EQUAL &&
          nextDiff.operation == Operation.EQUAL) {
        // This is a single edit surrounded by equalities.
        if (thisDiff.text.endsWith(prevDiff.text)) {
          // Shift the edit over the previous equality.
          thisDiff.text = prevDiff.text
              + thisDiff.text.substring(0, thisDiff.text.length()
                                           - prevDiff.text.length());
          nextDiff.text = prevDiff.text + nextDiff.text;
          pointer.previous(); // Walk past nextDiff.
          pointer.previous(); // Walk past thisDiff.
          pointer.previous(); // Walk past prevDiff.
          pointer.remove(); // Delete prevDiff.
          pointer.next(); // Walk past thisDiff.
          thisDiff = pointer.next(); // Walk past nextDiff.
          nextDiff = pointer.hasNext() ? pointer.next() : null;
          changes = true;
        } else if (thisDiff.text.startsWith(nextDiff.text)) {
          // Shift the edit over the next equality.
          prevDiff.text += nextDiff.text;
          thisDiff.text = thisDiff.text.substring(nextDiff.text.length())
              + nextDiff.text;
          pointer.remove(); // Delete nextDiff.
          nextDiff = pointer.hasNext() ? pointer.next() : null;
          changes = true;
        }
      }
      prevDiff = thisDiff;
      thisDiff = nextDiff;
      nextDiff = pointer.hasNext() ? pointer.next() : null;
    }
    // If shifts were made, the diff needs reordering and another shift sweep.
    if (changes) {
      diffCleanupMerge(diffs);
    }
  }

  /**
   * 단어 모드 비교 함수
   */
  public LinkedList<Diff> diffWordMode(String answerStr, String inputStr) {

    // 문자열 처리
    answerStr = processingComparableStr(answerStr);
    inputStr = processingComparableStr(inputStr);

    //Diff_Timeout = 0f; // 디버그시에는 0으로 수정
    WordsToCharsResult a = diffWordsToChars(answerStr, inputStr);
    String text1 = a.chars1;
    String text2 = a.chars2;
    List<String> wordArray = a.wordArray;

    LinkedList<Diff> diffs = diffMain(text1, text2);

    diffCharsToWords(diffs, wordArray);

    return diffs;
  }

  /**
   * 문자열들을 char index 값과 단어 목록 배열로 반환 함수
   */
  private WordsToCharsResult diffWordsToChars(String text1, String text2) {
    List<String> wordArray = new ArrayList<>();
    Map<String, Integer> wordHash = new HashMap<>();

    wordArray.add("");

    String chars1 = diffWordsToCharsMunge(text1, wordArray, wordHash, 40000);
    String chars2 = diffWordsToCharsMunge(text2, wordArray, wordHash, 65535);
    return new WordsToCharsResult(chars1, chars2, wordArray);
  }

  /**
   * 비교 문자열을 char index로 변환 후 문자열 값으로 반환
   */
  private String diffWordsToCharsMunge(String text, List<String> wordArray, Map<String, Integer> wordHash, int maxWords) {
    int wordStart = 0;
    int wordEnd = -1;
    String word;
    StringBuilder chars = new StringBuilder();

    while (wordEnd < text.length() - 1) {
      wordEnd = text.indexOf(' ', wordStart);
      if (wordEnd == -1) {
        wordEnd = text.length();
      }
      word = text.substring(wordStart, wordEnd);

      if (wordHash.containsKey(word)) {
        chars.append(String.valueOf((char) (int) wordHash.get(word)));
      } else {
        if (wordArray.size() == maxWords) {
          word = text.substring(wordStart);
          wordEnd = text.length();
        }
        wordArray.add(word);
        wordHash.put(word, wordArray.size() - 1);
        chars.append(String.valueOf((char) (wordArray.size() - 1)));
      }
      wordStart = wordEnd + 1;
    }
    return chars.toString();
  }

  /**
   * char로 담긴 값을 다시 문자열로 변환
   */
  private void diffCharsToWords(List<Diff> diffs, List<String> wordArray) {
    StringBuilder text;
    for (Diff diff : diffs) {
      text = new StringBuilder();
      for (int j = 0; j < diff.text.length(); j++) {
        if(!text.toString().isEmpty()) {
          text.append(" ");
        }
        text.append(wordArray.get(diff.text.charAt(j)));
      }
      diff.text = text.toString();
    }
  }

  /**
   * 문자열에서 연속공백, 특수문자처리 후 영문자는 소문자로 반환
   */
  private String processingComparableStr(String str) {

    // 문장에 문장에 &nbsp;(html 공백 특수문자)가 들어가 있는 경우

    str = str.replaceAll("\u00A0"," ").trim();

    // 연속공백을 공백으로 대체
    str = str.replaceAll("\\s{2,}", " ");

    // 특수문자 제거
    str = str.replaceAll("[,.!?;]*", "");

    // 모든 문자 소문자로 변경
    str = str.toLowerCase().trim();

    return str;
  }

  /**
   * 비교 점수 반환
   */
  public int getScore(LinkedList<Diff> diffs, String answerStr) {

    final float totalScore = 100f;
    float scorePerWord = totalScore / answerStr.split(" ").length;
    float score = 0;

    int deleteCnt = 0;
    int insertCnt = 0;

    // Dummy EQUAL값 넣기
    diffs.add(new Diff(Operation.EQUAL, ""));

    for(Diff diff : diffs) {
      int wordCnt = diff.text.split(" ").length;
      if(diff.text.isEmpty()) {
        wordCnt = 0;
      }
      switch (diff.operation) {
        case DELETE:
          deleteCnt = wordCnt;
          break;
        case INSERT:
          insertCnt = wordCnt;
          break;
        case EQUAL:
          score += wordCnt * scorePerWord;

          if (deleteCnt < insertCnt) {
            score -= (insertCnt - deleteCnt) * 5;
          }

          deleteCnt = 0;
          insertCnt = 0;
          break;
      }
    }

    // Dummy EQUAL값 제거
    diffs.removeLast();

    return Math.round(score);
  }

  /**
   * 비교 점수 상세 반환
   */
  public List<WordScore> getScoreDetail(LinkedList<Diff> diffs, String answerStr) {

    List<WordScore> detailList = new ArrayList<>();
    answerStr = processingComparableStr(answerStr);
    final float totalScore = 100f;
    int scorePerWord = (int) Math.floor(totalScore / answerStr.split(" ").length);
    int remainder = 100 % answerStr.split(" ").length;

    List<String> strList = new ArrayList<>(Arrays.asList(answerStr.split(" ")));
    List<Integer> scoreList = new ArrayList<>();
    for(String ignored : strList) {
      int score = scorePerWord;
      if(remainder > 0) {
        score += 1;
        remainder--;
      }
      scoreList.add(score);
    }

    List<String> insertStrList = new ArrayList<>();

    int deleteCnt = 0;
    int insertCnt = 0;
    String deleteWord = "";
    String insertWord = "";

    // Dummy EQUAL값 넣기
    diffs.add(new Diff(Operation.EQUAL, ""));

    for(Diff diff : diffs) {
      int wordCnt = diff.text.split(" ").length;
      if(diff.text.isEmpty()) {
        wordCnt = 0;
      }

      switch (diff.operation) {
        case DELETE:
          deleteCnt = wordCnt;
          deleteWord = diff.text;
          break;
        case INSERT:
          insertCnt = wordCnt;
          insertWord = diff.text;
          break;
        case EQUAL:
          if(deleteCnt > 0) {
            String[] strArr = deleteWord.split(" ");

            for(int k=0; k < strArr.length; k++) {
              int index = strList.indexOf(strArr[k]);
              if (index > -1) {

                int dupIndex = strList.indexOf(diff.text) - strArr.length + k;
                if (dupIndex >= 0){
                  index = dupIndex;
                }
                scoreList.set(index, 0);
              }
            }
          }

          if(deleteCnt < insertCnt) {
            String[] strArr = insertWord.split(" ");
            insertStrList.addAll(Arrays.asList(strArr).subList(deleteCnt, strArr.length));
          }

          deleteCnt = 0;
          insertCnt = 0;
          break;
      }
    }
    // Dummy EQUAL값 제거
    diffs.removeLast();

    List<Integer> insertScoreList = new ArrayList<>();
    for(String ignored : insertStrList) {
      insertScoreList.add(-5);
    }

    // 최종 리스트 합치기
    strList.addAll(insertStrList);
    scoreList.addAll(insertScoreList);

    // 반환 리스트에 새로 담기
    for(int i = 0; i < strList.size(); i++) {
      String word = strList.get(i);
      String score = scoreList.get(i).toString();
      detailList.add(new WordScore(word, score));
    }

    return detailList;
  }

  /* 테스트용 main 함수 */
  /*public static void main(String[] args) {

    String answerStr  = "I love you, you love me";
    String inputStr   = "I me";
//    String answerStr  = "태풍이 오고 있습니다. 여러분";
//    String inputStr   = "태풍이 태풍 태풍 하고 있습니다.";

    System.out.println("answerStr -> " + answerStr);
    System.out.println("inputStr -> " + inputStr);
    System.out.println();

    DiffMatchPatchUtil dmp = new DiffMatchPatchUtil();
    LinkedList<DiffMatchPatchUtil.Diff> diffs = dmp.diffWordMode(answerStr, inputStr);       // 비교 DIFFS

    int score = dmp.getScore(diffs, answerStr);                                               // 점수
    List<DiffMatchPatchUtil.WordScore> scoreDetail = dmp.getScoreDetail(diffs, answerStr);    // 점수 상세

    System.out.println("DIFFS -> " + diffs);
    System.out.println("score -> " + score);
    System.out.println("scoreDetail -> " + scoreDetail);

  }*/
}
