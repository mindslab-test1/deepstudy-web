package ai.mindslab.engedu.dic.dao.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class MathDicVO implements Serializable {
	private String mathId;
	private String word;
	private String means;
	private String printMeans;
	private String url;
	private int priority;
	
}
