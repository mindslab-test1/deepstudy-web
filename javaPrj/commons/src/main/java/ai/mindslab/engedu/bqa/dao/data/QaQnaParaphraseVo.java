package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@SuppressWarnings("serial")
@Data
public class QaQnaParaphraseVo implements Serializable {

	private int mainWordId;
	private String mainWord;
	private String paraphraseWord;
	private int[] wordId;
	private Date createAt;
}
