package ai.mindslab.engedu.dic.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.dic.dao.data.KorDicVO;

@Mapper
public interface KorDicMapper {
	int getCount();

	List<KorDicVO> getSearch(Map<String, String> hashMap);
}
