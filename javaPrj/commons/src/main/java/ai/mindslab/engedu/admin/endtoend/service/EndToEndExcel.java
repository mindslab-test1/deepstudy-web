package ai.mindslab.engedu.admin.endtoend.service;

import ai.mindslab.engedu.admin.endtoend.dao.data.EndToEndAdminVO;
import ai.mindslab.engedu.admin.servicement.dao.data.ServiceMentAdminVO;
import ai.mindslab.engedu.admin.servicement.service.GameMentService;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class EndToEndExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    EndToEndAdminService endToEndAdminService;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 2000, 4000, 4000, 4000, 4000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "단어", "연음1", "연음2","레벨"};

       List<EndToEndAdminVO> list = endToEndAdminService.getEndToEndExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;

        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(EndToEndVO vo : list){
            String[] data = {
                    vo.getEndId()+"",
                    vo.getWord(),
                    vo.getThumb1(),
                    vo.getThumb2(),
                    vo.getLevel()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}
