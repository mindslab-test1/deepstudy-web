package ai.mindslab.engedu.intent.service;


import ai.mindslab.engedu.intent.dao.IntentMsgMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class IntentMsgService {
	
	@Autowired
	private IntentMsgMapper intentMsgMapper;
	
	public String getServiceMsg(String serviceType, String code) {
		
		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("serviceType", serviceType);
		hashMap.put("code", code);
		
		return intentMsgMapper.getServiceMsg(hashMap);
	}
}
