package ai.mindslab.engedu.dic.dao.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class KorDicSynonymVO implements Serializable {
	private String korId;
	private String word;
	private String means;
	private String printMeans;
	private String url;
	private int priority;
	
}
