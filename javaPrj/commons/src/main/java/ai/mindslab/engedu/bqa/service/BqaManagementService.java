package ai.mindslab.engedu.bqa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.engedu.bqa.dao.QaAnswerMapper;
import ai.mindslab.engedu.bqa.dao.QaExcelMapper;
import ai.mindslab.engedu.bqa.dao.QaIndexMapper;
import ai.mindslab.engedu.bqa.dao.QaQnaParaphraseMapper;
import ai.mindslab.engedu.bqa.dao.QaQuestionMapper;
import ai.mindslab.engedu.bqa.dao.data.NlpAnalysisVo;
import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.dao.data.QaExcelVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.bqa.dao.data.QaQnaParaphraseVo;
import ai.mindslab.engedu.bqa.dao.data.QaQuestionVo;
import ai.mindslab.engedu.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.engedu.bqa.commons.codes.IBasicQAConst;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseListObject;

import ai.mindslab.engedu.bqa.commons.utils.PagingHelper;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.common.LangOuterClass.LangCode;

/**
 * @author park Chatbot Admin Data Management
 */
@Service
public class BqaManagementService implements IRestCodes,IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(BqaManagementService.class);
	public final String USE_YN_Y = "Y";
	public final String USE_YN_N = "N";

	@Autowired
	private QaQuestionMapper questionMapper;

	@Autowired
	private QaAnswerMapper answerMapper;

	@Autowired
	private QaExcelMapper excelMapper;

	@Autowired
	private QaIndexMapper indexMapper;

	@Autowired
	private QaQnaParaphraseMapper qnaParaphrase;

	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

	@Autowired
	private PagingHelper pgHelper;

	@Value("${maum.chunk.query.count :100}")
	private int queryCount;
	
	public BaseListObject<QaQuestionVo> getsQuestion(int pg, int pgSize, int... domainIds) throws EngEduException {
		logger.info("Service getsQuestion parameter pg{},domainIds {}", pg, domainIds);

		List<QaQuestionVo> list = null;
		int totalCount;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domainIds", domainIds);
			params.put("useYn", USE_YN_Y);

			if (domainIds != null && domainIds[0] != 0) {
				params.put("domain", 1);
			} else {
				params.put("domain", 0);
			}

			totalCount = questionMapper.getCountByDomain(params);
			if (totalCount > 0) {

				pgHelper.setPageInfo(params, pg, pgSize);
				list = questionMapper.gets(params);

				for (QaQuestionVo qaQuestionVo : list) {

					int count = answerMapper.getCountByQuestionId(qaQuestionVo.getQuestionId());
					if (count > 0) {

						List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(qaQuestionVo.getQuestionId());
						qaQuestionVo.setAnswerList(answerList);
					}
				}
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_SELECT_ERROR_Q, e);
			throw new EngEduException(ERR_CODE_MGMT_SELECT_ERROR_Q, ERR_MSG_MGMT_SELECT_ERROR_Q);
		}

		return new BaseListObject<>(totalCount, list);
	}

	public BaseListObject<NlpAnalysisVo> nlpTest(String text) throws EngEduException {
		logger.info("Service nlpTest parameter text {}", text);

		List<NlpAnalysisVo> list;
		int totalCount;
		try {

			list = nlpAnalyzeClient.nlpTestAnalyze(text);
			totalCount = list.size();
		} catch (Exception e) {
			logger.warn("nlpTest Error/{}", e);
			throw new EngEduException(ERR_CODE_NLP_ANALYSIS_ERROR, ERR_MSG_NLP_ANALYSIS_ERROR);
		}

		return new BaseListObject<>(totalCount, list);
	}
	
	@Transactional(rollbackFor = { Exception.class })
	public int createQna(QaQuestionVo q) throws EngEduException {
		logger.info("Service createQna parameter {}", q);

		int count = 0;
		try {
			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			q.setQuestionId(0);
			q.setUseYn(USE_YN_Y);
			q.setMainYn("Y");

			int resultCount = questionMapper.update(q);
			count += resultCount;
			if (resultCount == 0 && q.getQuestionId() == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			List<QaIndexVo> qaQnaIndexList = this.addParaphrase(q, paraphraseList);
			qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

			Map<String, Object> param = new HashMap<>();
			param.put("list", qaQnaIndexList);

			int resultIndex = indexMapper.insertList(param);
			logger.debug("createQna resultIndex {}", resultIndex);

			if (resultIndex == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
			}
			List<QaAnswerVo> answerList = q.getAnswerList();
			for (QaAnswerVo qaAnswerVo : answerList) {
				qaAnswerVo.setAnswerId(0);
				qaAnswerVo.setQuestionId(q.getQuestionId());
				qaAnswerVo.setUseYn(USE_YN_Y);
				qaAnswerVo.setCreatorId(q.getCreatorId()); // 수정
				qaAnswerVo.setUpdatorId(q.getUpdatorId());
			}

			Map<String, Object> paramA = new HashMap<>();
			paramA.put("list", answerList);

			int resultAnswerCount = answerMapper.insertList(paramA);
			logger.debug("createQna resultAnswerCount {}", resultAnswerCount);
			if (resultAnswerCount == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
			}

		} catch (Exception e) {

			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new EngEduException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return count;
	}

	@Transactional(rollbackFor = { Exception.class })
	public int updateQna(QaQuestionVo qaQuestionVo) throws EngEduException {
		logger.info("Service updateQna parameter {}", qaQuestionVo);
		int resultCount;
		int questionId = qaQuestionVo.getQuestionId();
		try {

			resultCount = questionMapper.update(qaQuestionVo);
			logger.debug("questionMapper update resultCount={}", resultCount);
			if (resultCount == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			Map<String, Object> param = new HashMap<>();
			param.put("useYn", USE_YN_N);
			param.put("questionId", questionId);
			param.put("updatorId", 1);

			int resultIndex = indexMapper.updateUseYn(param);
			logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);
			if (qaQuestionVo.getUseYn().equals(USE_YN_Y)) {

				List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
				List<QaIndexVo> qaQnaIndexList = this.addParaphrase(qaQuestionVo, paraphraseList);
				qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

				Map<String, Object> paramIndex = new HashMap<>();
				paramIndex.put("list", qaQnaIndexList);

				resultIndex = indexMapper.insertList(paramIndex);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

				if (resultIndex == 0) {
					throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
				}

				List<QaAnswerVo> answerList = qaQuestionVo.getAnswerList();
				int resultAnswerCount = answerMapper.updateUseYn(param);
				for (QaAnswerVo answer : answerList) {
					if (answer.getAnswerId() == 0) {
						answer.setQuestionId(questionId);
					}
					answerMapper.update(answer);
				}

				logger.debug("answerMapper update resultAnswerCount={}", resultAnswerCount);
			} else {

				int resultAnswerCount = answerMapper.updateUseYn(param);
				logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);
			}

		} catch (EngEduException e) {
			throw e;
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new EngEduException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return resultCount;
	}

	@Transactional(rollbackFor = { Exception.class })
	public int insertQnaList(List<QaQuestionVo> questionList) throws EngEduException {
		logger.info("Service insertQnaList questionList {}", questionList);

		int resultCount;
		try {

			Map<String, Object> param = new HashMap<>();
			param.put("list", questionList);

			resultCount = questionMapper.insertList(param);

			logger.debug("questionMapper update resultCount={}", resultCount);
			if (resultCount == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			List<QaIndexVo> indexList = this.addParaphraseList(questionList, paraphraseList);
			indexList = nlpAnalyzeClient.analyzeList(indexList);

			Map<String, Object> paramIndex = new HashMap<>();
			param.put("list", indexList);

			int resultIndex = indexMapper.insertList(paramIndex);
			logger.debug("insertQnaList resultIndex {}", resultIndex);
			if (resultIndex == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
			}

			List<QaAnswerVo> answerList = new ArrayList<>();
			for (QaQuestionVo qaQuestionVo : questionList) {
				answerList.addAll(qaQuestionVo.getAnswerList());
			}
			Map<String, Object> params = new HashMap<>();
			params.put("list", answerList);

			int resultAnswerCount = answerMapper.insertList(params);
			logger.debug("insertQnaList resultAnswerCount {}", resultAnswerCount);
			if (resultAnswerCount == 0) {
				throw new EngEduException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
			}
		} catch (EngEduException e) {
			throw e;
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new EngEduException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return resultCount;
	}

	@Transactional(rollbackFor = { Exception.class })
	public int deleteQna(QaQuestionVo qaQuestionVo) throws EngEduException {
		logger.info("Service deleteQna parameter {}", qaQuestionVo);
		int resultCount;
		int questionId = qaQuestionVo.getQuestionId();
		try {

			Map<String, Object> param = new HashMap<>();
			param.put("useYn", USE_YN_N);
			param.put("questionId", questionId);
			param.put("updatorId", "system");

			int resultIndex = indexMapper.updateUseYn(param);
			logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

			int resultAnswerCount = answerMapper.updateUseYn(param);
			logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);

			resultCount = questionMapper.updateUseYn(param);
			logger.debug("questionMapper updateUseYn resultCount={}", resultCount);

		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new EngEduException(ERR_CODE_MGMT_DELETE_ERROR_Q, ERR_MSG_MGMT_DELETE_ERROR_Q);
		}
		return resultCount;
	}
	
	@Transactional(rollbackFor = { Exception.class })
	public int deleteQnaByDomainId(int[] domainIds) throws EngEduException {
		logger.info("Service deleteQna domainIds {}", domainIds);
				
		int resultCount = 0;
		
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domainIds", domainIds);
			params.put("useYn", USE_YN_Y);
			if (domainIds != null && domainIds[0] != 0) {
				params.put("domain", 1);
			} else {
				params.put("domain", 0);
			}
			List<QaQuestionVo> list = questionMapper.gets(params);
			ArrayList<Integer> arrayList = new ArrayList<Integer>();
			for(int i = 0 ; i < list.size() ; i++) {
				
				arrayList.add(i,  list.get(i).getQuestionId());
			}
			
			if(arrayList.size() > 0) {
				Map<String, Object> param = new HashMap<>();
				param.put("useYn", USE_YN_N);
				param.put("questionIds", arrayList);
				param.put("updatorId", "system");
	
				int resultIndex = indexMapper.updateUseYn(param);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);
	
				int resultAnswerCount = answerMapper.updateUseYn(param);
				logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);
	
				resultCount = questionMapper.updateUseYn(param);
				logger.debug("questionMapper updateUseYn resultCount={}", resultCount);
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new EngEduException(ERR_CODE_MGMT_DELETE_ERROR_Q, ERR_MSG_MGMT_DELETE_ERROR_Q);
		}
		return resultCount;
	}

	public List<QaIndexVo> addParaphrase(QaQuestionVo qaQuestionVo, List<QaQnaParaphraseVo> paraphraseList) {

		List<QaIndexVo> qaIndexList = new ArrayList<>();

		int seq = 1;
		qaIndexList.add(new QaIndexVo(qaQuestionVo, seq, qaQuestionVo.getMainYn()));
		seq++;

		// ALL Paraphrase while
		for (QaQnaParaphraseVo qaQnaParaphraseVo : paraphraseList) {

			String question = qaQuestionVo.getQuestion();
			String mainWord = qaQnaParaphraseVo.getMainWord();
			StringBuilder questionBuilder = new StringBuilder(question);
			int lastIndex = StringUtils.lastIndexOf(question, mainWord);

			if (lastIndex > -1 && StringUtils.endsWith(question, mainWord)) {

				String[] paraphraseArray = StringUtils.split(qaQnaParaphraseVo.getParaphraseWord(), ",");

				// Same Paraphrase while
				for (String paraphrase : paraphraseArray) {

					StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), paraphrase);
					qaQuestionVo.setQuestion(paraphraseBuilder.toString());
					qaQuestionVo.setQuestionType(qaQuestionVo.getQuestionType());

					qaIndexList.add(new QaIndexVo(qaQuestionVo, seq, "N"));
					seq++;
				}
			}
		}
		return qaIndexList;
	}

	private List<QaIndexVo> addParaphraseList(List<QaQuestionVo> qaQuestionList, List<QaQnaParaphraseVo> paraphraseList) {

		List<QaIndexVo> qaIndexList = new ArrayList<>();

		// ALL Q&A while
		for (QaQuestionVo qaQuestionVo : qaQuestionList) {

			qaIndexList.addAll(addParaphrase(qaQuestionVo, paraphraseList));
		}
		return qaIndexList;
	}

	public BaseListObject<QaExcelVo> getsExcel(int domainId) throws EngEduException {
		logger.debug("Service getsExcel parameter domainId {}", domainId);

		List<QaExcelVo> qList;
		int totalCount = 0;
		try {
			HashMap<String, Object> paramQ = new HashMap<>();
			paramQ.put("domainId", domainId);
			paramQ.put("useYn", USE_YN_Y);

			qList = excelMapper.getsExcel(paramQ);
			if(qList != null) {
				totalCount = qList.size();
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_EXCEL_CREATE_ERROR, e);
			throw new EngEduException(ERR_CODE_EXCEL_CREATE_ERROR, ERR_MSG_EXCEL_CREATE_ERROR);
		}
		return new BaseListObject<>(totalCount, qList);
	}
	
	@Transactional(rollbackFor = { Exception.class })
	public int insertExcel(List<QaExcelVo> list) throws EngEduException {

		logger.debug("Servcice uploadFile list", list);
		int resultCount = 0;
		int resultAnswerCount = 0;
		try {
			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			List<QaIndexVo> qaQnaIndexList = new ArrayList<>();
			List<QaAnswerVo> answerList = new ArrayList<>();
			logger.debug("======================================================================================");
			int questionId = 0;
//			int beforeQuestionId = 0;
			boolean aFlag = true;
			for (int i = 0; i < list.size(); i++) {
				
				QaExcelVo e = list.get(i);
				QaQuestionVo q = new QaQuestionVo();

				if (i == 0) {
					q.setQuestionId(0);
					q.setDomainId(e.getDomainId());
					q.setQuestion(e.getQuestion());
					q.setQuestionType(e.getQuestionType());
					q.setUseYn(USE_YN_Y);
					q.setMainYn("Y");
					q.setCreatorId(e.getUserId()); // 수정
					q.setUpdatorId(e.getUserId());
					resultCount += questionMapper.update(q);
					questionId = q.getQuestionId();
					qaQnaIndexList.addAll(this.addParaphrase(q, paraphraseList));
				}

				// Excel 같은 질문일때
				if (i > 0 && e.getQuestionId() == list.get(i - 1).getQuestionId()) {
					aFlag = false;

					QaAnswerVo qaAnswerVo = new QaAnswerVo();
					QaExcelVo ea = list.get(i);
					logger.debug("{} 번째 응답", i, ea);
					qaAnswerVo.setAnswerId(0);
					qaAnswerVo.setAnswer(ea.getAnswer());
					qaAnswerVo.setQuestionId(questionId);
					qaAnswerVo.setUseYn(USE_YN_Y);
					qaAnswerVo.setCreatorId(ea.getUserId()); // 수정
					qaAnswerVo.setUpdatorId(ea.getUserId());
					answerList.add(qaAnswerVo);

				}else if (i > 0){
					aFlag = true;

					q.setQuestionId(0);
					q.setDomainId(e.getDomainId());
					q.setQuestion(e.getQuestion());
					q.setQuestionType(e.getQuestionType());
					q.setUseYn(USE_YN_Y);
					q.setMainYn("Y");
					q.setCreatorId(e.getUserId()); // 수정
					q.setUpdatorId(e.getUserId());
					resultCount += questionMapper.update(q);
					questionId = q.getQuestionId();
					qaQnaIndexList.addAll(this.addParaphrase(q, paraphraseList));
				}


				if (aFlag) { // Answer List 입력

					QaAnswerVo qaAnswerVo = new QaAnswerVo();
					QaExcelVo ea = list.get(i);
					logger.debug("{} 번째 응답", i, ea);
					qaAnswerVo.setAnswerId(0);
					qaAnswerVo.setAnswer(ea.getAnswer());
					qaAnswerVo.setQuestionId(questionId);
					qaAnswerVo.setUseYn(USE_YN_Y);
					qaAnswerVo.setCreatorId(ea.getUserId()); // 수정
					qaAnswerVo.setUpdatorId(ea.getUserId());
					answerList.add(qaAnswerVo);
				}
			}
			logger.debug("createQna answerList size ={}", answerList.size());

			if(answerList.size() > 100) {
				int pageCount =  answerList.size() / queryCount ;
				for(int i = 0 ; i < pageCount ; i++) {
					Map<String, Object> paramA = new HashMap<>();
					paramA.put("list", answerList.subList(i*queryCount, ((i+1)*(queryCount))));
					resultAnswerCount += answerMapper.insertList(paramA);
				}
				Map<String, Object> paramA = new HashMap<>();
				paramA.put("list", answerList.subList((queryCount*pageCount)+1, answerList.size()));
				resultAnswerCount += answerMapper.insertList(paramA);
			}else {
				Map<String, Object> paramA = new HashMap<>();
				paramA.put("list", answerList);
				resultAnswerCount += answerMapper.insertList(paramA);
			}
			logger.debug("createQna answerList resultAnswerCount ={}", resultAnswerCount);

			int result = 0;
			if(qaQnaIndexList.size() > queryCount) {
				int pageCount =  qaQnaIndexList.size() / queryCount ;
				for(int i = 0 ; i < pageCount ; i++) {
					result += insertIndexList(qaQnaIndexList.subList(i*queryCount, ((i+1)*(queryCount))));
				}
				result = insertIndexList(qaQnaIndexList.subList((queryCount*pageCount)+1, qaQnaIndexList.size()));
			}else {
				result = insertIndexList(qaQnaIndexList);
			}
			logger.info("insertIndexList result count: {}", result);

/*			StreamObserver<Document> responseObserver = new StreamObserver<Document>() {

				int index = 0;

				@Override
				public void onNext(Document document) {

					Map<String,Object> map = nlpAnalyzeClient.modify(document.getSentencesList());
					
					String questionMorph = map.get("questionMorph").toString();
					int morphCount = (Integer)map.get("morphCount");
					qaQnaIndexList.get(index).setMorphCount(morphCount);
					qaQnaIndexList.get(index).setQuestionMorph(questionMorph); // 분석 결과 삽입
					logger.info("question {}, questionMorph , {}", qaQnaIndexList.get(index).getQuestion(),
							questionMorph);
					index++;
				}

				@Override
				public void onError(Throwable t) {

					logger.error("analyzeMultiple Failed: {}", Status.fromThrowable(t));
					nlpAnalyzeClient.countDownLatch();
				}

				@Override
				public void onCompleted() {

					nlpAnalyzeClient.countDownLatch();
					int result = 0;
					if(qaQnaIndexList.size() > queryCount) {
						int pageCount =  qaQnaIndexList.size() / queryCount ;
						for(int i = 0 ; i < pageCount ; i++) {
							result += insertIndexList(qaQnaIndexList.subList(i*queryCount, ((i+1)*(queryCount))));
						}
						result = insertIndexList(qaQnaIndexList.subList((queryCount*pageCount)+1, qaQnaIndexList.size()));
					}else {
						result = insertIndexList(qaQnaIndexList);
					}
					logger.info("insertIndexList result count: {}", result);
				}
			};

			requestAnalyze(responseObserver, qaQnaIndexList);*/
			
			logger.debug("======================================================================================");
			logger.debug("resultCount = {}, resultAnswerCount = {}", resultCount, resultAnswerCount);
		} catch (Exception e) {
			logger.warn(e.toString(), e);
			throw new EngEduException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}
		return resultCount;
	}

	// index db insert 요청
	private int insertIndexList(List<QaIndexVo> qaIndexList) {

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("list", qaIndexList);

		return indexMapper.insertList(paramMap);
	}

	private void requestAnalyze(StreamObserver<Document> responseObserver, List<QaIndexVo> qaIndexList) {

		StreamObserver<InputText> requestObserver = nlpAnalyzeClient.analyzeMultiple(responseObserver);

		for (QaIndexVo qaIndexVo : qaIndexList) {

			String question = qaIndexVo.getQuestion();
			InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor).setSplitSentence(true)
					.setUseTokenizer(true).setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
					.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

			requestObserver.onNext(inputText);
		}
		requestObserver.onCompleted();
	}

}
