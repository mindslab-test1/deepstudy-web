package ai.mindslab.engedu.common.codes;

public interface IRestCodes {

	public static final String RESULT_TYPE_SIMPLE_TTS 	= "ST";
	public static final String RESULT_TYPE_SIMPLE_STT 	= "SS";
	public static final String RESULT_TYPE_EVAL_STT 	= "ES";
	public static final String RESULT_TYPE_DIALOG_STT 	= "DS";
	public static final String RESULT_TYPE_TUTORIAL_STT = "TS";
	public static final String RESULT_TYPE_UTTER_EVAL_STT	= "US";
	public static final String RESULT_TYPE_CLIENT_API 	= "CA";
	public static final String RESULT_TYPE_PHONICS_STT 	= "PS";
	public static final String RESULT_TYPE_LETTER_STT 	= "LS";

	public static final int ERR_CODE_EPD     = 183;
	public static final String ERR_MSG_EPD   = "EPD";
	
	public static final int ERR_CODE_SOCKET_TIMEOUT     = 144;
	public static final String ERR_MSG_SOCKET_TIMEOUT   = "SOCKET TIMEOUT";

	public static final int ERR_CODE_SUCCESS = 200;
	public static final String ERR_MSG_SUCCESS = "SUCCESS";

	public static final int ERR_CODE_NOT_FOUND = 404;
	public static final String ERR_MSG_NOT_FOUND = "NOT FOUND";

	public static final int ERR_CODE_FAILURE = 500;
	public static final String ERR_MSG_FAILURE = "FAILURE";

	public static final int ERR_CODE_SCRIPT_EXPRESSION_ERROR = 501;
	public static final String ERR_MSG_SCRIPT_EXPRESSION_ERROR = "SCRIPT_EXPRESSION_ERROR";
	
	public static final int ERR_CODE_PARAMS_INVALID = 502;
    public static final String ERR_MSG_PARAMS_INVALID = "INVALID PARAMETERS";

	public static final int ERR_CODE_GRAMMAR_EVALUATION_ERROR = 503;
	public static final String ERR_MSG_GRAMMAR_EVALUATION_ERROR = "Error Grammar Evaluation";

	public static final int ERR_CODE_PRONOUNCE_EVALUATION_ERROR = 504;
	public static final String ERR_MSG_PRONOUNCE_EVALUATION_ERROR = "Error Pronounce Evaluation";

	public static final int ERR_CODE_TTS_EVALUATION_ERROR = 505;
	public static final String ERR_MSG_TTS_EVALUATION_ERROR = "Error TTS";

	public static final int ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR = 506;
	public static final String ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR = "Error WebSocket closed improperly";

	//NLP
	public static final int ERR_CODE_NLP_ANALYSIS_ERROR = 507;
	public static final String ERR_MSG_NLP_ANALYSIS_ERROR = "ERROR GRPC NLP ANALYSIS";

	// BQA SERVICE
	public static final int ERR_CODE_SEARCH_DB_FAILURE = 508;
	public static final String ERR_MSG_SEARCH_DB_FAILURE = "Can't find the Data in Database";;
	public static final int ERR_CODE_SEARCH_SOLR_FAILURE = 509;
	public static final String ERR_MSG_SEARCH_SOLR_FAILURE = "Can't find the Data in Solr";;
	public static final int ERR_CODE_SEARCH_SOLR_IO_ERROR = 510;
	public static final String ERR_MSG_SEARCH_SOLR_IO_ERROR = "Error Solr Server IO";;
	public static final int ERR_CODE_SEARCH_SOLR_TOO_MANY_RESULT = 511;
	public static final String ERR_MSG_SEARCH_SOLR_TOO_MANY_RESULT = "Error Too Many Solr Result";;
	public static final int ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR = 512;
	public static final String ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR = "Error Solr Server";;
	public static final int ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR = 513;
	public static final String ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR = "Error NLP Result is nothing";
	public static final int ERR_CODE_SEARCH_DOMAIN_PARAMETER_ERROR = 514;
	public static final String ERR_MSG_SEARCH_DOMAIN_PARAMETER_ERROR = "Error Domain parameter is nothing";



	public static final int ERR_CODE_PRONOUNCE_SERVER_MANAGER_ERROR = 515;
	public static final String ERR_MSG_PRONOUNCE_SERVER_MANAGER_ERROR = "Error Pronounce Server";

	public static final int ERR_CODE_TTS_SERVER_MANAGER_ERROR = 516;
	public static final String ERR_MSG_TTS_SERVER_MANAGER_ERROR = "Error TTS Server";


	// MGMT
	public static final int ERR_CODE_MGMT_UPDATE_ERROR_Q = 601;
	public static final String ERR_MSG_MGMT_UPDATE_ERROR_Q = "Fail Insert or Update  Question table";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_Q = 602;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_Q = "Fail Insert or Update  Question table, Count 0";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_A = 603;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_A = "Fail Insert or Update  Answer table, Count 0";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX = 604;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX = "Fail Insert or Update  Q&A Index table, Count 0";
	public static final int ERR_CODE_MGMT_DELETE_ERROR_Q = 605;
	public static final String ERR_MSG_MGMT_DELETE_ERROR_Q = "Fail DELETE  Q&A table";
	public static final int ERR_CODE_MGMT_SELECT_ERROR_Q = 606;
	public static final String ERR_MSG_MGMT_SELECT_ERROR_Q = "Fail SELECT  Q&A table";
	
	// INDEX
	public static final int ERR_CODE_INDEX_SOLR_SYSTEM_ERROR = 607;
	public static final String ERR_MSG_INDEX_SOLR_SYSTEM_ERROR = "Error Indexing Solr Server";
	public static final int ERR_CODE_INDEX_DOMAIN_NOTHING = 608;
	public static final String ERR_MSG_INDEX_DOMAIN_NOTHING = "Error Indexing data is nothing";
	// NLP

	// EXCEL
	public static final int ERR_CODE_EXCEL_CREATE_ERROR = 609;
	public static final String ERR_MSG_EXCEL_CREATE_ERROR = "Fail To Create Q&A Excel Document";
	public static final int ERR_CODE_EXCEL_UPLOAD_ERROR = 610;
	public static final String ERR_MSG_EXCEL_UPLOAD_ERROR = "Fail To Upload Q&A Excel Document";
}