package ai.mindslab.engedu.admin.servicement.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class SelectOptVO  implements Serializable {

    private String serviceType;
    private String detailName;
}
