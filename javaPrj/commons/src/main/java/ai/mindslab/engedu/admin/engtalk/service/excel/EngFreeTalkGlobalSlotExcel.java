package ai.mindslab.engedu.admin.engtalk.service.excel;

import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminVO;
import ai.mindslab.engedu.admin.engtalk.service.EngFreeTalkGloballSlotAdminService;
import ai.mindslab.engedu.admin.engtalk.service.EngFreeTalkLocalSlotAdminService;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class EngFreeTalkGlobalSlotExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    EngFreeTalkGloballSlotAdminService  engFreeTalkGloballSlotAdminService;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 2500, 8000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,3);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"표현", "대체표현"};

       List<EngTalkGlobalSlotAdminVO> list = engFreeTalkGloballSlotAdminService.engTalkGlobalSlotExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;

        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(EngTalkGlobalSlotAdminVO vo : list){
            String[] data = {
                    vo.getSlotKey(),
                    vo.getSlotValue()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}
