package ai.mindslab.engedu.admin.dialog.service;

import ai.mindslab.engedu.admin.dialog.dao.DialogAdminMapper;
import ai.mindslab.engedu.admin.dialog.dao.data.DialogAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class DialogAdminService {

    @Autowired
    private DialogAdminMapper mapper;

    public int dialogAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.dialogAdminCount(paramMap);
    }

    public List<DialogAdminVO> dialogAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.dialogAdminList(paramMap);
    }

    public List<DialogAdminVO> dialogAdminExcelList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.dialogAdminExcelList(paramMap);
    }

    public DialogAdminVO dialogAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.dialogAdmin(paramMap);
    }
}
