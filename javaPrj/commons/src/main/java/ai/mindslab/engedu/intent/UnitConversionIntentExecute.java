package ai.mindslab.engedu.intent;

import java.util.HashMap;
import java.util.Map;

import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.common.codes.IRestCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.dic.service.UnitConversionService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UnitConversionIntentExecute implements IntentExecute {
	
	@Autowired
	private UnitConversionService unitConversionService ;
	
	@Autowired
	private IntentMsgService intentMsgService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO executeMessageVO = new IntentExecuteMessageVO();

		try {
			
			String QUESTION_TYPE="QT0002";
			String randomFlag="1";
			
			String question=intentExecuteVO.getInputStr().replaceAll("[,.!?;]*", "");

			QaIndexVo qaIndexVo = unitConversionService.searchKeyword(question ,QUESTION_TYPE);
			QaAnswerVo qaAnswerVo= null;
			String resultMsg="";
			
			if (qaIndexVo == null  || qaIndexVo.getIndexId() == 0) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.UNIT_CONVERSION, IntentServiceMsg.UNIT_CONVERSION_NOT_FOUND);
			
			} else {
				if (qaIndexVo != null) {
					log.debug("qaIndexVo = {}",qaIndexVo.toString());
					Map<String, String> paramMap = new HashMap<>();
					paramMap.put("questionId", qaIndexVo.getQuestionId()+"");
					paramMap.put("randomFlag", randomFlag);
					
					qaAnswerVo = unitConversionService.getAnswer(paramMap);
				}
			}
			
			if (qaAnswerVo == null ||  qaAnswerVo.getAnswerId() ==0 ) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.UNIT_CONVERSION, IntentServiceMsg.UNIT_CONVERSION_NOT_FOUND);
			} else {
				resultMsg = qaAnswerVo.getAnswer();
			}
			
			log.info("UnitConversionIntentExecute resultMsg:" + resultMsg);

			executeMessageVO.setResultMsg(resultMsg);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return executeMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}


}
