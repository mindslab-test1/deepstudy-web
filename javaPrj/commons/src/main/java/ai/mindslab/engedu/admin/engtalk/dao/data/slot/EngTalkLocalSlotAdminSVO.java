package ai.mindslab.engedu.admin.engtalk.dao.data.slot;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class EngTalkLocalSlotAdminSVO extends EngTalkLocalSlotAdminVO implements Serializable {
    private String creatorId;
    private String updatorId;


    public void setData(EngTalkLocalSlotAdminVO vo){

        this.setBrandId(vo.getBrandId());
        this.setBookId(vo.getBookId());
        this.setChapterId(vo.getChapterId());
        this.setQuestionId(vo.getQuestionId());
        this.setOldSlotKey(vo.getOldSlotKey());
        this.setSlotKey(vo.getSlotKey());
    }
}
