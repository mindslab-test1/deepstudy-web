package ai.mindslab.engedu.common.utils;

public class EngStringUtil {

    public static String specialCharacterRemove(String str){

        String result = str.replaceAll("[,.!?;]*", "")
                .replaceAll("\\s{2,}", " ");

        return result;
    }


}
