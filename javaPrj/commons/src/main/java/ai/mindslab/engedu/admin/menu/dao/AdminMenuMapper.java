package ai.mindslab.engedu.admin.menu.dao;

import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuTreeVO;
import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AdminMenuMapper {

    List<AdminMenuVO> selectParentMenu(Map<String, Object> pramMap) throws EngEduException;
    List<AdminMenuVO> selectSubMenu(Map<String, Object> pramMap) throws EngEduException;
    List<AdminMenuTreeVO> selectParentTreeMenu(Map<String, Object> pramMap) throws EngEduException;
    List<AdminMenuTreeVO> selectSubTreeMenu(Map<String, Object> pramMap) throws EngEduException;
    AdminMenuTreeVO selectMenuAdmin(Map<String, Object> pramMap) throws EngEduException;
    int addMenuAdmin(Map<String, Object> pramMap) throws EngEduException;
    int addMenuRoleAdmin(Map<String, Object> pramMap) throws EngEduException;
    int updateMenuAdmin(Map<String, Object> pramMap) throws EngEduException;
    int updateMenuRoleAdmin(Map<String, Object> pramMap) throws EngEduException;
    int deleteMenuAdmin(Map<String, Object> pramMap) throws EngEduException;

}
