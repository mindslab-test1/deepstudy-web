package ai.mindslab.engedu.adminweb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuVO;
import ai.mindslab.engedu.admin.menu.service.AdminMenuService;
import ai.mindslab.engedu.adminweb.common.base.BaseAdminInfo;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/view")
public class ViewController extends CommonController {

    @Autowired
    AdminMenuService service;

     @Value("${client.brand.code}")
    private String brandId;

     @Autowired
     private BaseAdminInfo baseAdminInfo;


    @RequestMapping("/layout")
    public ModelAndView  mainLayout(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = getUserInfo(req);

        String firstPage = baseAdminInfo.getFirstPage();

        param.put("brandId", brandId);
        param.put("roleId", vo.getRoleId());

        List<AdminMenuVO> list = service.getMenuList(param);
        view.addObject("menuList", list);
        view.addObject("firstPage",firstPage);
    	 view.setViewName("layout/layout");
        return view;
    }

}
