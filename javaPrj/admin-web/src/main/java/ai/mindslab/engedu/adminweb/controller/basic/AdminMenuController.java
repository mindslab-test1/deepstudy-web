package ai.mindslab.engedu.adminweb.controller.basic;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuTreeVO;
import ai.mindslab.engedu.admin.menu.service.AdminMenuService;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("view/menu/auth")
public class AdminMenuController extends CommonController {

    @Autowired
    AdminMenuService service;

    @Value("${client.brand.code}")
    private String brandId;


    @RequestMapping("/list")
    public ModelAndView userAuthList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("menu/menu");
        return view;
    }



        @RequestMapping("/menuList")
     public ModelAndView menuList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        List<AdminMenuTreeVO> list = null;

        UserVO vo = getUserInfo(req);

        param.put("brandId", brandId);
        param.put("roleId", vo.getRoleId());

        //최상위 노드 호출할때...
        if(!param.containsKey("nodeid")
                  || (param.containsKey("nodeid") && (param.get("nodeid") == null || param.get("nodeid") == ""))){

            list =service.selectParentTreeMenu(param);
        }else{
            //자식 호출 시 .
             param.put("menuId", param.get("nodeid").toString());
            list = service.selectSubTreeMenu(param);
        }


       	view.addObject("records", list.size());
       	view.addObject("total",0 );
       	view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/addMenuAdmin")
     public ModelAndView addMenuAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());
         param.put("brandId", brandId);

         BaseResponse<Object> resp = service.addMenuAdmin(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }


     @RequestMapping("/selectMenu")
     public ModelAndView selectMenu(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         AdminMenuTreeVO menuVo  = service.selectMenuAdmin(param);

         view.addObject("vo", menuVo);
         view.setViewName("jsonView");

        return view;
     }


     @RequestMapping("/updateMenuAdmin")
     public ModelAndView updateMenuAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         BaseResponse<Object> resp = service.updateMenu(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }


     @RequestMapping("/changeStatus")
     public ModelAndView deleteMenuAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         BaseResponse<Object> resp = service.deleteMenuAdmin(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }


}
