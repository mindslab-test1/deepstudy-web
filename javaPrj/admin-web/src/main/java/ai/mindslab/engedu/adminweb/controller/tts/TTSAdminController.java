package ai.mindslab.engedu.adminweb.controller.tts;


import ai.mindslab.engedu.admin.tts.dao.data.TtsLanguageVO;
import ai.mindslab.engedu.adminweb.common.base.BaseAdminInfo;
import ai.mindslab.engedu.adminweb.common.http.HttpProcess;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Controller
@RequestMapping("view/tts")
public class TTSAdminController extends CommonController {


    @Autowired
    HttpProcess http;


    @Value("${client.tts.domain}")
    private String serverUrl;

    @Value("${tts.file.path}")
    private String ttsFilePath;

    @Autowired
    DownloadView downloadView;


    @Autowired
    private BaseAdminInfo baseAdminInfo;



    @RequestMapping("/list")
    public ModelAndView mainView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        ArrayList<TtsLanguageVO> langSelectList = baseAdminInfo.getTtsLangSelectList();
        view.addObject("langSelectList",langSelectList);
        view.setViewName("tts/tts");
        return view;

    }


    @RequestMapping("/convert/text")
    public ModelAndView convertText(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("jsonView");

        Map<String, Object> param = reqToHash(req);

        JSONObject obj = new JSONObject();
        StringBuilder sb = new StringBuilder();

        try {
            sb.append(serverUrl);
            sb.append("uapi/tts/simpleTts.json?");
            sb.append("script=");
            sb.append(URLEncoder.encode(param.get("userText").toString(), "UTF-8"));
            sb.append("&language=" + URLEncoder.encode(param.get("lang").toString(), "UTF-8"));

            String pattern = "^(https?)";

            Pattern p = Pattern.compile(pattern);
            Matcher mc = p.matcher(sb.toString());

            if (mc.find()) {
                if (mc.group(1).equals("https")) {
                    obj = http.sendReceiveHttps(sb.toString());
                } else {
                    obj = http.sendReceiveHttp(sb.toString());
                }
            }

            JSONObject result = obj.getJSONObject("result");

            String ttsUrl = result.getString("ttsUrl");

            mc = p.matcher(ttsUrl);
            String midiFile = null;
            if (mc.find()) {
                if (mc.group(1).equals("https")) {
                    midiFile = http.createDownFileByHttps(ttsFilePath, ttsUrl);
                } else {
                    midiFile = http.createDownFileByHttp(ttsFilePath, ttsUrl);
                }
            }

            JSONObject ttsObj = new JSONObject();
            ttsObj.put("ttsUrl", ttsUrl);
            ttsObj.put("fileUrl", midiFile);

            view.addObject("data", ttsObj.toString());

        }catch(Exception e){
            e.printStackTrace();
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return view;
    }

    @RequestMapping("/downfile")
    public ModelAndView downFile(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);

        String fileName = param.get("fileName").toString();



        String[] tempFileUrlArr = fileName.split("/record");
        String fileUrl;
        if(tempFileUrlArr.length == 2){
            fileUrl = "/record" + tempFileUrlArr[1];
        }else{
            fileUrl = fileName;
        }

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }


   /* @RequestMapping("/downfile")
    public void downloadPDFResource(HttpServletRequest req, HttpServletResponse res ) throws Exception {

        Map<String, Object> param = reqToHash(req);

        String fileName = param.get("fileName").toString();
        File file = new File(ttsFilePath + fileName);
        if (file.exists()) {

            //get the mimetype
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                //unknown mimetype so set the mimetype to application/octet-stream
                mimeType = "application/octet-stream";
            }
            res.setContentType(mimeType);

            res.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            res.setContentLength((int) file.length());
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            FileCopyUtils.copy(inputStream, res.getOutputStream());
        }
    }*/
}
