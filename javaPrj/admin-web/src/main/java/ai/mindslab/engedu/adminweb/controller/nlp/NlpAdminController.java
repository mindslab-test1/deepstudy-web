package ai.mindslab.engedu.adminweb.controller.nlp;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("view/nlp/test")
public class NlpAdminController {

     @Value("${client.server.domain}")
    private String serverUrl;

     @RequestMapping("/list")
    public ModelAndView mainView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.addObject("nlpUrl", serverUrl);
        view.setViewName("nlp/nlp_main");
        return view;

    }
}
