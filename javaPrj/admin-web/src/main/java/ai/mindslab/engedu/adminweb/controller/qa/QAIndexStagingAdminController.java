package ai.mindslab.engedu.adminweb.controller.qa;


import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.qa.index.service.QAIndexStagingAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/qa/index/staging")
public class QAIndexStagingAdminController {

    @Autowired
    private QAIndexStagingAdminService qaIndexStagingAdminService;

    @RequestMapping("/allReset")
    public String allReset(
            HttpServletRequest request,
            HttpServletResponse response)
    {

        HttpSession session = request.getSession();
        UserVO userVO = null;
        String result = "";

        if (session.getAttribute("userInfo") != null) {

            userVO = (UserVO)session.getAttribute("userInfo");

        }

        try{

            if(userVO != null){

                Map<String, Object> param = new HashMap<>();
                result = qaIndexStagingAdminService.AllReset(param);

            }else{

                result = "login error";

            }

        }catch(Exception e){

            log.error("allReset Error :{}"+ e.getMessage());
            e.printStackTrace();
            result = "error";

        }finally {

            return result;

        }


    }

    @RequestMapping("/oneReset")
    public String oneReset(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(name="questionId") int questionId)
    {

        HttpSession session = request.getSession();
        UserVO userVO = null;
        String result = "";

        if (session.getAttribute("userInfo") != null) {

            userVO = (UserVO)session.getAttribute("userInfo");
        }

        try{

            if(userVO != null){

                result = qaIndexStagingAdminService.OneReset(questionId);

            }else{

                result = "login error";

            }

        }catch(Exception e){

            log.error("oneReset Error :{}"+ e.getMessage());
            e.printStackTrace();
            result = "error";

        }finally {

            return result;

        }

    }

}
