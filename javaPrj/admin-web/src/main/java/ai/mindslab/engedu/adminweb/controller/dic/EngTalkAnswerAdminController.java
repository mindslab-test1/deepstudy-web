package ai.mindslab.engedu.adminweb.controller.dic;

import ai.mindslab.engedu.admin.engtalk.dao.data.answer.EngTalkAdminAnswerVO;
import ai.mindslab.engedu.admin.engtalk.service.EngFreeTalkAnswerAdminService;
import ai.mindslab.engedu.admin.engtalk.service.excel.EngFreeTalkAnswerExcel;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/view/engtalk/answer")
public class EngTalkAnswerAdminController extends CommonController {

    private static Logger logger  = LoggerFactory.getLogger(EngTalkAnswerAdminController.class);

    @Autowired
    EngFreeTalkAnswerAdminService service;

    @Autowired
    EngFreeTalkAnswerExcel engFreeTalkAnswerExcel;


    @Autowired
    DownloadView downloadView;

    @RequestMapping("/list")
    public ModelAndView mathDicView(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView view = new ModelAndView();
        view.setViewName("engtalk/eng_talk_answer");
        return view;

    }

    @RequestMapping("/diclist")
    public ModelAndView getEngTalkAnswerList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.EngTalkAnswerAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<EngTalkAdminAnswerVO> list = service.EngTalkAnswerAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/selectDetail")
    public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);

        EngTalkAdminAnswerVO vo = service.EngTalkAnswerAdmin(param);
        view.addObject("data", vo);
        view.setViewName("jsonView");

        return view;

    }


    @RequestMapping("/updateEngTalkQuestionAdmin")
    public ModelAndView updateEngTalkQuestionAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);

        param.put("user_id", vo.getUserId());

        BaseResponse<Object> resp = service.updateEngTalkAnswerAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }


    @RequestMapping("/deleteEngTalkQuestionAdmin")
    public ModelAndView deleteEngTalkQuestionAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);
        BaseResponse<Object> resp = service.deleteEngTalkAnswerAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

    @RequestMapping(value = "/downSample")
    public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        String fileUrl  = req.getSession().getServletContext().getRealPath("resources/sample/engTalkAnswer_sample.xlsx");

        logger.info("down sample url : " + fileUrl);

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }


    @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        param.put("searchType",param.get("excelDownSearchType"));
        param.put("searchText",param.get("excelDownSearchText"));

        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "engTalkAnswer_"+ date +".xlsx";
        String title ="영어대화 답변관리";

        engFreeTalkAnswerExcel.setTitle(title);
        Workbook book = engFreeTalkAnswerExcel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }


    @RequestMapping(value="/insertData")
    public ModelAndView uploadExcelData(HttpServletRequest req,
                                        @RequestParam("file") MultipartFile uploadfile)throws Exception {
        ModelAndView view = new ModelAndView();
        UserVO vo = getUserInfo(req);
        BaseResponse<Object> resp = service.uploadExcelData(vo, uploadfile);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }
}
