<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script src="${path}/resources/js/security/rsa/jsbn.js"></script>
	<script src="${path}/resources/js/security/rsa/rsa.js"></script>
	<script src="${path}/resources/js/security/rsa/prng4.js"></script>
	<script src="${path}/resources/js/security/rsa/rng.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>

</head>
<body>
<script>

    var role_type = '';

    function createGrid(){
        $("#grid").jqGrid({
            url : serverUrl('view/user/auth/userDataList'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : 'User Id', name : 'userId', width : '100px', align : 'center', formatter : formatter_names},
                {label : 'User Name', name : 'userName', width : '150px', align : 'center'},
                {label : 'Email', name : 'email', width : '150px', align : 'center'},
                {label : 'Role', name : 'roleName', width : '150px', align : 'center'},
                {label : 'Activate', name : 'activeYn', width : '150px', align : 'center', formatter : formatter_useYn},
                {label : '생성일', name : 'createdTime', width : '150px', align : 'center'}
                ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            multiselect: true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'userId',
            sortorder: 'asc',
            loadComplete : function(data) {
                customResize('grid');
            }
        });
    }


     function checkClickEvent(){
        var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');      //체크된 row id들을 배열로 반환
        var keys ={};
        var keyArray =[];
        var rowObject = null;
        for(var i = 0; i < ids.length; i++){
            rowObject = $("#grid").getRowData(ids[i]);      //체크된 id의 row 데이터 정보를 Object 형태로 반환
            var keys ={
                userId : $(rowObject.userId).text()
            };

            console.log(keys);

            keyArray.push(keys);
        }

        var param ={
            unique : JSON.stringify(keyArray)
        };

        return param;
    }


    function formatter_names(cellvalue, options, rowObject){
        var html = "<a style=\"text-decoration: underline;color: #6BA8D1;\" href='#' onclick=\"showlayerPopUp('update', '"+rowObject.userId + "')\">" + rowObject.userId +"</a>";
        return html;
    }


    function formatter_useYn(cellvalue, options, rowObject){
        var useYnStr ='';
        var uesYn = cellvalue;

        if(uesYn.toLowerCase() ==='y' || uesYn.toLowerCase() ==='yes'){
            useYnStr ='사용';
        }else{
            useYnStr ='미사용';
        }

        return useYnStr;
    }


    function searchUser(){

        var param ={
            searchText:  $("#searchText").val()
        };
        reloadGrid(param);
    }

     function reloadGrid(param){
        console.log(param);
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }


    function showlayerPopUp(type, userId){

        if(type ==='update'){
            makeUpdateLayer(userId);
        }else{

            $('.lyrWrap').fadeIn(300);
            $('#lyr_modify').hide();
            $('#lyr_add').show();
            $('#lyr_delete').hide();
        }
    }

    function makeUpdateLayer(userId){


        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();
        $('#lyr_add').hide();
        $('#lyr_delete').hide();

        var param ={
            userId : userId
        };

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/user/auth/selectUserAuth"),
            data : param,
            success: function(data){
                $( '#data_tbody').empty();

                var result = data['userInfo'];
                var roleTypes =  role_type.roleList;
                var $table = $("#data_tbody");

                var $tr = null;
                var $td = null;
                var $th = null;

                $.each(result, function(key, value){
                    $tr = $('<tr/>');
                    $td = $('<td/>');
                    $th = $('<th/>');

                    $th.attr('scope', 'row');

                    if(key ==='userId'){
                        $th.text('user Id');
                    }else if(key ==='userKey'){
                        $th.text('pw');
                    }else if(key ==='userName'){
                        $th.text('userName');
                    }else if(key ==='email') {
                        $th.text('Email');
                    }else if(key ==='roleName') {
                        $th.text('role');
                    }else if(key ==='activeYn') {
                        $th.text('activate');
                    }else if(key ==='createdTime'){
                        $th.text('생성일');
                    }

                     if(value == null || value == 'undefined'){
                        value ='';
                    }

                     if(key =='userId' || key =='createdTime'){
                        $td.html('<span id="'+key +'">'+ value +'</span>');
                     }else if(key ==='userKey'){
                         $td.html('<input type="password"  id="'+ key +'" class="ipt_txt" value="'+value+'">');
                     }else if(key ==='roleName'){
                         var $ro_select = $("<select id='roleName'/>");

                         roleTypes.forEach(function (obj,index) {
                            var opt = $('<option></option>');
                            console.log(obj);
                             opt.attr({'value' : obj.roleId});
                             opt.append( obj.roleName);

                             if(value == obj.roleName){
                                 opt.attr("selected", 'selected');
                             }
                             $ro_select.append(opt);

                         });

                         $td.append($ro_select);

                     }else if(key ==='activeYn'){
                         var $select = $('<select id="'+ key +'"/>');
                         var yOpt = $('<option></option>');
                         var NOpt = $('<option></option>');

                         yOpt.attr({'value' : 'Y'});
                         yOpt.append('사용');
                         NOpt.attr({'value' : 'N'});
                         NOpt.append('미사용');

                         if(value.toUpperCase() ==='Y'){
                            yOpt.prop("selected", "selected");
                         }else{
                            NOpt.prop("selected", "selected");
                         }

                         $select.append(yOpt);
                         $select.append(NOpt);
                         $td.append($select);

                     }else{
                         $td.html('<input type="text"  id="'+ key +'" class="ipt_txt" value="'+value+'">');
                     }

                    $tr.append($th);
                    $tr.append($td);
                    $table.append($tr);
                });
            },
              fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }

     function cancelUpate(type){

        if(type === 'add'){
            initaddUserArea();
            $('.lyrWrap').fadeOut(300);
            $('#lyr_add').hide();
        }else{
            $('.lyrWrap').fadeOut(300);
            $('#lyr_modify').hide();
            $( '#data_tbody').empty();
        }
    }

    function updateUserAuth(type){

        var rsaKeys = getRSAKey();

        var userId = $("#userId").text();
        var securedUserId ='';
        var userKey = $("#userKey").val();
        var userName = $("#userName").val();
        var securedUserKey ='';
        var email = $("#email").val();
        var activeYn =  $("#activeYn option:selected").val();
        var roleName = $("#roleName option:selected").val();

        securedUserId = encryptedId(userId, rsaKeys.publicKeyModulus, rsaKeys.publicKeyExponent);

        if(userKey !=='undefined'  &&  $.trim(userKey).length > 0){
            securedUserKey =  encryptedUserKey(userKey, rsaKeys.publicKeyModulus, rsaKeys.publicKeyExponent);
        }else{
            securedUserKey =userKey;
        }


        var param = {
            userId : securedUserId,
            userKey: securedUserKey,
            userName :userName,
            email : email,
            roleName :roleName,
            activeYn : activeYn
        }

        $.ajax({

            type:"post",
            dataType: "json",
            url: serverUrl("view/user/auth/updateUserAuth"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('업데이트 되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_modify').hide();
                    $("#grid").trigger("reloadGrid");
                    $( '#data_tbody').empty();

                }else{
                    alert('업데이트에 실패하였습니다.');
                }
            },
            fail:function(data){
                alert('update fail');
                console.log(data)
            },
            error: function(data, status, error){
                alert('update error');
                console.log(error);
            }
        });
    }

    function addUserAuth(){
         var rsaKeys = getRSAKey();

         var userId = $("#add_userId").val();
         var securedUserId ='';
         var userKey = $("#add_userKey").val();
         var userName = $("#add_userName").val();
         var securedUserKey ='';
         var email = $("#add_email").val();
         var activeYn =  $("#add_activeYn option:selected").val();
         var roleName = $("#add_roleName option:selected").val();


         if($.trim(userId).length == 0){
             alert('사용자 id를 입력하십시오.');
             return false;
          }else if($.trim(userKey).length == 0){
             alert('사용자 패스워드를 입력하십시오.');
              return false;
          }

          securedUserId = encryptedId(userId, rsaKeys.publicKeyModulus, rsaKeys.publicKeyExponent);
          securedUserKey =  encryptedUserKey(userKey, rsaKeys.publicKeyModulus, rsaKeys.publicKeyExponent);


          var param = {
              userId : securedUserId,
              userKey: securedUserKey,
              userName :userName,
              email : email,
              roleName :roleName,
              activeYn : activeYn
          };

           $.ajax({

            type:"post",
            dataType: "json",
            url: serverUrl("view/user/auth/addUserAuth"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('사용자가 추가되었습니다.');

                    initaddUserArea();
                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_add').hide();
                    $("#grid").trigger("reloadGrid");

                }else if(result.code =='300'){
                       alert(result.msg);
                }else{
                    alert('사용자 추가에 실패하였습니다.');
                }
            },
            fail:function(data){
                alert('update fail');
                console.log(data)
            },
            error: function(data, status, error){
                alert('update error');
                console.log(error);
            }
        });
    }

    function initaddUserArea(){

        $("#add_userId").val('');
        $("#add_userKey").val('');
        $("#add_userName").val('');
        $("#add_email").val('');
        $('#add_activeYn option:eq(0)').prop('selected', true);
        $('#add_roleName option:eq(0)').prop('selected', true);
    }


    function deleteUserData(){

        var param = checkClickEvent();

        $.ajax({
            type: "post",
            dataType: "json",
            url: serverUrl("view/user/auth/deleteUserAuth"),
            data: param,
            success: function (data) {
                var result = data.result;
                if (result.code == '200') {
                    alert('삭제되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_delete').hide();
                    $("#grid").trigger("reloadGrid");
                } else {
                    alert('삭제가 실패하였습니다.');
                }
            },
            fail: function (data) {
                console.log(data)
            },
            error: function (data, status, error) {
                console.log(error);
            }
        });
    }


    function initEvent(){

        $('#searchBtn').on('click', function(){
            searchUser();
        });

        $('#searchText').on('keyup', function (e){
            if(e.keyCode === 13){
                searchUser();
            }
        });

          $('.lyr_delete').on('click',function() {
              //선택된 대상이 있는지 확인.
              var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');

              if(ids =='undefined' || ids.length ==0) {
                  alert('삭제할 대상을 선택하십시오.');
              }else{
                  $('.lyrWrap').fadeIn(300);
                  $('#lyr_add').hide();
                  $('#lyr_modify').hide();
                  $('#lyr_delete').show();
              }
          });


        $('.lyr_add').on('click',function(){
			$('.lyrWrap').fadeIn(300);
			$('#lyr_add').show();
			$('#lyr_modify').hide();
			$('#lyr_delete').hide();

            var roleTypes =  role_type.roleList;
            var $ro_select = $("#add_roleName");

            $ro_select.empty();

            roleTypes.forEach(function (obj, index) {
                var opt = $('<option></option>');
                console.log(obj);
                opt.attr({'value' : obj.roleId});
                opt.append( obj.roleName);
                console.log(index);
                if(index == 0){
                    opt.attr("selected", 'selected');
                }

                $ro_select.append(opt);
            });
		});
    }



    $(document).ready(function(){
        initPage();
        createGrid();
        getRoleType();
        customResize('grid');
        initEvent();
    });

    function getRoleType(){
        var param={};

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/user/auth/getUserRole"),
            data : param,
            success: function(data){
                role_type = data;
            },
              fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }

    $(window).on('resize', function () {
        customResize('grid');
    }).trigger('resize');



    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }

</script>
<div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
	<div class="lyr_bg"></div>
    <div id="lyr_add" class="lyrBox">
    	<div class="lyr_top">
        	<h3>추가하기</h3>
            <button type="button"  onclick="cancelUpate('add');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">User id</th>
                        <td><input type="text" class="ipt_txt" id="add_userId" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">User Name</th>
                        <td><input type="text" class="ipt_txt" id="add_userName" value=""></td>
                    </tr>
                     <tr>
                        <th scope="row">pw</th>
                        <td><input type="password" class="ipt_txt" id="add_userKey" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">email</th>
                        <td><input type="text" class="ipt_txt" id="add_email" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">role</th>
                        <td>
                            <select id="add_roleName">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">role</th>
                        <td>
                            <select id="add_activeYn">
                                <option value="y">사용</option>
                                 <option value="n">미사용</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="addUserAuth();" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpate('add');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_modify" class="lyrBox" >
    	<div class="lyr_top">
        	<h3>수정하기</h3>
            <button type="button" onclick="cancelUpate('update');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody id="data_tbody"></tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="updateUserAuth();" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpate('update');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox02">
		<input type="hidden" id="del_target" value=""/>
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="deleteUserData();" class="btn_clr">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
</div>
<!-- //.lyrWrap modal 창 -->

<!-- .titArea -->
<div class="titArea">
 <h3>User/권한 관리</h3>
 <div class="path">
    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
	 <span>기본관리</span>
	 <span>User/권한 관리</span>
 </div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
	<!-- .fl -->
	<div class="fl">
		<%--<div class="selectbox">
			<label for="ex_select">선택</label>
			<select id="ex_select">
                <option value="">선택</option>
                <option value="word">단어</option>
			</select>
		</div>--%>
		<div class="srchbox">
			<input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
			<button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
		</div>
	</div>
	<!-- //.fl -->
	<!-- .fr -->
	<div class="fr">
		<ul class="btn_lst">
			<li><button  type="button" class="btn_clr lyr_add"><img src="${path}/resources/images/ico_add_bk.png" alt="삭제">추가</button></li>
            <li><button  type="button" class="btn_clr lyr_delete"><img src="${path}/resources/images/ico_delete_bk.png" alt="삭제">삭제</button></li>
		</ul>
	</div>
	<!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<!-- .stn -->
<div class="content">
  <div class="stn">
	<table id="grid"  class="tbl_lst"></table>
	<div id="pager"></div>
  </div> 	<!-- //.stn -->
</div>
</body>
</html>
