<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />
    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>


</head>
<body>

<script type="text/javascript">
    var sttUrl = '${sttUrl}';

    function initDatePicker(){

        $( "#ipt_date_start" ).datepicker({
             dateFormat: 'yy-mm-dd',
             onSelect: function(d,i){
                 $('#ipt_date_end').datepicker("option", "minDate", d);
             }
         });

         $( "#ipt_date_end" ).datepicker({
            dateFormat: 'yy-mm-dd',
             maxDate: 0, // 오늘 이후 날짜 선택 불가
        });
         var startDate = new Date();
         var endDate = new Date();

         startDate.setDate(startDate.getDate() - 1);

         $("#ipt_date_start").datepicker('setDate', startDate);
         $("#ipt_date_end").datepicker('setDate', endDate);
         $('#ipt_date_end').datepicker("option", "minDate", startDate);


    }

    function initPage(){

        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });


        $('#btn_srch').on('click', function(e){
             searchEval();
         });

         $('#searchText').on('keyup', function (e) {
             if(e.keyCode ==13){
                 searchEval();
             }
         });

    }

      function createGrid(){

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        var param ={
            startDate : startDate,
            endDate : endDate
        }


        $("#grid").jqGrid({
            url : serverUrl('view/eval/kor/monitor/korlist'),
            mtype : "POST",
            datatype : "json",
            postData : param,
            colModel : [
                {label : 'ID', name : 'readId', width : '10%', align : 'center', formatter: formatter_names},
                {label : '학습자 ID', name : 'userId', width : '10%', align : 'center'},
                {label : '평가일시', name : 'createdTime', width : '10%', align : 'center'},
                {label : '정답텍스트', name : 'answerText', width : '10%', align : 'center'},
                {label : '발화텍스트', name : 'userText', width : '25%', align : 'center'},
                {label : '평가점수', name : 'score', width : '10%', align : 'center'}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'readId',
            sortorder: 'desc',
            loadComplete : function(data) {
                customResize('grid');
			}
        });
    }

    function formatter_names(cellvalue, options, rowObject){
        var html = "<a style=\"text-decoration: underline;color: #6BA8D1;\" href='#' onclick=\"showdetailPopUp('"+rowObject.readId + "')\">" + rowObject.readId +"</a>";
        return html;
    }


    function showdetailPopUp(readId){
            $('.lyrWrap').fadeIn(300);
            $('#lyr_detail').show();


        var param={
            readId : readId
        };

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/eval/kor/monitor/korEvalAdmin"),
            data : param,
            success: function(data){

                var result = data['data'];

                $('#view_readId').html(result.readId);
                $('#view_userId').html(result.userId);
                $('#view_createdTime').html(result.createdTime);
                $('#view_answerText').html(result.answerText);
                $('#view_userText').html(result.userText);
                $('#view_answerText').html(result.answerText);
                $('#view_grammer').html(result.grammarScore);
                $('#view_pronounce').html(result.pronunceScore);
                $('#view_score').html(result.score);

                var path = result.filePath  + result.oralMedia;

                $('#view_record').html(path);



            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });


    }


    function closeModal(){
        $('.lyrWrap').fadeOut(300);
        $('#lyr_detail').hide();
    }



    function searchEval(){

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        var param ={
            searchType : $("#ex_select option:selected").val(),
            searchText:  $("#searchText").val(),
            startDate: startDate,
            endDate: endDate
        };

        reloadGrid(param);
    }


    function reloadGrid(param){
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }


    function excelDownLoad(){
        setCookie("fileDownload","false"); //호출
        checkDownloadCheck();
        //$('.wrap-loading').removeClass('display-none');
        loadSpinner();

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        $("#excelDownSearchType").val($("#ex_select option:selected").val());
        $("#excelDownSearchText").val($("#searchText").val());
        $("#excelStartDate").val(startDate).val();
        $("#excelEndDate").val(endDate).val();
        $("#excelDownForm").submit();

    }


    function setCookie(c_name,value){
        var exdate=new Date();
        var c_value=escape(value);
        document.cookie=c_name + "=" + c_value + "; path=/";
    }

    function checkDownloadCheck(){
        if (document.cookie.indexOf("fileDownload=true") != -1) {
            var date = new Date(1000);
            document.cookie = "fileDownload=; expires=" + date.toUTCString() + "; path=/";
            //프로그래스바 OFF
            //$('.wrap-loading').addClass('display-none');
            stopspin();
            return;
        }
        setTimeout(checkDownloadCheck , 100);
    }



    $(document).ready(function (){
	    initPage();
        initDatePicker();
	    createGrid();
        customResize('grid');

	});

    $(window).on('resize', function () {
       customResize('grid');
    }).trigger('resize');


</script>
<form style="display: hidden" action="eval/kor/monitor/excelDown" method="POST" id="excelDownForm">
    <input type="hidden" id="excelDownSearchType" name="excelDownSearchType" value=""/>
    <input type="hidden" id="excelDownSearchText" name="excelDownSearchText" value=""/>
    <input type="hidden" id="excelStartDate" name="excelStartDate" value=""/>
    <input type="hidden" id="excelEndDate" name="excelEndDate" value=""/>
</form>

    <div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
    </div>
<div class="lyrWrap">
	<div class="lyr_bg"></div>
    <div id="lyr_detail" class="lyrBox">
    	<div class="lyr_top">
        	<h3>평가모니터링</h3>
            <button type="button"  onclick="closeModal();" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">ID</th>
                        <td id="view_readId"></td>
                    </tr>
                    <tr>
                        <th scope="row">학습자 ID</th>
                        <td id="view_userId"></td>
                    </tr>
                     <tr>
                        <th scope="row">평가 일시</th>
                        <td id="view_createdTime"></td>
                    </tr>
                    <tr>
                        <th scope="row">정답 텍스트</th>
                        <td id="view_answerText"></td>
                    </tr>
                    <tr>
                        <th scope="row">발화 텍스트</th>
                        <td id="view_userText"></td>
                    </tr>
                    <tr>
                        <th scope="row">발화 음원</th>
                         <td>
                             <a class="link btn_audio_play" href="#none"><span id="view_record"></span></a>
                             <div class="audioBox">
                                 <audio id="myAudio" controls>
                                     <source type="audio/mpeg" src="${path}/resources/audio/aekukka.mp3">
                                 </audio>
                             </div>
                         </td>
                    </tr>
                   <tr>
                        <th scope="row">문법 점수</th>
                         <td id="view_grammer"></td>
                    </tr>
                     <tr>
                        <th scope="row">발음 점수</th>
                         <td id="view_pronounce"></td>
                    </tr>
                    <tr>
                        <th scope="row">평가 점수</th>
                         <td id="view_score"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
                <li><button type="button" onclick="closeModal();" class="btn_lyr_cancel">확인</button></li>
            </ul>
        </div>
    </div>
</div>

    <div class="titArea">
       <h3>평가 모니터링</h3>
       <div class="path">
       <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
       <span>영어 평가 관리</span>
       <span>평가 모니터링</span>
    </div>
    </div>
    <!-- //.titArea -->
    <!-- .srchArea -->
    <div class="srchArea">
     <!-- .fl -->
       <div class="fl">
          <div class="dateBox">
              <input type="text" id="ipt_date_start" class="ipt_txt" placeholder="시작일">
              <span class="hyphen">-</span>
              <input type="text" id="ipt_date_end" class="ipt_txt" placeholder="종료일">
           </div>
           <div class="selectbox">
               <label for="ex_select">선택</label>
               <select id="ex_select">
                   <option value="" selected>선택</option>
                   <option value="readId" >세션 ID</option>
                   <option value="userId">학습자 ID</option>
                   <option value="answerText">정답 텍스트</option>
               </select>
           </div>
           <div class="srchbox">
               <input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
               <button type="button" id="btn_srch" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
           </div>
       </div>
        <!-- //.fl -->
        <!-- .fr -->
        <div class="fr">
            <ul class="btn_lst">
                <li><a href="#" onclick="excelDownLoad();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드">엑셀 다운로드</a></li>
            </ul>
        </div>
        <!-- //.fr -->
    </div>
    <!-- //.srchArea -->
    <!-- .content -->
    <div class="content">
      <div class="stn">
        <table id="grid"  class="tbl_lst"></table>
        <div id="pager"></div>
      </div> 	<!-- //.stn -->
    </div>
</body>
</html>
