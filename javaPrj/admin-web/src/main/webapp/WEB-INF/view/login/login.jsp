<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<c:set var="path"  value="${pageContext.request.contextPath}" />
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<!-- icon_favicon -->
	<link rel="apple-touch-icon-precomposed" href="${path}/resources/images/ico_favicon_64x64.png">
	<link rel="shortcut icon" type="image/x-icon" href="${path}/resources/images/ico_favicon_64x64.ico"/>
	<%@ include file="../common/commonHeader.jspf" %>

	<title>maum Admin</title>
	<!-- resources -->
	<link rel="stylesheet" type="text/css" href="${path}/resources/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="${path}/resources/css/font.css" />
	<link rel="stylesheet" type="text/css" href="${path}/resources/css/login.css" />

	<script src="${path}/resources/js/security/rsa/jsbn.js"></script>
	<script src="${path}/resources/js/security/rsa/rsa.js"></script>
	<script src="${path}/resources/js/security/rsa/prng4.js"></script>
	<script src="${path}/resources/js/security/rsa/rng.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>

</head>

<body>
<!--[if lt IE 9]>
<div class="legacy_browser">
	<div class="legacyBox">
		<div class="tit">
			<h1><img src="resources/images/logo_mindslab_w.png" alt="MINDsLab"></h1>
			<button type="button" class="btn_legacy_close">닫기</button>
		</div>
		<div class="txt">
			사용중인 브라우저는 지원이 중단된 브라우저입니다.<br>
			원활한 온라인 서비스를 위해 브라우저를 <a href="http://windows.microsoft.com/ko-kr/internet-explorer/ie-11-worldwide-languages" target="_blank">최신 버전</a>으로 업그레이드 해주세요.
		</div>
	</div>
</div>
<![endif]-->

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!-- //.page loading -->
<form id="loginFrm" method="post" action="">
	<input type="hidden" name="securedUsername" id="securedUsername" value="" />
	<input type="hidden" name="securedPassword" id="securedPassword" value="" />
</form>
<form name="rsafrm" id="rsafrm">
	<input type="hidden" id="rsaPublicKeyModulus" name="rsaPublicKeyModulus" value="<c:out value="${publicKeyModulus}"/>" >
	<input type="hidden" id="rsaPpublicKeyExponent" name="rsaPpublicKeyExponent" value="<c:out value="${publicKeyExponent}"/>" >
</form>
<!-- loginBox -->
<form class="loginWrap" method="get" action="">
	<fieldset>
		<legend>Login</legend>
		<div class="loginBox">
			<div class="fl"><img src="${path}/resources/images/logo_mindslab_admin.png" alt="MINDsLab Admin"></div>

			<div class="fr">
				<span><input type="text" name="ipt_id" id="ipt_id" class="ipt_txt" title="User Name"  value=""   placeholder="User Name"></span>
				<span><input type="password" name="ipt_pw" id="ipt_pw" class="ipt_txt" title="Password" value="" placeholder="Password"></span>
				<span class="checks">
                	<%--<input type="checkbox" name="ipt_check" id="ipt_check" class="ipt_check">
                    <label for="ipt_check">Remember me</label>--%>
                </span>
				<span>
                	<em class="disbBox"></em>
                	<input type="button" name="btn_login" id="btn_login" class="btn_login disabled" title="Login" value="Login" disabled>
                </span>
			</div>
		</div>
	</fieldset>
</form>
<!-- //loginBox -->
<div class="copyRight"><span>MINDsLab &copy; 2018</span></div>

<script type="text/javascript">

</script>
<script type="text/javascript">

	function initLoginForm(){

        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
        // input 초기화
        $('.ipt_id, ipt_pw').each(function(){
            $(this).val('');
        });

        // 입력값 체크 (버튼 활성화)
        $('.ipt_txt').on('change keyup paste click', function(e) {
            var idValLth = $('#ipt_id').val().length;
            var pwValLth = $('#ipt_pw').val().length;



            if ( idValLth > 0 && pwValLth > 0) {
                $('.btn_login').removeClass('disabled');
                $('.btn_login').removeAttr('disabled');
                $('.disbBox').remove();
                //버튼이 활성화 될때만 엔터키 허용
                if(e.keyCode == 13){
                    login();
				}

            } else {
                $('.btn_login').addClass('disabled');
                $('.btn_login').attr('disabled');
            }
        });


        $('#btn_login').on('click', function(e){
            login();
		});

        // 브라우저 안내메세지 닫기
        $('.legacy_browser .legacyBox .tit .btn_legacy_close').on('click', function(e) {
            $('.legacy_browser').hide();
        });
	}


    function login(){

        var   id  = $("#ipt_id").val();
        var   pw  =  $("#ipt_pw").val();

        if(checkValidate(id, pw)){

            submitEncryptedForm(); // rsa 로 id / passwd 암호화

			var   userName  =    $("#loginFrm").find("#securedUsername").val();
			var   userPasswd  =  $("#loginFrm").find("#securedPassword").val();



            var param={
                userId :userName,
                userKey : userPasswd
            };

            $.ajax({
                type : "post",
                dataType: "json",
                url : serverUrl("login/userlogin"),
                data : param,
                success : function (data) {
                    console.log(data);
                    if(data.resultCode == 200){
                        console.log('성공');
                        location.href =serverUrl("view/layout");
                    }else{
                        alert(data.resultMsg);
                        return false;
                    }
                },
                fail:function(data){console.log(data)},
                error: function(data, status, error){
                    console.log(error);
                }
            });

        }

    }

    function submitEncryptedForm() {

		 var securedLoginForm = document.getElementById('loginFrm');
		 var rsa = new RSAKey();
		 var rsaF = document.rsafrm;

		 rsa.setPublic(rsaF.rsaPublicKeyModulus.value, rsaF.rsaPpublicKeyExponent.value);
		 // 사용자ID와 비밀번호를 RSA로 암호화한다.
	 	 securedLoginForm.securedUsername.value = rsa.encrypt($("#ipt_id").val());  // id 암호화
		 securedLoginForm.securedPassword.value = rsa.encrypt($("#ipt_pw").val());  // passwd 암호화
	 }

    function checkValidate(userName, userPasswd){

        if(userName =='' && userPasswd ==''){
            alert("아이디와 암호를 모두  입력하시오");
            return false;
        }else if(userName ==''){
            alert("아이디를   입력하시오");
            return false;
        }else  if(userPasswd ==''){
            alert("암호를 입력  입력하시오");
            return false;
        }
        return true;
    }


    $(document).ready(function (){
        initLoginForm();
    });

</script>
</body>
</html>


