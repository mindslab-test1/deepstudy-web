<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.theme.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script src="${path}/resources/js/jqgrid/src/grid.treegrid.js"></script>
    <script type="text/javascript" src="${path}/resources/js/common.js"></script>



</head>
<body>
<script>

    var role_type = '';

     //그리드생성
    function createGrid(){

        $("#grid").jqGrid({
            url : serverUrl('view/menu/auth/menuList'),
            mtype : "POST",
            datatype : "json",
            colModel : [
                {label : '메뉴', name : 'menuName', width : '30%', align :'left', sortable : false},
                {label : '주소', name : 'menuPath', width : '15%', align : 'center',  sortable : false},
                {label : '권한', name : 'roleName', width : '30%', align : 'center',  sortable : false},
                {label : 'Action', name : 'Action', width : '25%', align : 'right',  sortable : false, formatter: actionFormatter},
                {label : 'menuId' ,name:'menuId'  ,width:0, key:true ,hidden:true}            ],
            width : '1200',
            height : '360',
            treeGrid: true,
            treeGridModel: 'adjacency',
            treeIcons: {
                plus : 'ui-icon-plusthick',
                minus : 'ui-icon-minusthick',
				leaf : 'ui-icon-blank'
            },
            treeReader : {
                  level_field: "zlevel",
                  parent_id_field: "parentMenuId",
                  leaf_field: "lev",
                  expanded_field: "expanded"
            },

            ExpandColumn : 'menuName',
            viewrecords: true,
            gridview: false,
            autoencode:true,
            shrinkToFit : true,
            rownumbers:false,

            loadComplete : function(data) {
               var rData = $("#grid").jqGrid('getGridParam', 'data');
               console.log(rData);
               /*setTimeout(function(){
                   $.each(rData, function(index, value){
                       if(value.zlevel == 0 && value.loaded == false){
                           $("#grid").jqGrid('expandNode', value);
                       }
                   });
               }, 10);*/

               customResize('grid');
			}
        });

        setTreeGroupChangeDefine();
    }


     function  actionFormatter(cellvalue, options, rowObject){
        var addImage = '<%=request.getContextPath()%>/resources/images/ico_add_bk.png';
        var editImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var deleteImage = '<%=request.getContextPath()%>/resources/images/ico_delete_bk.png';

        var  html =  "<ul class=\"btn_lst\">"

        var depth = rowObject.depth;

        if(depth < 3){
            html += "<li><button type=\"button\" onclick=\"showAddSubModal('"+rowObject.menuId+"', '"+rowObject.depth+"');\" class=\"btn_type_w lyr_lowRank\"><img src="+addImage+" alt=\"하위 추가\">하위 추가</button></li>"
        }

        html += "<li><button type=\"button\" onclick=\"showUpdateModal('"+rowObject.menuId+"');\" class=\"btn_type_w lyr_mfy\"><img src="+editImage+" alt=\"수정\">수정</button></li>"
             + "<li><button type=\"button\" onclick=\"showDeleteModal('"+rowObject.menuId+"');\" class=\"btn_type_w lyr_del\"><img src="+deleteImage+" alt=\"삭제\">삭제</button></li></ul>";

		return  html;
	}


	function setTreeGroupChangeDefine(){
        $.fn.jqGrid.expandNode = defaultExpandNode;
        $.fn.jqGrid.collapseNode = defaultCollapseNode;

        /*트리 접고 필때....*/
        $.jgrid.extend({
            expandNode : function(rc) {
                var rowId = rc.domainId;
                if (!rc.lev) {
                    if(rc.parentMenuId || rc.parentMenuId == '0'){
                        var gridObj = $(this);
                        return defaultExpandNode.call(this, rc);
                    }
                }
            },

            collapseNode : function(rc) {
                if (!rc.lev) {
                    if(rc.parentMenuId || rc.parentMenuId == '0'){
                        return defaultCollapseNode.call(this, rc);
                    }
                }
            }
        });
    }


    function showAddMenuModal(){

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_highRank').show();

        //초기화...
        $('input[id^="add_"]').each(function(i, obj){
            $(obj).val('');
        });

        $('#add_depth').val(1);

        var roleTypes =  role_type.roleList;
        var $ro_select = $('#add_roleName');
        var label = $('#add_role_label');

        $ro_select.empty();

        roleTypes.forEach(function (obj,index) {
            var opt = $('<option></option>');

            opt.attr({'value' : obj.roleId});
            opt.append( obj.roleName);

             if(index === 0){
                opt.attr("selected", "selected");
                label.text(obj.roleName);
            }
            $ro_select.append(opt);
        });
    }

    function addMenu(){

        if(validation('add')){

            var param ={
                menuName : $('#add_menuName').val(),
                menuPath : $('#add_menuPath').val(),
                menuOrder : $('#add_order').val(),
                parentId : 0,
                depth : 1,
                menuRole : $("#add_roleName option:selected").val(),
                menuIcon : $('#add_icon').val(),
            };

            $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/menu/auth/addMenuAdmin"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('메뉴가 추가 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_highRank').hide();
                     //$("#grid").trigger("reloadGrid");
                     location.reload(true);

                 } else {
                     alert('메뉴 추가에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

        }
    }

    function showAddSubModal(menuId, depth){
        	$('.lyrWrap').fadeIn(300);
			$('.lyrBox').hide();
			$('#lyr_lowRank').show();

			$('input[id^="sub_"]').each(function(i, obj){
			    $(obj).val('');
            });

			var order = Number(depth) + 1;

			$('#parentId').val(menuId);
			$('#sub_depth').val(order);

			var roleTypes =  role_type.roleList;
            var $ro_select = $('#sub_roleName');
            var label = $('#sub_role_label');

            $ro_select.empty();

            roleTypes.forEach(function (obj,index) {
                var opt = $('<option></option>');

                opt.attr({'value' : obj.roleId});
                opt.append( obj.roleName);

                 if(index === 0){
                    opt.attr("selected", "selected");
                    label.text(obj.roleName);
                }
                $ro_select.append(opt);
            });

    }
    
    function  addsubMaenu() {

        if(validation('sub')){


            var param ={
                menuName : $('#sub_menuName').val(),
                menuPath : $('#sub_menuPath').val(),
                menuOrder : $('#sub_order').val(),
                parentId : $('#parentId').val(),
                depth :    $('#sub_depth').val(),
                menuRole : $("#sub_roleName option:selected").val(),
                menuIcon : $('#sub_icon').val(),
            };

            $.ajax({
             type:"post",
             dataType: "json",
             data : param,
             url: serverUrl("view/menu/auth/addMenuAdmin"),
             success: function(data){
                 var result = data.result;

                 if (result.code == '200') {
                     alert('메뉴가 추가 되었습니다.');

                     $('.lyrWrap').fadeOut(300);
                     $('#lyr_highRank').hide();
                  //   $("#grid").trigger("reloadGrid");
                     location.reload(true);
                 } else {
                     alert('메뉴 추가에 실패하였습니다.');
                 }
             },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

        }
    }

    function showUpdateModal(menuId){

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();

        $('#menuId').val(menuId);

        var param ={
            menuId : menuId
        };

        $('input[id^="modify_"]').each(function(i, obj){
			    $(obj).val('');
        });

         $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/menu/auth/selectMenu"),
            data : param,
            success: function(data){
                vo = data.vo;

                $('#modify_menuName').val(vo.menuName);
                $('#modify_menuPath').val(vo.menuPath);
                $('#modify_menuPath').val(vo.menuPath);
                $('#modify_icon').val(vo.menuIcon);
                $('#modify_order').val(vo.sortNum);
                $('#modify_depth').val(vo.depth);

                var roleTypes =  role_type.roleList;
                var $ro_select = $('#modify_roleName');
                var label = $('#modify_role_label');

                $ro_select.empty();

                roleTypes.forEach(function (obj,index) {
                    var opt = $('<option></option>');

                    opt.attr({'value' : obj.roleId});
                    opt.append( obj.roleName);

                     if(vo.roleId === obj.roleId){
                         console.log(vo.roleId);
                        opt.attr("selected", "selected");
                        label.text(obj.roleName);
                    }
                    $ro_select.append(opt);
                });


            },
              fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }

    function updateMenu(){

        if(validation('modify')){

             var param ={
                menuName : $('#modify_menuName').val(),
                menuPath : $('#modify_menuPath').val(),
                menuOrder : $('#modify_order').val(),
                menuId : $('#menuId').val(),
                menuRole : $("#modify_roleName option:selected").val(),
                menuIcon : $('#modify_icon').val(),
             };


             $.ajax({
                 type:"post",
                 dataType: "json",
                 data : param,
                 url: serverUrl("view/menu/auth/updateMenuAdmin"),
                 success: function(data){
                     var result = data.result;

                     if (result.code == '200') {
                         alert('메뉴가 수정 되었습니다.');

                         $('.lyrWrap').fadeOut(300);
                         $('#lyr_modify').hide();

                         //$("#grid").trigger("reloadGrid");
                         location.reload(true);
                     } else {
                         alert('메뉴 수정을 실패하였습니다.');
                     }
                 },
                fail:function(data){console.log(data)},
                error: function(data, status, error){
                    console.log(error);
                }
            });
        }
    }

     function showDeleteModal(menuId){

        $('.lyrBox').hide();
        $('.lyrWrap').fadeIn(300);
        $('#lyr_delete').show();

        $('#menuId').val(menuId);
    }

    function deleteMenu(){

         var param ={
            menuId : $('#menuId').val()
        };

         $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/menu/auth/changeStatus"),
            data : param,
            success: function(data){
                var result = data.result;

                if (result.code == '200') {
                    alert('메뉴가 삭제 되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_delete').hide();
                   // $("#grid").trigger("reloadGrid");
                    location.reload(true);
                } else {
                    alert('메뉴 삭제를 실패하였습니다.');
                }
            },
              fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

    }

    function validation(type){
        var menuName = $('#'+type+'_menuName').val();
        var menuOrder = $('#'+type+'_order').val();
        var regexp = /^[0-9]*$/

        if($.trim(menuName).length ==0){
            alert('메뉴이름을 입력해 주십시오.');
            return false;
        }


        if($.trim(menuOrder).length ==0){
            alert('메뉴 순서을 입력해 주십시오.');
            return false;
        }else if(!regexp.test(menuOrder)){
              alert('메뉴 순서는 숫자만 입력 가능합니다.');
            return false;
        }

        return true;
    }

    $(document).ready(function(){
        initPage();
        createGrid();
        getRoleType();
        customResize('grid');
    });


    function getRoleType(){
        var param={};

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/user/auth/getUserRole"),
            data : param,
            success: function(data){
                role_type = data;
            },
              fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }



    $(window).on('resize', function () {
        customResize('grid');
    }).trigger('resize');

    function initPage(){

        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });

        $('.lyr_add').on('click',function(){
            showAddMenuModal();
		});
    }



</script>
<div class="lyrWrap">
    <div class="lyr_bg"></div>
    <input type="hidden" id="parentId" name="parentId" value="" />
    <input type="hidden" id="menuId" name="menuId" value="" />
    <div id="lyr_highRank" class="lyrBox">
    	<div class="lyr_top">
        	<h3>메뉴추가</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">메뉴명</th>
                        <td>
                            <input type="text" class="ipt_txt" id="add_menuName" value="" placeholder="메뉴명을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">메뉴주소</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="add_menuPath"  placeholder="메뉴주소을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">아이콘</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="add_icon" placeholder="아이콘명 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                    <th scope="row">메뉴 순서</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="add_order" placeholder="메뉴순서를 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled" id="add_depth" value="1" disabled></td>
                    </tr>
                    <tr>
                        <th scope="row">Role</th>
                        <td>
                            <div class="selectbox">
                                <label for="add_roleName" id="add_role_label"></label>
                                <select id="add_roleName">
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="addMenu();">저장</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_lowRank" class="lyrBox">
    	<div class="lyr_top">
        	<h3>하위 추가</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">메뉴명</th>
                        <td>
                            <input type="text" class="ipt_txt" id="sub_menuName" value="" placeholder="메뉴명을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">메뉴주소</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="sub_menuPath"  placeholder="메뉴주소을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">아이콘</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="sub_icon" placeholder="아이콘명 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                    <th scope="row">메뉴 순서</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="sub_order" placeholder="메뉴순서를 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled" id="sub_depth" value="" disabled></td>
                    </tr>
                    <tr>
                        <th scope="row">Role</th>
                        <td>
                            <div class="selectbox">
                                <label for="sub_roleName" id="sub_role_label"></label>
                                <select id="sub_roleName">
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="addsubMaenu();">저장</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_modify" class="lyrBox" >
    	<div class="lyr_top">
        	<h3>수정하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                 <tbody>
                    <tr>
                        <th scope="row">메뉴명</th>
                        <td>
                            <input type="text" class="ipt_txt" id="modify_menuName" value="" placeholder="메뉴명을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">메뉴주소</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="modify_menuPath"  placeholder="메뉴주소을 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">아이콘</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="modify_icon" placeholder="아이콘명 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                    <th scope="row">메뉴 순서</th>
                        <td>
                            <input type="text" class="ipt_txt" value="" id="modify_order" placeholder="메뉴순서를 입력하세요.">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">레벨</th>
                        <td><input type="text" class="ipt_txt disabled" id="modify_depth" value="1" disabled></td>
                    </tr>
                    <tr>
                        <th scope="row">Role</th>
                        <td>
                            <div class="selectbox">
                                <label for="add_roleName" id="modify_role_label"></label>
                                <select id="modify_roleName">
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="updateMenu();">저장</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox">
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" class="btn_clr" onclick="deleteMenu();">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
</div>
<div class="titArea">
  <h3>메뉴 관리</h3>
  <div class="path">
    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
    <span>기본 관리</span>
    <span>메뉴 관리</span>
    </div>
</div>
<div class="srchArea">
    <!-- .fr -->
    <div class="fr">
        <ul class="btn_lst">
            <li><button type="button" class="lyr_add"><img src="${path}/resources/images/ico_add_bk.png" alt="추가">메뉴 추가</button></li>
        </ul>
    </div>
    <!-- //.fr -->
</div>
 <div class="content">
     <!-- .stn -->
     <div class="stn">
         <table id="grid"  class="tbl_lst"></table>
     </div>
 </div>

</body>
</html>
