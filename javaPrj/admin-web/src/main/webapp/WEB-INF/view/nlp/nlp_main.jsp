<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<c:set var="path"  value="${pageContext.request.contextPath}" />
    <script type="text/javascript" src="${path}/resources/js/common.js"></script>

</head>
<body>
<script>

    var nlpUrl = '${nlpUrl}';
    function initEvent(){

        $('#nlpStart').on('click', function(){
           testNlp();
        });
    }


    function testNlp(){

        var text = $('#nlpText').val();
        var targetUrl = nlpUrl + 'uapi/nlp/analyze';

        var param ={
            question : text
        };


        $.ajax({
            url: targetUrl,
            dataType: 'jsonp',
            data: param,
            type: 'GET',
            jsonpCallback: "callback",
            success: function(data) {
                var dataLength = data.length;
                var ol = $('#reultWords');


                if(dataLength == 0){
                    $("#noData").css("display", "");
                    $("#dataRow").css("display", "none");
                }else{
                    ol.empty();

                    $("#noData").css("display", "none");
                    $("#dataRow").css("display", "");


                    $('#textSeq').text(dataLength);

                    $.each(data, function(index, obj){
                        var li =$('<li></li>')
                        var str = obj.lemma +'. ' + obj.lemmaTypeDesc;
                        li.append(str);
                        ol.append(li);
                    });
                }
            },
            error: function(xhr) {
              console.log('실패 - ', xhr);
            }
          });

        $('#resultText').text(text);
    }

    $(document).ready(function(){
       initEvent();
    });


</script>
<div class="titArea">
    <h3>NLP 테스트</h3>
    <div class="path">
        <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
        <span>부가 기능</span>
        <span>NLP 테스트</span>
    </div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
    <div class="txtareaBox">
        <textarea class="txtArea" role="3" id="nlpText"></textarea>
        <button type="button" class="btn_textArea" id="nlpStart">실행하기</button>
    </div>
</div>
<!-- //.srchArea -->
<!-- .content -->
<div class="content">
    <!-- .stn -->
    <div class="stn">
        <table class="tbl_view">
            <colgroup>
                <col width="40%"><col width="25%"><col width="35%">
            </colgroup>
            <thead>
            <tr>
                <th scope="col">Text</th>
                <th scope="col">Text Seq</th>
                <th scope="col">Morp List</th>
               <%-- <th scope="col">Nes List</th>--%>
            </tr>
            </thead>
            <tbody>
            <tr id="dataRow">
                <td scope="row">
                    <p class="txt" id="resultText"></p>
                </td>
                <td><span id="textSeq"></span></td>
                <td >
                    <ol class="ol_lst" id="reultWords">
                    </ol>
                </td>
 <%--               <td>
                    <ol class="ol_lst">
                        <li>정부.</li>
                        <li>부동산대책.</li>
                        <li>13일.</li>
                        <li>세제.</li>
                    </ol>
                </td>--%>
            </tr>
            <tr id="noData" style="display: none">
                <td scope="row" colspan="3"><span class="dataNone">데이터가 없습니다.</span></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- //.stn -->
</div>

</body>
</html>
