  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

         <%@ include file="../common/commonHeader.jspf" %>

        <c:set var="path"  value="${pageContext.request.contextPath}" />

          <!-- icon_favicon -->
        <link rel="apple-touch-icon-precomposed" href="${path}/resources/images/ico_favicon_64x64.png">
        <link rel="shortcut icon" type="image/x-icon" href="${path}/resources/images/ico_favicon_64x64.ico"/>

        <title>maum Admin</title>
        <!-- resources -->
        
        <link rel="stylesheet" type="text/css" href="${path}/resources/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="${path}/resources/css/font.css" />
	    <link rel="stylesheet" type="text/css" href="${path}/resources/css/common.css" />
        <link rel="stylesheet" type="text/css" href="${path}/resources/css/loading.css" />
        <link rel="stylesheet" type="text/css" href="${path}/resources/css/login.css" />

      <script type="text/javascript" src="${path}/resources/js/common.js"></script>
      <script type="text/javascript" src="${path}/resources/js/sessionCheck.js"></script>
      <script type="text/javascript" src="${path}/resources/js/spin/spin.js"></script>
      <script type="text/javascript" src="${path}/resources/js/spin/deepstudy_spin.js"></script>


      </head>
      <body>

      <script type="text/javascript">

        function logout(){

            $.ajax({
                    type : "post",
                    dataType: "json",
                    url : serverUrl("login/logout"),
                    success : function (data) {
                        console.log(data);
                        if(data.resultCode == 200){
                            console.log('성공');
                            clearInterval();
                            location.href =serverUrl("login/main");
                        }else{
                            alert(data.resultMsg);
                            return false;
                        }
                    },
                    fail:function(data){console.log(data)},
                    error: function(data, status, error){
                        console.log(error);
                    }
                });


       }

       var isAudio = false;

       function validationSession(url){
           if(checkSession()){
               goPage(url);
           }else{
               clearInterval();
               location.href =serverUrl("login/main");
           }
      }

       function goPage(url){

           //평가테스트 화면에서 레코더가 남아잇는 경우가 있음
           // 레코더 객체가 존재하고 레코딩중이면 중지시킴
           if(typeof(audioRecorder) !='undefined'){
              if(recorded){
                  audioRecorder.stop();
                  recorded = false;
              }
           }

           $('#contentsArea').load(serverUrl(url), null);
      }

      //로드시 첫페이지 자동으로 갱신.
      function initPageGo(){
           var firstPage = '${firstPage}';
          goPage(firstPage);
      }


      $(document).ready(function(){
            initPageGo();
      });

</script>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
  <div class="loading_itemBox">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
  </div>
 </div>
<!-- wrap -->
    <div id="wrap">
	<!-- #header -->
     <div id="header">
        <h1><a target="_self" href="#" onclick="goPage('view/layout');"><img src="${path}/resources/images/logo_mindslab_admin.png" alt="MINDsLab Admin"></a></h1>
        <div class="hamBox">
        	<a class="btn_ham" href="#none">
            	<span>주메뉴 버튼</span>
            </a>
        </div>
		<!-- .etcmenu -->
		<div class="etcmenu">
            <div class="userBox">
            	<dl>
                	<dt class="ico_user">User</dt>
                    <dd>
                    	<a target="_self" href="#none">admin@console.com</a>
                    </dd>
                    <ul class="lst">
                       <%-- <li class="ico_profile"><a class="lyr_profile" href="#none">프로필</a></li>--%>
                        <li class="ico_logout"><a target="_self" href="#" onclick="logout();">로그아웃</a></li>
                    </ul>
                </dl>
            </div>
		</div>
		<!-- //.etcmenu -->
	</div>
	<!-- //#header -->

	<!-- #container -->
	    <div id="container">
            <!-- .snb -->
            <div class="snb" id="menuArea">
             	<h2 class="ico_pjt_name"><img src="${path}/resources/images/logo_mindslab_w.png" alt="mindslab"></h2>
                  <ul class="nav">
               <c:forEach var="item" items="${menuList}" varStatus="i">
                   <c:if test="${i.index == 0}">
                     <li class="active">
                  </c:if>
                  <c:if test="${i.index > 0}">
                     <li>
                  </c:if>
                  <c:if test="${item.childCount == 0}">
                        <a href="#" onclick="validationSession('${item.menuPath}');"><i class="${item.menuIcon}"></i><span>${item.menuName}</span></a>
                   </c:if>
                    <c:if test="${item.childCount > 0}">
                        <a href="#"><i class="${item.menuIcon}"></i><span>${item.menuName}</span></a>
                        <ul class="sub_nav">
                        <c:forEach var="subItem" items="${item.subMenuList}" varStatus="i">
                            <c:if test="${i.index == 0}">
                             <li class="active">
                          </c:if>
                          <c:if test="${i.index > 0}">
                             <li>
                          </c:if>
                            <c:if test="${subItem.childCount == 0}">
                                 <a href="#" onclick="validationSession('${subItem.menuPath}');">${subItem.menuName}</a>
                            </c:if>
                             <c:if test="${subItem.childCount > 0}">
                                  <a href="#">${subItem.menuName}</a>
                                  <ul class="third_nav">
                                <c:forEach var="thirdItem" items="${subItem.subMenuList}">
                                   <li><a href="#" onclick="validationSession('${thirdItem.menuPath}');">${thirdItem.menuName}</a></li>
                                </c:forEach>
                                  </ul>
                             </c:if>
                            </li>
                        </c:forEach>
                        </ul>
                    </c:if>
                     </li>
                </c:forEach>
                </ul>
               <%-- <ul class="nav">
                    <li class="active"><a href="#"><i class="ico_ass"></i><span>한국어 평가 관리</span></a>
                        <ul class="sub_nav">
                            <li><a class="active" href="#" onclick="goPage('koreval_test');">평가 테스트</a></li>
                            <li><a href="#" onclick="goPage('korleng_monitor');">평가 모니터링</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="ico_ass"></i><span>영어 평가 관리</span></a>
                        <ul class="sub_nav">
                            <li><a href="#" onclick="goPage('evaleng_test');">평가 테스트</a></li>
                            <li><a href="#" onclick="goPage('evaleng_monitor');">평가 모니터링</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="ico_db"></i><span>DB 관리</span></a>
                        <ul class="sub_nav">
                            <li><a href="#" onclick="goPage('koren');">한영사전</a></li>
                            <li><a href="#">국어사전</a>
                                <ul class="third_nav">
                                    <li><a href="#" onclick="goPage('kor');">국어사전</a></li>
                                    <li><a href="#" onclick="goPage('kor_synonym');">유의어사전</a></li>
                                    <li><a href="#" onclick="goPage('kor_antonym');">반의어사전</a></li>
                                </ul>
                            </li>
                            <li><a href="#" onclick="goPage('math');">수학사전</a></li>
                            <li><a href="#" onclick="goPage('endtoend');">끝말잇기</a></li>
                            <li><a href="#" onclick="goPage('game_ment');">게임멘트</a></li>
                            <li><a href="#" onclick="goPage('regex');">진입발화</a></li>
                            <li><a href="#" >영어대화</a>
                                <ul class="third_nav">
                                    <li><a href="#" onclick="goPage('eng_quest');">질문관리</a></li>
                                    <li><a href="#" onclick="goPage('eng_answer');">답변관리</a></li>
                                    <li><a href="#" onclick="goPage('eng_lslot');">슬롯관리</a></li>
                                    <li><a href="#" onclick="goPage('eng_gslot');">공통슬롯관리</a></li>
                                </ul>
                            </li>
                            <li><a href="#" onclick="goPage('game_speed');">탕수육게임</a></li>
                        </ul>
                    </li>
                    <li>
                      <a href="#none"><i class="ico_tts"></i><span>TTS 관리</span></a>
                        <ul class="sub_nav">
                            <li><a target="_self" href="#" onclick="goPage('tts');">TTS 테스트</a></li>
                        </ul>
                    </li>
                    <li><a href="#" onclick="goPage('qa_main');"><i class="ico_qna"></i><span>Q&A 관리</span></a></li>
                    <li><a href="#"><i class="ico_puzzle"></i><span>부가 기능</span></a>
                        <ul class="sub_nav">
                            <li><a href="#">NLP 테스트</a></li>
                           <!--<li><a href="#">동의어 사전</a></li> -->
                        </ul>
                    </li>
                    <li><a href="#"><i class="ico_monitoring"></i><span>대화 모니터링</span></a>
                        <ul class="sub_nav">
                            <li><a href="#" onclick="goPage('talk_log');">대화 이력 조회</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="ico_setting"></i><span>기본 관리</span></a>
                        <ul class="sub_nav">
                            <li><a href="#" onclick="goPage('user_auth');">User/권한 관리 </a></li>
                            <li><a href="#" onclick="goPage('detail_code');">코드 관리</a></li>
                            <li><a href="#" onclick="goPage('group_code');">코드 그룹 관리</a></li>
                            <li><a href="#">메뉴 관리</a></li>
                        </ul>
                    </li>
                </ul>--%>
            </div>
            <!-- //.snb -->
            <!-- .contents -->
          <div class="contents" id="contentsArea"></div>
		<!-- //.contents -->
	</div>
	    <!-- //#container -->
	    <!-- #footer -->
	    <div id="footer">
		<div class="lot_c">
			<div class="cyrt"><span>MINDsLab &copy; 2018</span></div>
		</div>
	</div>
	<!-- //#footer -->
   </div>
</body>
</html>