<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script src="${path}/resources/js/security/rsa/jsbn.js"></script>
	<script src="${path}/resources/js/security/rsa/rsa.js"></script>
	<script src="${path}/resources/js/security/rsa/prng4.js"></script>
	<script src="${path}/resources/js/security/rsa/rng.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>

</head>
<body>
<script>


    function initEvent(){

        $('#searchBtn').on('click', function(){
            searchCode();
        });

        $('#searchText').on('keyup', function (e){
            if(e.keyCode === 13){
                searchCode();
            }
        });

        $('.lyr_delete').on('click',function() {
            //선택된 대상이 있는지 확인.
            var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');

            if(ids =='undefined' || ids.length ==0) {
                alert('삭제할 대상을 선택하십시오.');
            }else{
                $('.lyrWrap').fadeIn(300);
                $('#lyr_add').hide();
                $('#lyr_modify').hide();
                $('#lyr_delete').show();
            }
        });


        $('.lyr_add').on('click',function(){
            $('.lyrWrap').fadeIn(300);
            $('#lyr_add').show();
            $('#lyr_modify').hide();
            $('#lyr_delete').hide();

            var codeType =  getGroupCode();
            var $ro_select = $("#add_groupCodeSelect");

            $ro_select.empty();

            codeType.forEach(function (obj, index) {
                var opt = $('<option></option>');
                console.log(obj);
                opt.attr({'value' : obj.groupCode});
                opt.append( obj.codeName);
                console.log(index);
                if(index == 0){
                    opt.attr("selected", 'selected');
                }

                $ro_select.append(opt);
            });
        });

        $('#add_detailCode').on('keyup', function(e){
           var groupCode = $("#add_groupCodeSelect option:selected").val();
           var code = this.value;
           var fullCode = groupCode + code;

           $('#add_fullCode').val(fullCode);
        });
    }

    function createGrid(){
        $("#grid").jqGrid({
            url : serverUrl('view/code/detail/codeList'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : '코드', name : 'fullCode', width : '10%', align : 'center', formatter : formatter_names},
                {label : '상세코드', name : 'detailCode', width : '10%', align : 'center'},
                {label : '그룹코드', name : 'groupCode', width : '10%', align : 'center'},
                {label : '이름', name : 'detailName', width : '20%', align : 'center'},
                {label : '설명', name : 'descript', width : '20%', align : 'center'},
                {label : '사용여부', name : 'useYn', width : '10%', align : 'center', formatter : formatter_useYn},
                {label : '생성일', name : 'createdTime', width : '20%', align : 'center'}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            multiselect: true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'fullCode',
            sortorder: 'asc',
            loadComplete : function(data) {
                customResize('grid');
            }
        });
    }

    function formatter_names(cellvalue, options, rowObject){
        var html = "<a style=\"text-decoration: underline;color: #6BA8D1;\" href='#' onclick=\"showlayerPopUp('update', '"+rowObject.fullCode + "')\">" + rowObject.fullCode     +"</a>";
        return html;
    }


    function formatter_useYn(cellvalue, options, rowObject){
        var useYnStr ='';
        var uesYn = cellvalue;

        if(uesYn.toLowerCase() ==='y' || uesYn.toLowerCase() ==='yes'){
            useYnStr ='사용';
        }else{
            useYnStr ='미사용';
        }

        return useYnStr;
    }


    function checkClickEvent(){
        var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');      //체크된 row id들을 배열로 반환
        var keys ={};
        var keyArray =[];
        var rowObject = null;
        for(var i = 0; i < ids.length; i++){
            rowObject = $("#grid").getRowData(ids[i]);      //체크된 id의 row 데이터 정보를 Object 형태로 반환
            var keys ={
                fullCode : $(rowObject.fullCode).text()
            };

            console.log(keys);

            keyArray.push(keys);
        }

        var param ={
            unique : JSON.stringify(keyArray)
        };

        return param;
    }


    function searchCode(){

        var param ={
            searchText:  $("#searchText").val()
        };
        reloadGrid(param);
    }

    function reloadGrid(param){
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }


    function showlayerPopUp(type, fullCode){

        if(type ==='update'){
            makeUpdateLayer(fullCode);
        }else{

            $('.lyrWrap').fadeIn(300);
            $('#lyr_modify').hide();
            $('#lyr_add').show();
            $('#lyr_delete').hide();
        }
    }


    function makeUpdateLayer(fullCode){


        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();
        $('#lyr_add').hide();
        $('#lyr_delete').hide();

        var param ={
            searchText : fullCode
        };

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/code/detail/getCodeDetailAdmin"),
            data : param,
            success: function(data){
                $( '#data_tbody').empty();

                var result = data['data'];
                var $table = $("#data_tbody");

                var $tr = null;
                var $td = null;
                var $th = null;

                $.each(result, function(key, value){
                    $tr = $('<tr/>');
                    $td = $('<td/>');
                    $th = $('<th/>');

                    $th.attr('scope', 'row');

                    if(key ==='fullCode'){
                        $th.text('코드');
                    }else if(key ==='groupCode'){
                        $th.text('그룹코드');
                    }else if(key ==='detailCode'){
                        $th.text('상세코드');
                    }else if(key ==='detailName'){
                        $th.text('이름');
                    }else if(key ==='descript') {
                        $th.text('설명');
                    }else if(key ==='useYn') {
                        $th.text('사용여부');
                    }else if(key ==='createdTime') {
                        $th.text('생성일');
                    }


                    if(value == null || value == 'undefined'){
                        value ='';
                    }

                    if(key =='fullCode' || key =='groupCode' || key =='detailCode' || key =='createdTime'){
                        $td.html('<span id="'+key +'">'+ value +'</span>');
                    }else if(key ==='useYn'){
                        var $select = $('<select id="'+ key +'"/>');
                        var yOpt = $('<option></option>');
                        var NOpt = $('<option></option>');

                        yOpt.attr({'value' : 'Y'});
                        yOpt.append('사용');
                        NOpt.attr({'value' : 'N'});
                        NOpt.append('미사용');

                        if(value.toUpperCase() ==='Y'){
                            yOpt.prop("selected", "selected");
                        }else{
                            NOpt.prop("selected", "selected");
                        }

                        $select.append(yOpt);
                        $select.append(NOpt);
                        $td.append($select);

                    }else{
                        $td.html('<input type="text"  id="'+ key +'" class="ipt_txt" value="'+value+'">');
                    }

                    $tr.append($th);
                    $tr.append($td);
                    $table.append($tr);
                });
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }


    function updateCode(type){


        var fullCode = $("#fullCode").text();
        var groupCode = $("#groupCode").text();
        var detailName = $("#detailName").val();
        var descript = $("#descript").val();
        var useYn =  $("#useYn option:selected").val();


        var param = {
            fullCode : fullCode,
            groupCode: groupCode,
            detailName :detailName,
            descript : descript,
            groupCode : groupCode,
            useYn : useYn
        }

        $.ajax({

            type:"post",
            dataType: "json",
            url: serverUrl("view/code/detail/updateCodeDetail"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('업데이트 되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_modify').hide();
                    $("#grid").trigger("reloadGrid");
                    $( '#data_tbody').empty();

                }else{
                    alert('업데이트에 실패하였습니다.');
                }
            },
            fail:function(data){
                alert('update fail');
                console.log(data)
            },
            error: function(data, status, error){
                alert('update error');
                console.log(error);
            }
        });
    }


    function addCodeDetail(){

        var fullCode = $("#add_fullCode").val();
        var detailCode = $("#add_detailCode").val();
        var groupCode = $("#add_groupCodeSelect option:selected").val();
        var detailName = $("#add_detailName").val();
        var descript = $("#add_descript").val();
        var useYn =  $("#add_useYn option:selected").val();

        var testCode = groupCode +detailCode;

        if($.trim(fullCode).length == 0){
            alert('코드를 입력하십시오.');
            return false;
        }else if($.trim(detailCode).length == 0){
            alert('상세코드를 입력해주십시오.');
            return false;
        }else if(fullCode != testCode){
            alert('코드입력을 다시 확인해주십시오.');
            return false;
        }

        var param = {
            fullCode : fullCode,
            groupCode: groupCode,
            detailCode: detailCode,
            detailName :detailName,
            descript : descript,
            groupCode : groupCode,
            useYn : useYn
        }

        $.ajax({

            type:"post",
            dataType: "json",
            url: serverUrl("view/code/detail/addCodeDetail"),
            data : param,
            success: function(data){
                var result = data.result;
                if(result.code =='200'){
                    alert('코드가 추가되었습니다.');

                    initaddgropCodeArea();
                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_add').hide();
                    $("#grid").trigger("reloadGrid");

                }else if(result.code =='300'){
                    alert(result.msg);
                }else{
                    alert('코드 추가에 실패하였습니다.');
                }
            },
            fail:function(data){
                alert('update fail');
                console.log(data)
            },
            error: function(data, status, error){
                alert('update error');
                console.log(error);
            }
        });
    }

    function initaddgropCodeArea(){

        $("#add_fullCode").val('');
        $("#add_detailCode").val('');
        $("#add_descript").val('');
        $("#add_detailName").val('');
        $('#add_useYnYn option:eq(0)').prop('selected', true);
        $('#add_groupCodeSelect option:eq(0)').prop('selected', true);
    }

    function cancelUpate(type){

        if(type === 'add'){
            initaddgropCodeArea();
            $('.lyrWrap').fadeOut(300);
            $('#lyr_add').hide();
        }else{
            $('.lyrWrap').fadeOut(300);
            $('#lyr_modify').hide();
            $( '#data_tbody').empty();
        }
    }


    function getGroupCode(){
        var codeList  = null;

        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/code/detail/getGroupCode"),
            async : false,
            success: function(data){
                codeList = data['codeList'];
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });

        return codeList;
    }


    function deleteDetailCode(){

        var param = checkClickEvent();

        $.ajax({
            type: "post",
            dataType: "json",
            url: serverUrl("view/code/detail/deleteCodeDetail"),
            data: param,
            success: function (data) {
                var result = data.result;
                if (result.code == '200') {
                    alert('삭제되었습니다.');

                    $('.lyrWrap').fadeOut(300);
                    $('#lyr_delete').hide();
                    $("#grid").trigger("reloadGrid");
                } else {
                    alert('삭제가 실패하였습니다.');
                }
            },
            fail: function (data) {
                console.log(data)
            },
            error: function (data, status, error) {
                console.log(error);
            }
        });
    }


    $(document).ready(function(){
        initPage();
        initEvent();
        createGrid();
        customResize('grid');
    });

    $(window).on('resize', function () {
        customResize('grid');
    }).trigger('resize');



   function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }

</script>
<div id="page_ldWrap" class="page_loading">
      <div class="loading_itemBox">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
      </div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
	<div class="lyr_bg"></div>
    <div id="lyr_add" class="lyrBox">
    	<div class="lyr_top">
        	<h3>추가하기</h3>
            <button type="button"  onclick="cancelUpate('add');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">코드</th>
                        <td><input type="text" class="ipt_txt" id="add_fullCode" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">상세코드</th>
                        <td><input type="text" class="ipt_txt" id="add_detailCode" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">그룹코드</th>
                        <td>
                            <select id="add_groupCodeSelect">
                            </select>
                        </td>
                    </tr>
                     <tr>
                        <th scope="row">이름</th>
                        <td><input type="text" class="ipt_txt" id="add_detailName" value=""></td>
                    </tr>
                    <tr>
                        <th scope="row">설명</th>
                        <td><input type="text" class="ipt_txt" id="add_descript" value=""></td>
                    </tr>

                    <tr>
                        <th scope="row">사용여부</th>
                        <td>
                            <select id="add_useYn">
                                <option value="y">사용</option>
                                 <option value="n">미사용</option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="addCodeDetail();" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpate('add');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_modify" class="lyrBox" >
    	<div class="lyr_top">
        	<h3>수정하기</h3>
            <button type="button" onclick="cancelUpate('update');" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col width="20%"><col>
                </colgroup>
                <tbody id="data_tbody"></tbody>
            </table>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">

            	<li><button type="button" onclick="updateCode();" class="btn_clr">저장</button></li>
                <li><button type="button" onclick="cancelUpate('update');" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
	<div id="lyr_delete" class="lyrBox02">
		<input type="hidden" id="del_target" value=""/>
    	<div class="lyr_top">
        	<h3>삭제하기</h3>
            <button type="button" class="btn_lyr_close">닫기</button>
        </div>
    	<div class="lyr_mid">
            <div class="txtBox">
            	<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
            	<p class="txt">삭제 하시겠습니까?</p>
            </div>
        </div>
        <div class="lyr_btm">
        	<ul class="btn_lst">
            	<li><button type="button" onclick="deleteDetailCode();" class="btn_clr">확인</button></li>
                <li><button type="button" class="btn_lyr_cancel">취소</button></li>
            </ul>
        </div>
    </div>
</div>
<!-- //.lyrWrap modal 창 -->

<!-- .titArea -->
<div class="titArea">
 <h3>코드관리</h3>
 <div class="path">
    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
	 <span>기본관리</span>
	 <span>코드관리</span>
 </div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
	<!-- .fl -->
	<div class="fl">
		<%--<div class="selectbox">
			<label for="ex_select">선택</label>
			<select id="ex_select">
                <option value="">선택</option>
                <option value="word">단어</option>
			</select>
		</div>--%>
		<div class="srchbox">
			<input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
			<button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
		</div>
	</div>
	<!-- //.fl -->
	<!-- .fr -->
	<div class="fr">
		<ul class="btn_lst">
			<li><button  type="button" class="btn_clr lyr_add"><img src="${path}/resources/images/ico_add_bk.png" alt="삭제">추가</button></li>
            <li><button  type="button" class="btn_clr lyr_delete"><img src="${path}/resources/images/ico_delete_bk.png" alt="삭제">삭제</button></li>
		</ul>
	</div>
	<!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<!-- .stn -->
<div class="content">
  <div class="stn">
	<table id="grid"  class="tbl_lst"></table>
	<div id="pager"></div>
  </div> 	<!-- //.stn -->
</div>
</body>
</html>