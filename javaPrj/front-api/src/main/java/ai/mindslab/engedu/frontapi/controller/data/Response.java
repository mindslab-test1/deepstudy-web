package ai.mindslab.engedu.frontapi.controller.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
	
	private String resCode;
	private String resMsg;
	private String resultType;
	private String resultId;
	private Result result;

}
