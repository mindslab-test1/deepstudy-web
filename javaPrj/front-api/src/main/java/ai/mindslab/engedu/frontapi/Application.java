package ai.mindslab.engedu.frontapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@ComponentScan(basePackages={"ai.mindslab.engedu"})
@EnableAutoConfiguration(exclude = {SolrAutoConfiguration.class})
@EnableScheduling
public class Application extends SpringBootServletInitializer {
	
	private static Logger logger  = LoggerFactory.getLogger(Application.class);

	//war를 처리하기위해 필요하다.
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
	
	public static void main(String[] args) {
		
		logger.info("front-api server started.");
		
		SpringApplication.run(Application.class, args);
	}
}
