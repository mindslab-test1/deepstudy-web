package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.DayIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 요일 sample rest api
 */
@RestController
@RequestMapping("/day")
public class DaySampleController {

    @Autowired
    private DayIntentExecute dayIntentExecute;

    /***
     * @param paramInputStr 사용자 발화 문장  ex) 오늘 몇일이야? || 내일 몇일이야?
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = dayIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }
}
