package ai.mindslab.engedu.frontapi.controller.data;

import lombok.Data;

@Data
public class SttFileResponseVO {

    /** 접속한 회원 아이디 */
    private String userId;

    /** 발화 */
    private String utter;

    /** 학습자 음성 녹음 url */
    private String recordUrl;

    /** 정답 문장 */
    private String answer;

    /** 파일 경로 */
    private String filePath;

    /** text 파일명 */
    private String fileTextName;

    /** wav 파일명 */
    private String fileWavName;

    /** mp3 파일명 */
    private String fileMp3Name;

    /** 폴더 + 파일 wav 파일명 */
    private String fileFolderFileWavName;

    /** 폴더 + 파일 pcm 파일명 */
    private String fileFolderFilePcmName;

}
