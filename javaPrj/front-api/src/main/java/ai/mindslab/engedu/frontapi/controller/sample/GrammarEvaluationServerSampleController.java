package ai.mindslab.engedu.frontapi.controller.sample;


import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.eev.Evaluation;
import maum.brain.eev.EvaluationServiceGrpc;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/grammarEvaluationServer")
public class GrammarEvaluationServerSampleController {

    private final static String SERVER_IP = "10.122.64.57";
    private final static int SERVER_PORT = 9992;

    private ManagedChannel channel;
    private EvaluationServiceGrpc.EvaluationServiceBlockingStub blockingStub;


    @RequestMapping("/sample")
    public Response sample(
            @RequestParam(name="inputStr",defaultValue = "i love you") String inputStr,
            @RequestParam(name="answerStr",defaultValue = "i like you") String answerStr)
    {

        this.channel = ManagedChannelBuilder.forAddress(SERVER_IP, SERVER_PORT).usePlaintext().build();
        this.blockingStub = EvaluationServiceGrpc.newBlockingStub(channel);

        int cnt = answerStr.split(IMarkCodes.ANSWER_SPLIT_TEXT).length;
        int distribution = 100 / cnt;

        StringBuilder tempResult = new StringBuilder();

        int remainder = 100 % cnt;

        for (int i = 0; i < cnt; i++) {

            int tempNumber = remainder <= 0 ? 0 : 1;

            tempResult.append(distribution + tempNumber).append(IMarkCodes.ANSWER_SPLIT_TEXT);

            remainder--;
        }


        inputStr = inputStr.replaceAll(IMarkCodes.BACKSLASH + IMarkCodes.COMMA,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.EXCLAMATION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.QUESTION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.PERIOD,"");

        answerStr = answerStr.replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.COMMA,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.EXCLAMATION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.QUESTION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.PERIOD,"");

        Evaluation.TextInfo textInfo = Evaluation.TextInfo.newBuilder()
                .setType(Evaluation.EvaluationType.SENTENCE)
                .setExpected(answerStr)
                .setUtter(inputStr)
                .setScore(tempResult.toString())
                .setKeywordPos(5)
                .build();

        Evaluation.Analysis analysis =  this.blockingStub.textAnalyze(textInfo);

        ArrayList<GrammarEvaluationDetailVO> scoreDetail = new ArrayList<>();

        analysis.getWordScoreList().stream().forEach(
                p-> scoreDetail.add(new GrammarEvaluationDetailVO(p.getWord(),String.valueOf(p.getScore())))
        );

        Response response = new Response();

        response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
        response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
        Result result = new Result();
        result.setUserText(inputStr);
        result.setAnswerText(answerStr);
        result.setGrammarScore(String.valueOf(analysis.getScore()));
        result.setGrammarScoreDetail(scoreDetail);
        response.setResult(result);

        this.channel.shutdown();

        return  response;

    }

}
