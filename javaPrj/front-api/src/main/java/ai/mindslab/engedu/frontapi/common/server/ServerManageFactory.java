package ai.mindslab.engedu.frontapi.common.server;


import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IChoiceCodes;
import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.PronounceEvaluationClient;
import ai.mindslab.engedu.frontapi.client.TtsMindsLabClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ServerManageFactory {

    private List<ServerVO> pronounceServerList;

    private List<ServerVO> ttsServerList;

    private List<ServerVO> phonicsServerList;

    private List<ServerVO> selvasServerList;

    private String ttsRecDir;

    private String ttsServerCheckDir;

    private String domain;

    @Value("${brain.evaluation.eng.stt.rec.dir}")
    private String evaluationDir;

    @Value("${engedu.pronounce.evaluation.standard}")
    private String pronounceStandard;

    @Value("${server.check.delay.time}")
    private int checkServerDelayTime;

    @Autowired
    EnvironmentBase environmentBase;

    @Autowired
    private UserService userService;

    @Autowired
    public ServerManageFactory(
            @Value("#{'${engedu.pronounce.evaluation.server.ip.list}'.split(',')}") List<String> pronounceServerIpList,
            @Value("#{'${engedu.pronounce.evaluation.server.port.list}'.split(',')}")  List<Integer> pronounceServerPortList,
            @Value("#{'${engedu.tts.server.ip.list}'.split(',')}") List<String> ttsServerIpList,
            @Value("#{'${engedu.tts.server.port.list}'.split(',')}")  List<Integer> ttsServerPortList,
            @Value("${brain.dialog.tts.rec.dir}") String ttsRecDir,
            @Value("${brain.tts.server.check.rec.dir}") String ttsServerCheckDir,
            @Value("${client.record.domain}") String domain,
            @Value("#{'${phonics.ip}'.split(',')}") List<String> phonicsServerIpList,
            @Value("#{'${phonics.ip.port}'.split(',')}") List<Integer> phonicsServerPortList,
            @Value("#{'${selvas.tts.ip}'.split(',')}") List<String> selvasServerList,
            @Value("#{'${selvas.tts.port}'.split(',')}") List<Integer> selvasServerPortList)
    {

        this.ttsRecDir = ttsRecDir;
        this.ttsServerCheckDir = ttsServerCheckDir;
        this.domain = domain;

        setPronounceServerList(pronounceServerIpList,pronounceServerPortList);
        setTtsServerList(ttsServerIpList,ttsServerPortList);
        setPhonicsServerList(phonicsServerIpList, phonicsServerPortList);
        setSelvasServerList(selvasServerList, selvasServerPortList);

    }
    //default 30000
    @Scheduled(fixedDelay = 3000000)
    private void serverCheckSchedule(){

        checkPronounceServerList();
        checkTtsServerList();

    }

    /**
     * properties 의 TTS 서버 List 정보 set
     * @param paramTtsServerIpList TTS Server Ip List
     * @param paramTtsServerPortList TTS Server Port List
     */
    private void setTtsServerList(List<String> paramTtsServerIpList, List<Integer> paramTtsServerPortList)
    {

        ttsServerList = new ArrayList<>();

        for(int i=0; i<paramTtsServerIpList.size(); i++){

            String serverIp = paramTtsServerIpList.get(i);
            int serverPort = paramTtsServerPortList.get(i);

            ServerVO vo = new ServerVO();
            vo.setServerIp(serverIp);
            vo.setServerPort(serverPort);
            vo.setServerEnable(IChoiceCodes.RESULT_N);
            vo.setTtsRecDir(ttsRecDir);
            vo.setTtsDevDir(ttsServerCheckDir);
            vo.setDomain(domain);
            ttsServerList.add(vo);

        }

    }

    private void setSelvasServerList(List<String> paramSelvasServerIpList, List<Integer> paramSelvasServerPortList) {
        selvasServerList = new ArrayList<>();

        for(int i=0; i<paramSelvasServerIpList.size(); i++) {
            String serverIp = paramSelvasServerIpList.get(i);
            int serverPort = paramSelvasServerPortList.get(i);

            ServerVO vo = new ServerVO();
            vo.setServerIp(serverIp);
            vo.setServerPort(serverPort);
            vo.setServerEnable(IChoiceCodes.RESULT_Y);
            vo.setTtsDevDir(ttsRecDir);
            vo.setTtsDevDir(ttsServerCheckDir);
            vo.setDomain(domain);
            selvasServerList.add(vo);
        }
    }

    /*
    private void setSttServerList(
            List<String> paramSttServerIpList,
            List<Integer> paramSttServerPortList,
            List<Integer> paramSttServerSampleRate,
            List<String> paramSttServerModel)
    {

        sttServerList = new ArrayList<>();
        for(int i=0; i<paramSttServerIpList.size(); i++){

            String serverIp = paramSttServerIpList.get(i);
            int serverPort = paramSttServerPortList.get(i);
            int serverSampleRate = paramSttServerSampleRate.get(i);
            String serverModel = paramSttServerModel.get(i);
            ServerVO vo = new ServerVO();
            vo.setServerIp(serverIp);
            vo.setServerPort(serverPort);
            vo.setSttSampleRate(serverSampleRate);
            vo.setSttModel(serverModel);
            vo.setServerEnable(IChoiceCodes.RESULT_N);
            sttServerList.add(vo);

        }

    }
    */

    /**
     *  properties 의 발음평가 서버 List 정보 set
     * @param paramPronounceServerIpList 발음 평가 Server Ip List
     * @param paramPronounceServerPortList 발음 평가 Server Port List
     */
    private void setPronounceServerList(List<String> paramPronounceServerIpList, List<Integer> paramPronounceServerPortList)
    {

        pronounceServerList = new ArrayList<>();

        for(int i=0; i<paramPronounceServerIpList.size(); i++){

            String serverIp = paramPronounceServerIpList.get(i);
            int serverPort = paramPronounceServerPortList.get(i);
            ServerVO vo = new ServerVO();
            vo.setServerIp(serverIp);
            vo.setServerPort(serverPort);
            vo.setServerEnable(IChoiceCodes.RESULT_N);
            pronounceServerList.add(vo);

        }

    }

    private void setPhonicsServerList(List<String> paramPhonicsServerIpList, List<Integer> paramPhonicsServerPortList)
    {

        phonicsServerList = new ArrayList<>();

        for(int i=0; i<paramPhonicsServerIpList.size(); i++){

            String serverIp = paramPhonicsServerIpList.get(i);
            int serverPort = paramPhonicsServerPortList.get(i);
            ServerVO vo = new ServerVO();
            vo.setServerIp(serverIp);
            vo.setServerPort(serverPort);
            vo.setServerEnable(IChoiceCodes.RESULT_N);
            phonicsServerList.add(vo);

        }

    }

    /**
     * 발음 서버 List 에서 활성화 된 값중 사용안된 서버를 랜덤으로 한개 가져온다.
     * @return ServerVO
     */
    public ServerVO getPronounceServerInfo(){

        List<ServerVO> result = pronounceServerList.stream()
                .filter(s->s.getServerEnable().equals(IChoiceCodes.RESULT_Y))
                .collect(Collectors.toList());

        ServerVO serverVo = getServerInfo(result);

        return serverVo;

    }

    /**
     * TTS 서버 List 에서 활성화 된 값중 사용안된 서버를 랜덤으로 한개 가져온다.
     * @return ServerVO
     */
    public ServerVO getTtsServerInfo(){

        List<ServerVO> result = ttsServerList.stream()
                .filter(s->s.getServerEnable().equals(IChoiceCodes.RESULT_Y))
                .collect(Collectors.toList());

        ServerVO serverVo = getServerInfo(result);

        return serverVo;
    }

    public ServerVO getPhonicsServerInfo(){

        List<ServerVO> result = phonicsServerList.stream()
                .filter(s->s.getServerEnable().equals(IChoiceCodes.RESULT_Y))
                .collect(Collectors.toList());

        ServerVO serverVo = getServerInfo(result);

        return serverVo;
    }

    public ServerVO getSelvasServerInfo() {
        List<ServerVO> result = selvasServerList.stream()
                .filter(s->s.getServerEnable().equals(IChoiceCodes.RESULT_Y))
                .collect(Collectors.toList());

        ServerVO serverVo = getServerInfo(result);

        return serverVo;
    }

    /**
     * random 값 가져온다. 0 ~ endNum
     * @param endNum List Size = endNum
     * @return 0~N
     */
    private int randomRange(int endNum) {

        int result = (int) (Math.random() * (endNum - 0 + 1)) + 0;

        return result;

    }

    /**
     * 서버 정보를 랜덤으로 가져오고, 해당 서버 정보 사용여부를 Y로 변경한다.
     * @param paramResult 활성화 된 서버중 사용 안된 List
     * @return ServerVO
     */
    private ServerVO getServerInfo(List<ServerVO> paramResult){

        if(paramResult.size() != 0){

            int ranNum = randomRange(paramResult.size()-1);

            return paramResult.get(ranNum);

        }else{

            return null;

        }

    }

    /**
     * TTS 서버 상태 CHECK
     */
    private void checkTtsServerList(){

        try{

            for (ServerVO serverVO : ttsServerList) {

                String fileName = "test.mp3";

                ITtsClient ttsClient = new TtsSelvasClient(
                        serverVO.ttsDevDir,
                        ILanguageCodes.KOR,
                        "안녕",
                        "",
                        serverVO.ttsRecDir,
                        fileName,
                        environmentBase.getActiveProfile(),
                        serverVO.getDomain()
                );

                ttsClient.setServerInfo(serverVO.getServerIp(), serverVO.getServerPort());

                Response ttsResponse = ttsClient.getTtsResponse();

                // tts error 시 처리 X

                if (ttsResponse == null) {

                    serverVO.setServerEnable(IChoiceCodes.RESULT_N);

                } else if (ttsResponse.getResCode() == null || ttsResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR))) {

                    serverVO.setServerEnable(IChoiceCodes.RESULT_N);

                } else {

                    log.info("TTS Server Check ===" + new DateUtil().getCurrentTime(DateUtil.TO_SECOND));

                    // 응답값이 정상이기 때문에 서버 사용 여부를 Y로 변경
                    serverVO.setServerEnable(IChoiceCodes.RESULT_Y);
                    ttsClient.shutdown();

                }
            }

            log.info("TTS Server Check ===" + new DateUtil().getCurrentTime(DateUtil.TO_SECOND));

            for (ServerVO ttsServer : ttsServerList) {
                log.info("TTS Sever Info  ===" + ttsServer.toString());
            }

        }catch(Exception e){

           // e.printStackTrace();
            log.error("getProcessTtsServerList Error : {}" + e.getMessage());

            String errText = ExceptionUtils.getStackTrace(e);
            Response response = new Response();
            response.setResultType("SV");
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_TTS_SERVER_MANAGER_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_TTS_SERVER_MANAGER_ERROR);

           // ResponseLogResolver.insertResponseLog(response, "server", errText);


        }

    }

    /**
     * 발음 서버 상태 CHECK
     */
    private void checkPronounceServerList(){

        //EngEvalFileExtVO engEvalFileExtVO = userService.getUserEvaluation(evaluationDir);
        //EngEvalFileExtVO engEvalFileExtVO = null;

        try{

            String inputStr;
            String answerStr;
            String fileWav;



            inputStr = "yes i do";
            answerStr = "yes i do";
            fileWav = "/record/checkServer/test.pcm";


            for (ServerVO pronounceServer : pronounceServerList) {

                PronounceEvaluationClient pronounceEvaluationClient = new PronounceEvaluationClient(
                        pronounceServer.getServerIp(),
                        pronounceServer.getServerPort(),
                        environmentBase.getActiveProfile()
                );

                Response pronounceResponse;
                pronounceResponse = pronounceEvaluationClient.getPronounceEvaluationInfo(
                        inputStr,
                        answerStr,
                        fileWav,
                        pronounceStandard
                );

                if (pronounceResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR))) {

                    pronounceServer.setServerEnable(IChoiceCodes.RESULT_N);

                } else {

                    pronounceServer.setServerEnable(IChoiceCodes.RESULT_Y);

                }

                pronounceEvaluationClient.shutdown();

            }

        }catch(Exception e){

            //e.printStackTrace();
            log.error("getProcessPronounceServerList Error: {}" + e.getMessage());
        }

        log.debug("Pronounce Server Check ===" + new DateUtil().getCurrentTime(DateUtil.TO_SECOND));

        for (ServerVO pronounceServer : pronounceServerList) {
            log.debug("Pronounce Server Info  ===" + pronounceServer.toString());
        }

    }

}
