package ai.mindslab.engedu.frontapi.controller.sample.dialog;

import ai.mindslab.engedu.intent.DateIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 날짜 sample rest api
 */
@RestController
@RequestMapping("/date")
public class DateSampleController {

    @Autowired
    private DateIntentExecute dateIntentExecute;

    /***
     * @param paramInputStr 사용자 발화 문장  ex) 지금 몇시야?
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = dateIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }
}
