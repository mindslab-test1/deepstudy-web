package ai.mindslab.engedu.frontapi.controller.rest;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.TtsMindsLabClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.server.ServerManageFactory;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/tts")
public class SimpleTtsController {


    @Value("${brain.simple.tts.rec.dir}")
    private String filePath;

    @Value("${brain.tts.dev.rec.dir}")
    private String ttsDevDir;

    @Value("${client.record.domain}")
    private String domain;

    @Autowired
    private EnvironmentBase environmentBase;

    @Autowired
    private ServerManageFactory serverManageFactory;


    @RequestMapping("/simpleTts.json")
    public Response simpleTts(
            @RequestParam(name = "v", defaultValue = "1") int v,
            @RequestParam(name="biz",defaultValue = "biz partner") String biz,
            @RequestParam(name="channel",defaultValue = "contact channel") String channel,
            @RequestParam(name="language",defaultValue = "KOR") String language,
            @RequestParam(name="userId",defaultValue = "test") String userId,
            @RequestParam(name="script",defaultValue = "테스트 입니다.") String script
            )
    {

        Response response = null;

        ITtsClient ttsClient = null;

        String errText = null;

        try{

            FileCreateUtil fileCreateUtil = new FileCreateUtil();

            // 현재 시각 년월일시분초
            String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
            String randomNum = fileCreateUtil.getRandomNum();

            String fileWavName = fileCreateUtil.getFileName(userId,currentTime,randomNum,IExtensionCodes.EXT_WAV);

            ttsClient = new TtsSelvasClient(
                    this.ttsDevDir,
                    language,
                    script,
                    "",
                    filePath,
                    fileWavName,
                    environmentBase.getActiveProfile(),
                    domain
            );

            ttsClient = setTtsServerInfo(ttsClient,language);

            response = ttsClient.getTtsResponse();

            response.setResultType(IRestCodes.RESULT_TYPE_SIMPLE_TTS);

            //ResponseLogResolver.insertResponseLog(response);



            if(response.getResult() == null || response.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR)) ) {   // tts error
                log.error("simpleTts.json ttsError : {} ", response);
                int errorCode = IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR;
                String errorMsg = IRestCodes.ERR_MSG_TTS_EVALUATION_ERROR;
                response = getClientErrorReturnMessage(script,errorCode,errorMsg);
            } 

            ttsClient.shutdown();

        } catch(Exception e){

            try{
            	response = getClientErrorReturnMessage(script,IRestCodes.ERR_CODE_FAILURE,IRestCodes.ERR_MSG_FAILURE);
            	errText = ExceptionUtils.getStackTrace(e);
                if(ttsClient != null){
                    ttsClient.shutdown();
                }
            }catch (Exception e1){
                log.error("simpleTts.json ttsError : {} ", e1);
                e.printStackTrace();
            }

        } finally {
        	ResponseLogResolver.insertResponseLog(response, userId, errText);
		}

        return response;

    }

    private ITtsClient setTtsServerInfo(ITtsClient paramTtsClient, String paramLanguage)
    {

        ITtsClient result = paramTtsClient;
        ServerVO serverVO =  serverManageFactory.getSelvasServerInfo();

        if(serverVO != null){

            log.info("setSelvasServerInfo serverVO : "+ serverVO);
            result.setServerInfo(serverVO.getServerIp(),serverVO.getServerPort());

        }else{

            log.info("setSelvasServerInfo serverVO null");
            result = new TtsSelvasClient();

        }

        return result;

    }


    private Response getClientErrorReturnMessage(String paramUtter,int paramErrorCode,String paramErrorMsg) throws  Exception{

        Result result = new Result();
        Response response = new Response();

        response.setResCode(Integer.toString(paramErrorCode));
        response.setResMsg(paramErrorMsg);
        response.setResultType(IRestCodes.RESULT_TYPE_SIMPLE_TTS);
        result.setUserText(paramUtter);
        response.setResult(result);
        
        return response;

       /* ResponseLogResolver.insertResponseLog(response);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(response);*/

    }

}
