package ai.mindslab.engedu.frontapi.config;

import java.nio.charset.Charset;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
public class AppConfig {

	public static final String DATASOURCE_MAIN = "dataSource";
	public static final String TRANSACTION_MANAGER_MAIN = "transactionManager";
	public static final String TRANSACTION_SESSION_FACTORY_MAIN = "sqlSessionFactory";
	
	@Value("${engedu.restapi.timeout}")
	private int TIME_OUT;
	
	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		encodingFilter.setForceEncoding(true);
		
		return encodingFilter;
		
	}
	
	@Bean
    public HttpMessageConverter<String> responseBodyConverter() {
        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
    }
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		
		return restTemplateBuilder.setConnectTimeout(TIME_OUT).setReadTimeout(TIME_OUT).build();
	}
	
}
