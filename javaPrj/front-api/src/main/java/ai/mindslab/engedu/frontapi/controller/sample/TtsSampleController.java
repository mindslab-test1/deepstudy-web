package ai.mindslab.engedu.frontapi.controller.sample;

import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import engedu.tts_gw.TtsGw;
import engedu.tts_gw.TtsServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tts")
public class TtsSampleController {

    private ManagedChannel channel;

    private TtsServiceGrpc.TtsServiceBlockingStub blockingStub;


    private final static String SERVER_IP = "10.122.64.57";
    private final static int SERVER_PORT = 9994;
    private final static String TTS_URL ="https://10.122.64.57:8080/record/sample/";


    @RequestMapping("/sample")
    public Response sample(String lang, String inputStr){

        Response responseResult = new Response();

        try{

            this.channel = ManagedChannelBuilder.forAddress(SERVER_IP,SERVER_PORT).usePlaintext().build();
            this.blockingStub = TtsServiceGrpc.newBlockingStub(channel);

            FileCreateUtil fileCreateUtil = new FileCreateUtil();
            String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
            fileCreateUtil.makeFolder("/record/evaluation/tts/",currentTime);
            /**
             * Lang : 영어 = ENG / 한국어 = KOR
             * Script : 응답 문장
             * Filename : 응답 문장 mp3 생성
             *
             */
            String fileName = "sample_"+ currentTime+".mp3";
            String ttsInputStr = inputStr;
            String ttsLang = lang;
            if(StringUtils.isEmpty(lang) || StringUtils.isEmpty(inputStr)){

                ttsLang = ILanguageCodes.KOR;
                ttsInputStr = "나는 전기를 충전해 주면 전기를 먹고 살아.";

            }
            ttsLang = ttsLang.toUpperCase();
            TtsGw.TtsRequest request = TtsGw.TtsRequest.newBuilder()
                    .setLang(ttsLang)
                    .setScript(ttsInputStr)
                    .setFilename("/record/sample/"+fileName)
                    .build();

            TtsGw.TtsResponse response = this.blockingStub.simpleTts(request);

            Result result = new Result();

            if(response.getResultCode() == -1){

                responseResult.setResult(result);
                responseResult.setResCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
                responseResult.setResMsg(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));

            }else{

                result.setTtsText(ttsInputStr);
                result.setTtsUrl(TTS_URL + fileName);
                responseResult.setResult(result);
                responseResult.setResCode(String.valueOf(HttpStatus.OK));
                responseResult.setResMsg(String.valueOf(HttpStatus.OK.getReasonPhrase()));
            }

            this.channel.shutdown();

        }catch (Exception e){

            e.printStackTrace();
        }



        return responseResult;
    }
}
