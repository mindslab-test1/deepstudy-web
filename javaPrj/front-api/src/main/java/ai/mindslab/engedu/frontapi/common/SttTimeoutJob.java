package ai.mindslab.engedu.frontapi.common;

import java.util.TimerTask;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SttTimeoutJob extends TimerTask {
	
	private  WebSocketCallbackInterface webSocket;
	private  WebSocketSession session;
	
	public SttTimeoutJob( WebSocketCallbackInterface webSocket,WebSocketSession session) {
		this.webSocket = webSocket;
		this.session = session;
	}
	
	@Override
	public void run() {
		log.debug("SttTimeoutJob RUN!!!!!!!!!!!!!!!!!!!");
		
		try {
			Response response = new Response(Integer.toString(IRestCodes.ERR_CODE_SOCKET_TIMEOUT), IRestCodes.ERR_MSG_SOCKET_TIMEOUT, "", "", null);
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ", jsonString);
			session.sendMessage(new TextMessage(jsonString));
	
			webSocket.removeSttClient(session);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
