package ai.mindslab.engedu.frontapi.service.data;

import lombok.Data;

@Data
public class Parameters {
	private String v 			= "";    			
	private String biz 			= "";  			
	private String channel 		= ""; 		
	private String language 	= "";	
	private String service 		= "";
	private String userId 		= "";  		
	private String lectureId 	= ""; 		
	private String chapterId 	= ""; 		
	private String contentId 	= ""; 		
	private String sequence 	= "";  		
	private String answerText 	= ""; 	
	private String recordYn 	= "";
	private String targetLetter 	= "";
	private String counter 	= "";
	private String checkSymbol = "";

	
}
